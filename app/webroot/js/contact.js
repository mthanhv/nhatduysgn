jQuery(function($) {'use strict';
	var msg = $('#flash-message');
	if (msg.text() != '') {
		msg.fadeIn().delay(3000).fadeOut();
	}
});