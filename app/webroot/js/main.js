/*price range*/

if ($('#sl2').count > 0) {
    $('#sl2').slider();
}

var RGBChange = function () {
    $('#RGB').css('background', 'rgb(' + r.getValue() + ',' + g.getValue() + ',' + b.getValue() + ')')
};

/*scroll to top*/

$(document).ready(function () {
    $(function () {
        $.scrollUp({
            scrollName: 'scrollUp', // Element ID
            scrollDistance: 300, // Distance from top/bottom before showing element (px)
            scrollFrom: 'top', // 'top' or 'bottom'
            scrollSpeed: 300, // Speed back to top (ms)
            easingType: 'linear', // Scroll to top easing (see http://easings.net/)
            animation: 'fade', // Fade, slide, none
            animationSpeed: 200, // Animation in speed (ms)
            scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
            //scrollTarget: false, // Set a custom target element for scrolling to the top
            scrollText: '<i class="fa fa-angle-up"></i>', // Text for element, can contain HTML
            scrollTitle: false, // Set a custom <a> title if required.
            scrollImg: false, // Set true to use image
            activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
            zIndex: 2147483647 // Z-Index for the overlay
        });

        if (typeof NUMBER_OF_SUPPLIER == 'number' && NUMBER_OF_SUPPLIER > 0) {
            $.scroller({
                item: NUMBER_OF_SUPPLIER,
                mainEl: '#supplier-slide',
                bounderEl: '#supplier-slide #slide-bounder',
                containerEl: '#supplier-slide #slide-container',
                itemEl: '#supplier-slide .video-gallery',
                itemW: 151,
                itemH: 89,
                nextEl: '#supplier-slide #nextctl',
                prevEl: '#supplier-slide #prevctl',
                direction: $.fn.scroller.DIRECTION.HORIZONTAL
            });
        }

        if (typeof NUMBER_OF_RECENT_ITEMS == 'number' && NUMBER_OF_RECENT_ITEMS > 0) {
            $.scroller({
                item: NUMBER_OF_RECENT_ITEMS,
                mainEl: '#recent-item',
                bounderEl: '#recent-item-bounder',
                containerEl: '#recent-item-container',
                itemEl: '.recent-product',
                itemW: 215,
                itemH: 330,
                nextEl: '#recent-item-next',
                prevEl: '#recent-item-prev',
                direction: $.fn.scroller.DIRECTION.HORIZONTAL
            });
        }

        if (typeof NUMBER_OF_RECOMMENDED_ITEMS == 'number' && NUMBER_OF_RECOMMENDED_ITEMS > 0) {
            $.scroller({
                item: NUMBER_OF_RECOMMENDED_ITEMS,
                mainEl: '#recommended-item',
                bounderEl: '#recommended-item-bounder',
                containerEl: '#recommended-item-container',
                itemEl: '.recommended-product',
                itemW: 215,
                itemH: 330,
                nextEl: '#recommended-item-next',
                prevEl: '#recommended-item-prev',
                direction: $.fn.scroller.DIRECTION.HORIZONTAL
            });
        }

        if (typeof NUMBER_OF_LATEST_ITEMS == 'number' && NUMBER_OF_LATEST_ITEMS > 0) {
            $.scroller({
                item: NUMBER_OF_LATEST_ITEMS,
                mainEl: '#latest-item',
                bounderEl: '#latest-item-bounder',
                containerEl: '#latest-item-container',
                itemEl: '.latest-product',
                itemW: 215,
                itemH: 330,
                nextEl: '#latest-item-next',
                prevEl: '#latest-item-prev',
                direction: $.fn.scroller.DIRECTION.HORIZONTAL
            });
        }
    });
});
