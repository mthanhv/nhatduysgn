(function($, window, document) {
    'use strict';

    $.fn.scroller = function(options) {
        var money = new MonkeySlider();
        money.init(options);
    }

    $.fn.scroller.DIRECTION = {
        HORIZONTAL: 'HORIZONTAL',
        VERTICAL: 'VERTICAL',
    };

    $.fn.scroller.defaults = {
        mainEl: '#boundEl',
        bounderEl: '#bounderEl',
        containerEl: '#containerEl',
        itemEl: '.item',
        itemW: 0,
        itemH: 0,
        nextEl: '#next',
        prevEl: '#prev',
        direction: $.fn.scroller.DIRECTION.HORIZONTAL
    };

    $.scroller = $.fn.scroller;

    var MonkeySlider = function () {
        this.options = {};

        this.vars = {
            topLeftX: 0,
            topLeftY: 0,
            page: 1,
            totalPage: 1,
            boundW: 0,
            boundH: 0,
            moveDelta: 0,
            mainObj: null,
            bounderObj: null,
            containerObj: null,
            itemObjs: null,
            nextObj: null,
            prevObj: null,
            itemPerRow: 1
        };

        this.init = function(options) {
            var o = this.options = $.extend({}, $.fn.scroller.defaults, options);
            var vars = this.vars;
            var me = this;

            vars.mainObj = $(o.mainEl);
            vars.bounderObj = $(o.bounderEl);
            vars.containerObj = $(o.containerEl);
            vars.itemObjs = $(o.itemEl);
            vars.nextObj = $(o.nextEl);
            vars.prevObj = $(o.prevEl);

            $(window).resize(function() {
                me.onBoundSizesChanges();
            });

            vars.nextObj.click(function (){
                if (o.direction == $.fn.scroller.DIRECTION.HORIZONTAL) {
                    me.moveRight();
                } else {
                    me.moveDown();
                }
            });

            vars.prevObj.click(function(){
                if (o.direction == $.fn.scroller.DIRECTION.HORIZONTAL) {
                    me.moveLeft();
                } else {
                    me.moveUp();
                }
            });

            this.reset();
        }

        this.reset = function() {
            var o = this.options;
            var vars = this.vars;

            vars.page = 1;
            vars.topLeftX = 0;
            vars.topLeftY = 0;

            if (o.itemW == 0 && vars.itemObjs.length > 0) {
                o.itemW = $(vars.itemObjs[0]).width();
            }
            if (o.itemH == 0 && vars.itemObjs.length > 0) {
                o.itemH = $(vars.itemObjs[0]).height();
            }

            vars.itemObjs.each(function(){
                $(this).css({
                    width: o.itemW,
                    height: o.itemH,
                });
            });

            vars.boundW = vars.mainObj.width();
            vars.boundH = vars.mainObj.height();

            if (o.direction == $.fn.scroller.DIRECTION.HORIZONTAL) {
                vars.itemPerRow = Math.min(Math.max(Math.floor(vars.boundW / o.itemW), 1), o.item);
                vars.totalPage = Math.ceil(o.item / vars.itemPerRow);

                vars.containerObj.css({
                    width: o.item * o.itemW,
                    height: o.itemH,
                    marginLeft: vars.topLeftX,
                    marginTop: vars.topLeftY
                });

                vars.moveDelta = vars.itemPerRow * o.itemW;
            }

            if (o.direction == $.fn.scroller.DIRECTION.VERTICAL) {
                vars.itemPerRow = Math.min(Math.max(Math.floor(vars.boundW / o.itemW), 1), o.item);
                vars.totalPage = Math.ceil(o.item / vars.itemPerRow);
                vars.containerObj.css({
                    width: vars.itemPerRow * o.itemW,
                    height: vars.totalPage * o.itemH,
                    marginLeft: vars.topLeftX,
                    marginTop: vars.topLeftY
                });

                vars.moveDelta = o.itemH;
            }

            vars.bounderObj.css({
                width: vars.itemPerRow * o.itemW,
                height: o.itemH,
                marginLeft: Math.floor((vars.boundW - vars.itemPerRow * o.itemW) / 2),
                marginTop: 0,
                overflow: 'hidden'
            });

            this.updateNextBtnState();
            this.updatePrevBtnState();
        }

        this.onBoundSizesChanges = function() {
            this.reset();
        }

        this.moveLeft = function() {
            this.vars.page --;
            this.vars.topLeftX += this.vars.moveDelta;

            this.setPosition(this.vars.topLeftX, this.vars.topLeftY);
            this.updateNextBtnState();
            this.updatePrevBtnState();
        }

        this.moveRight = function() {
            this.vars.page ++;
            this.vars.topLeftX -= this.vars.moveDelta;

            this.setPosition(this.vars.topLeftX, this.vars.topLeftY);
            this.updateNextBtnState();
            this.updatePrevBtnState();
        }

        this.moveUp = function() {
            this.vars.page --;
            this.vars.topLeftY += this.vars.moveDelta;

            this.setPosition(this.vars.topLeftX, this.vars.topLeftY);
            this.updateNextBtnState();
            this.updatePrevBtnState();
        }

        this.moveDown = function() {
            this.vars.page ++;
            this.vars.topLeftY -= this.vars.moveDelta;

            this.setPosition(this.vars.topLeftX, this.vars.topLeftY);
            this.updateNextBtnState();
            this.updatePrevBtnState();
        }

        this.setPosition = function(x, y) {
            this.vars.containerObj.animate({
                marginLeft: x,
                marginTop: y
            }, 500);
        }

        this.updateNextBtn = function(enable) {
            this.vars.nextObj.css({
                display: (enable ? 'block' : 'none')
            });
        }

        this.updatePrevBtn = function(enable) {
            this.vars.prevObj.css({
                display: (enable ? 'block' : 'none')
            });
        }

        this.updateNextBtnState = function() {
            var enable = true;
            if (this.vars.itemPerRow >= this.options.item) {
                enable = false;
            }

            if (this.vars.page >= this.vars.totalPage) {
                enable = false;
            }

            this.updateNextBtn(enable);
        }

        this.updatePrevBtnState = function() {
            var enable = true;
            if (this.vars.itemPerRow >= this.options.item) {
                enable = false;
            }

            if (this.vars.page <= 1) {
                enable = false;
            }

            this.updatePrevBtn(enable);
        }
    }
} (jQuery, window, document));