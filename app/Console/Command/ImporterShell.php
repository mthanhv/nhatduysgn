<?php
App::uses('Shell', 'Console');

class ImporterShell extends Shell {
    private function getReader() {
        App::uses('ConnectionManager', 'Model');
        return ConnectionManager::create('reader', array(
            'datasource' => 'Database/Mysql',
            'persistent' => false,
            'host' => '127.0.0.1',
            'login' => 'root',
            'password' => 'SuCon7891',
            'database' => 'kvsvalve',
            'prefix' => '',
            'encoding' => 'utf8',
        ));
    }

    private function getWriter() {
        App::uses('ConnectionManager', 'Model');
        return ConnectionManager::getDataSource('default');
    }

    public function menu() {
        $reader = $this->getReader();
        $writer = $this->getWriter();

        $sql = "SELECT * FROM jos_menu AS menu WHERE published = 1";
        $menus = call_user_func_array(array(&$reader, 'query'), array($sql));

        $sql = "TRUNCATE menu_items";
        call_user_func_array(array(&$writer, 'query'), array($sql));

        $sql = "DELETE FROM i18n WHERE model = 'MenuItem'";
        call_user_func_array(array(&$writer, 'query'), array($sql));

        App::import('Model', 'MenuItem');
        App::import('Model', 'Product');
        $this->MenuItem = new MenuItem();
        $this->Product = new Product();

        $home_menu_id = 0;

        foreach ($menus as $menu) {
            //Preprocess link
            $link = '';
            if (preg_match('/index\.php\?option\=com_k2\&view\=item\&layout\=item\&id\=([0-9]+)/', $menu['menu']['link'], $matches)) {
                if ($matches[1] > 0) {
                    $product = $this->Product->find('first', array('conditions'=>array('old_id'=>$matches[1])));
                    if ($product) {
                        $link = "/products/view/{$product['Product']['id']}/{$menu['menu']['alias']}.html";
                    }
                }
            }

            if ($menu['menu']['name'] == 'CONTACT US') {
                $link = '/contacts/contactus/contact-us.html';
            }

            if ($menu['menu']['name'] == 'CERTIFICATES') {
                $link = '/articles/view/2/certificates.html';
            }

            $this->MenuItem->create();
            $result = $this->MenuItem->save(array(
                'title' => $menu['menu']['name'],
                'link' => empty($link) ? '/' : $link,
                'publish' => $menu['menu']['published'],
                'menu_id' => 1,
                'old_id' => $menu['menu']['id'],
                'old_parent_id' => $menu['menu']['parent'],
            ));

            if (!$result) {
                debug($menu);
            }

            $menuItem = $this->MenuItem->read(null, $this->MenuItem->getLastInsertID());

            // Get HOME menu id
            if ($menu['menu']['name'] == 'HOME') {
                $home_menu_id = $menuItem['MenuItem']['id'];
            }

            if ($menuItem) {
                $sql = "SELECT * FROM jos_jf_content AS jjc WHERE reference_table = 'menu' AND reference_field in ('name') AND reference_id = {$menuItem['MenuItem']['old_id']}";
                $translatedDatas = call_user_func_array(array(&$reader, 'query'), array($sql));
                $newTranslatedData = array();
                foreach ($translatedDatas as $translatedData) {
                    $languageCode = $this->mapLanguage($translatedData['jjc']['language_id']);
                    if ($languageCode) {
                        $newTranslatedData[$languageCode][$translatedData['jjc']['reference_field']] = $translatedData['jjc']['value'];
                    }
                }

                $titleBak = $menuItem['MenuItem']['title'];

                $languages = Configure::read('AVAILABLE_LANGUAGES');
                foreach ($languages as $language => $languageName) {
                    $menuItem['MenuItem']['title'] = isset($newTranslatedData[$language]['name']) ? $newTranslatedData[$language]['name'] : $titleBak;

                    $this->MenuItem->locale = $language;
                    $this->MenuItem->save($menuItem);
                }
            } else {
                debug('Can\' read product information after saving');
            }
        }

        $menus = $this->MenuItem->find('all', array('conditions' => array('old_parent_id !=' => 0)));
        foreach ($menus as $menu) {
            $parent = $this->MenuItem->find('first', array('conditions' => array('old_id' => $menu['MenuItem']['old_parent_id'])));
            if ($parent) {
                $menu['MenuItem']['parent_id'] = $parent['MenuItem']['id'];
                $this->MenuItem->save($menu, true, array('parent_id'));
            } else {
                debug($parent);
            }
        }

        if (!empty($home_menu_id)) {
            $this->MenuItem->moveUp($home_menu_id, 3);
        }
    }

    private function mapLanguage($languageId = null) {
        $map = array(
            1 => 'eng',
            3 => 'tur',
            4 => 'rus',
            5 => 'ara',
        );

        return isset($map[$languageId]) ? $map[$languageId] : null;
    }

    public function product() {
        $reader = $this->getReader();
        $writer = $this->getWriter();

        $sql = "SELECT * FROM jos_k2_items AS jki LEFT JOIN jos_k2_attachments AS jka on jka.itemID = jki.id WHERE catid = 57 GROUP BY jki.id";
        $items = call_user_func_array(array(&$reader, 'query'), array($sql));

        $sql = "TRUNCATE products";
        call_user_func_array(array(&$writer, 'query'), array($sql));

        $sql = "DELETE FROM i18n WHERE model = 'Product'";
        call_user_func_array(array(&$writer, 'query'), array($sql));

        exec('rm -f /home/tvo/www/KVSValveNew/app/webroot/files/products/attachments/*.*');
        exec('rm -f /home/tvo/www/KVSValveNew/app/webroot/files/products/previews/*.*');
        exec('rm -f /home/tvo/www/KVSValveNew/app/webroot/files/products/thumbs/*.*');
        exec('rm -f /home/tvo/www/KVSValveNew/app/webroot/files/products/*.*');
        exec('rm -f /home/tvo/www/KVSValveNew/app/webroot/media/products/*.*');

        App::import('Model', 'Product');
        $this->Product = new Product();
        foreach ($items as $item) {
            //Preprocess attachment
            $attachment = $item['jka']['filename'];
            $attachment_old_path = '/home/tvo/www/KVSValve/media/k2/attachments/';
            $attachment_new_path = '/home/tvo/www/KVSValveNew/app/webroot/files/products/attachments/';
            if (!empty($attachment) && file_exists($attachment_old_path.$attachment)) {
                if (!copy($attachment_old_path.$attachment, $attachment_new_path.$attachment)) {
                    $attachment = '';
                }
            } else {
                $attachment = '';
            }

            //Preprocess image
            $image = md5('Image'.$item['jki']['id']) . '.jpg';
            $image_old_path = '/home/tvo/www/KVSValve/media/k2/items/src/';
            $image_new_path = '/home/tvo/www/KVSValveNew/app/webroot/files/products/';
            if (file_exists($image_old_path.$image)) {
                if (!copy($image_old_path.$image, $image_new_path.$image)) {
                    $image = '';
                }
            } else {
                $image = '';
            }
            if (!empty($image) && file_exists($image_new_path.$image)) {
                $this->img_convert($image_new_path.$image, $image_new_path.'thumbs/'.$image, 320, 320);
                $this->img_convert($image_new_path.$image, $image_new_path.'previews/'.$image, 230, 150);
            }

            //Preprocess images in content
            $content = $this->replace_image_path($item['jki']['introtext']);

            $this->Product->create();
            $result = $this->Product->save(array(
                'image' => $image,
                'title' => $item['jki']['title'],
                'content' => $content,
                'view_count' => $item['jki']['hits'],
                'product_category_id' => 1,
                'attachment' => $attachment,
                'meta_keywords' => $item['jki']['metakey'],
                'meta_description' => $item['jki']['metadesc'],
                'publish' => $item['jki']['published'],
                'ordering' => $item['jki']['ordering'],
                'old_id' => $item['jki']['id'],
            ));

            if (!$result) {
                debug($item);
                continue;
            }

            $product = $this->Product->read(null, $this->Product->getLastInsertID());
            if ($product) {
                $sql = "SELECT * FROM jos_jf_content AS jjc WHERE reference_table = 'k2_items' AND reference_field in ('title', 'introtext') AND reference_id = {$product['Product']['old_id']}";
                $translatedDatas = call_user_func_array(array(&$reader, 'query'), array($sql));
                $newTranslatedData = array();
                foreach ($translatedDatas as $translatedData) {
                    $languageCode = $this->mapLanguage($translatedData['jjc']['language_id']);
                    if ($languageCode) {
                        $newTranslatedData[$languageCode][$translatedData['jjc']['reference_field']] = $translatedData['jjc']['value'];
                    }
                }

                $titleBak = $product['Product']['title'];
                $contentBak = $product['Product']['content'];

                $languages = Configure::read('AVAILABLE_LANGUAGES');
                foreach ($languages as $language => $languageName) {
                    $product['Product']['title'] = isset($newTranslatedData[$language]['title']) ? $newTranslatedData[$language]['title'] : $titleBak;
                    if (isset($newTranslatedData[$language]['introtext'])) {
                        $product['Product']['content'] = $this->replace_image_path($newTranslatedData[$language]['introtext']);
                    } else {
                        $product['Product']['content'] = $contentBak;
                    }

                    $this->Product->locale = $language;
                    $this->Product->save($product);
                }
            } else {
                debug('Can\'t read product information after saving');
            }
        }
    }

    private function replace_image_path($content) {
        preg_match_all('/(img|src)\=(\"|\')[^\"\'\>]+/i', $content, $media);
        if (empty($media)) {
            return $content;
        }

        $medias = preg_replace('/(img|src)(\"|\'|\=\"|\=\')(.*)/i', "$3", $media[0]);
        if (empty($medias)) {
            return $content;
        }

        $media_old_path = '/home/tvo/www/KVSValve/';
        $media_new_path = '/home/tvo/www/KVSValveNew/app/webroot/media/products/';
        $web_media_path = '/media/products/';
        foreach (array_chunk($medias, 2, false) as $media) {
            echo '====='.PHP_EOL;
            echo 'Old: '.$media[0].PHP_EOL;
            if (false !== strpos($media[0], 'http://') || false !== strpos($media[0], 'https://')) {
                $path = $media[0];
                $image_content = @file_get_contents($path);
            } else if (file_exists($media_old_path.$media[0])) {
                $path = $media_old_path.$media[0];
                $image_content = file_get_contents($path);
            }
            if (!empty($image_content)) {
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $name = uniqid();
                $filename = "{$name}.{$ext}";
                if (file_put_contents($media_new_path.$filename, $image_content)) {
                    echo 'New: '.$web_media_path.$filename.PHP_EOL;
                    $content = str_replace($media[0], $web_media_path.$filename, $content);
                }
            }
        }

        return $content;
    }

    private function img_convert($src_name, $dst_name, $new_w = 0, $new_h = 0) {
        //get image extension.
        $src_ext = strtolower(pathinfo($src_name, PATHINFO_EXTENSION));
        $dst_ext = strtolower(pathinfo($dst_name, PATHINFO_EXTENSION));

        //creates the new image using the appropriate function from gd library
        $src_img = $dst_img = null;
        if($src_ext == 'jpg' || $src_ext == 'jpeg') $src_img = imagecreatefromjpeg($src_name);
        else if($src_ext == 'png') $src_img = imagecreatefrompng($src_name);
        else if ($src_ext == 'gif') $src_img = imagecreatefromgif($src_name);

        if($src_img) {
            $src_offset_x = $src_offset_y = $dst_offset_x = $dst_offset_y = $dst_rect_w = $dst_rect_h = $src_rect_w = $src_rect_h = 0;

            //gets the dimmensions of the image
            $old_w = imageSX($src_img);
            $old_h = imageSY($src_img);

            if($new_w != 0 && $new_h != 0) {
                $ratio1 = $old_w/$new_w;
                $ratio2 = $old_h/$new_h;
                if($ratio1 > $ratio2) {
                    $src_rect_h = $old_h;
                    $src_rect_w = $new_w*$ratio2;
                } else {
                    $src_rect_w = $old_w;
                    $src_rect_h = $new_h*$ratio1;
                }
                $src_offset_x = ($old_w - $src_rect_w)/2;
                $src_offset_y = ($old_h - $src_rect_h)/2;
                $dst_rect_w = $new_w;
                $dst_rect_h = $new_h;
            } else if($new_w != 0) {
                $ratio1 = $new_w/$old_w;
                $src_rect_w = $old_w;
                $src_rect_h = $old_h;
                $dst_rect_w = $new_w;
                $dst_rect_h = $old_h*$ratio1;
            } else if($new_h != 0) {
                $ratio2 = $new_h/$old_h;
                $src_rect_w = $old_w;
                $src_rect_h = $old_h;
                $dst_rect_h = $new_h;
                $dst_rect_w = $old_w*$ratio2;
            } else {
                $src_rect_w = $dst_rect_w = $new_w = $old_w;
                $src_rect_h = $dst_rect_h = $new_h = $old_h;
            }

            // we create a new image with the new dimmensions
            $dst_img = ImageCreateTrueColor($dst_rect_w, $dst_rect_h);

            // resize the big image to the new created one
            //var_dump(array($dst_offset_x, $dst_offset_y, $src_offset_x, $src_offset_y, $dst_rect_w, $dst_rect_h, $src_rect_w, $src_rect_h));
            imagecopyresampled($dst_img, $src_img, $dst_offset_x, $dst_offset_y, $src_offset_x, $src_offset_y, $dst_rect_w, $dst_rect_h, $src_rect_w, $src_rect_h);

            // Output the created image to the file. Now we will have the thumbnail into the file named by $dst_name
            if($dst_ext == 'jpg' || $dst_ext == 'jpeg') imagejpeg($dst_img, $dst_name);
            else if($dst_ext == 'png') imagepng($dst_img, $dst_name);
            else if ($dst_ext == 'gif') imagegif($dst_img, $dst_name);

            //destroys source and destination images.
            imagedestroy($dst_img);
            imagedestroy($src_img);
            return true;
        }
        return false;
    }
}
