<?php

class BrandModule extends AppModule
{
    public function index()
    {
        $this->loadModel('Brand');

        $brands = $this->Brand->find('all', array(
            'fields' => ['Brand.*', 'count(Product.id) as count'],
            'conditions' => array('Brand.publish' => PUBLISHED),
            'order' => array('Brand.id' => 'DESC'),
            'joins' => [
                [
                    'table' => 'products',
                    'alias' => 'Product',
                    'type' => 'LEFT',
                    'conditions' => ['Product.brand_id = Brand.id']
                ]
            ],
            'group' => 'Brand.id'
        ));

        $this->set('brands', $brands);
    }
}