<?php

class CustomerModule extends AppModule
{
    public function index()
    {
        $this->loadModel('Customer');

        $customers = $this->Customer->find('all', array(
            'conditions' => array('Customer.publish' => PUBLISHED),
            'order' => array('Customer.id' => 'DESC'),
//			'limit' => 6
        ));

        $this->set('customers', $customers);
    }
}