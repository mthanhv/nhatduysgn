<?php

class LanguageModule extends AppModule
{
    public function index()
    {
        $this->set('languages', Configure::read('AVAILABLE_LANGUAGES'));
    }
}