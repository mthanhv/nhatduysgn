<?php

class ProductCategoryModule extends AppModule
{
    public function index()
    {
        $this->loadModel('ProductCategory');

        $categories = $this->ProductCategory->find('threaded', array(
            'conditions' => array('publish' => PUBLISHED),
            'order' => array('lft' => 'ASC')
        ));

        $this->set('categories', $categories);
    }

    public function featured($limit = null)
    {
        $this->loadModel('ProductCategory');

        $params = array(
            'conditions' => array('featured' => 1),
            'order' => array('lft' => 'ASC')
        );
        if ($limit) {
            $params['limit'] = $limit;
        }
        $categories = $this->ProductCategory->find('all', $params);
        $this->set('categories', $categories);
    }

    public function categories()
    {
        $this->loadModel('ProductCategory');

        $tree = $this->ProductCategory->generateTreeList(null, null, null, '----');
        $counts = $this->ProductCategory->find('all', array(
            'fields' => array('ProductCategory.id', '(SELECT COUNT(*) FROM product_categories as ProductCategory2 INNER JOIN products as Product ON Product.product_category_id = ProductCategory2.id WHERE ProductCategory2.lft >= ProductCategory.lft AND ProductCategory2.rght <= ProductCategory.rght) as count')
        ));

        $categories = array();
        foreach ($tree as $id => $node) {
            $count = 0;
            foreach ($counts as $item) {
                if ($item['ProductCategory']['id'] == $id) {
                    $count = $item[0]['count'];
                }
            }
            $categories[$id] = array(
                'id' => $id,
                'name' => $node,
                'count' => $count
            );
        }

        $this->set('categories', $categories);
    }
}