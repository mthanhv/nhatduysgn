<?php

class ProductModule extends AppModule
{
    public function featured($category_id = null, $limit = 10)
    {
        $this->loadModel('Product');

        $params = array();
        if (!empty($category_id)) {
            $params['conditions']['Product.product_category_id'] = $category_id;
        }
        if (!empty($limit)) {
            $params['limit'] = $limit;
        }
        $params['order'] = array('Product.ordering' => 'DESC', 'Product.id' => 'DESC');
        $params['conditions']['Product.publish'] = PUBLISHED;
        $params['conditions']['Product.featured'] = FEATURED;
        $params['fields'] = array('Product.id', 'Product.image', 'Product.title', 'Product.content');

        $products = $this->Product->find('all', $params);
        $this->set('products', $products);
    }

    public function lastest($category_id = null, $limit = 10)
    {
        $this->loadModel('Product');

        $params = array();
        if (!empty($category_id)) {
            $params['conditions']['Product.product_category_id'] = $category_id;
        }
        if (!empty($limit)) {
            $params['limit'] = $limit;
        }
        $params['order'] = array('Product.ordering' => 'DESC', 'Product.id' => 'DESC');
        $params['conditions']['Product.publish'] = PUBLISHED;
        $params['fields'] = array('Product.id', 'Product.image', 'Product.title', 'Product.content');

        $products = $this->Product->find('all', $params);
        $this->set('products', $products);
    }

    public function latest($category_id = null, $limit = 10)
    {
        $this->loadModel('Product');

        $params = array();
        if (!empty($category_id)) {
            $params['conditions']['Product.product_category_id'] = $category_id;
        }
        if (!empty($limit)) {
            $params['limit'] = $limit;
        }
        $params['order'] = array('Product.ordering' => 'DESC', 'Product.id' => 'DESC');
        $params['conditions']['Product.publish'] = PUBLISHED;
        $params['fields'] = array('Product.id', 'Product.image', 'Product.title', 'Product.content');

        $products = $this->Product->find('all', $params);
        $this->set('products', $products);
    }

    public function recent($max_items = null)
    {
        $recentViewIds = $this->Session->read(RECENT_VIEW_ITEMS_KEY);

        $products = array();
        if (!empty($recentViewIds) && is_array($recentViewIds)) {
            $this->loadModel('Product');

            arsort($recentViewIds, SORT_NUMERIC);

            $counter = 0;
            foreach ($recentViewIds as $id => $lastVisit) {
                $item = $this->Product->find('first', array(
                    'conditions' => array('Product.publish' => PUBLISHED, 'Product.id' => $id),
                    'fields' => array('Product.id', 'Product.image', 'Product.title')
                ));
                if (!empty($item)) {
                    $products[] = $item;
                }
                if ($max_items !== null && ++$counter >= $max_items) {
                    break;
                }
            }
        }

        $this->set('products', $products);
    }

    public function recentv2($max_items = null)
    {
        $recentViewIds = $this->Session->read(RECENT_VIEW_ITEMS_KEY);

        $products = array();
        if (!empty($recentViewIds) && is_array($recentViewIds)) {
            $this->loadModel('Product');

            arsort($recentViewIds, SORT_NUMERIC);

            $counter = 0;
            foreach ($recentViewIds as $id => $lastVisit) {
                $item = $this->Product->find('first', array(
                    'conditions' => array('Product.publish' => PUBLISHED, 'Product.id' => $id),
                    'fields' => array('Product.id', 'Product.image', 'Product.title')
                ));
                if (!empty($item)) {
                    $products[] = $item;
                }
                if ($max_items !== null && ++$counter >= $max_items) {
                    break;
                }
            }
        }

        $this->set('products', $products);
    }
}