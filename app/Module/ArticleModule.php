<?php

class ArticleModule extends AppModule
{
    public function index($category_id = null, $limit = 10)
    {
        $this->loadModel('Article');

        $params = array();
        if (!empty($category_id)) {
            $params['conditions']['Article.article_category_id'] = $category_id;
        }
        if (!empty($limit)) {
            $params['limit'] = $limit;
        }
        $params['order'] = array('id' => 'DESC');

        $articles = $this->Article->find('all', $params);
        $this->set('articles', $articles);
    }

    public function latest($category_id = null, $limit = 10)
    {
        $this->loadModel('Article');

        $params = array();
        if (!empty($category_id)) {
            $params['conditions']['Article.article_category_id'] = $category_id;
        }
        if (!empty($limit)) {
            $params['limit'] = $limit;
        }
        $params['order'] = array('id' => 'DESC');

        $articles = $this->Article->find('all', $params);
        $this->set('articles', $articles);
    }
}