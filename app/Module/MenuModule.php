<?php

class MenuModule extends AppModule
{
    public function index($menu = null)
    {
        $this->loadModel('MenuItem');

        $menu_items = $this->MenuItem->find('threaded', array(
            'conditions' => array('menu_id' => $menu, 'publish' => PUBLISHED),
            'order' => array('lft' => 'ASC')
        ));

        $this->set('menu_items', $menu_items);
    }
}