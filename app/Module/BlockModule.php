<?php

class BlockModule extends AppModule
{
    public function index($block_id = null)
    {
        $this->loadModel('Block');

        $block = $this->Block->read(null, $block_id);
        $this->set('block', $block);
    }
}