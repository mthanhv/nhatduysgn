<?php

class FilterModule extends AppModule
{
    const FILTERABLE_ATTRIBUTES_CACHE_KEY = 'filterableAttributes';

    protected function getFilterableAttributes()
    {
        return [
            'Product.title' => ['name' => __('Product Name'), 'type' => 'text'],
            'Product.product_category_id' => ['name' => __('Category'), 'type' => 'select'],
            'Product.publish' => ['name' => __('Publish'), 'type' => 'select']
        ];
    }

    public function index()
    {
        $cacheKey = static::FILTERABLE_ATTRIBUTES_CACHE_KEY;
        $facets = Cache::read($cacheKey);

        $facets = false;

        if (false === $facets) {
            App::import('Model', 'Product');
            $this->Product = new Product();

            $facets = $this->getFilterableAttributes();

            foreach ($facets as $attr => &$attrData) {
                if ($attrData['type'] == 'text') continue;

                $datas = $this->Product->find('all', [
                    'fields' => [$attr, 'count(Product.id) as count'],
                    'group' => [$attr],
                    'order' => [$attr => 'DESC'],
                    'limit' => 20,
                    'conditions' => ['Product.publish' => 1]
                ]);

                $options = [];
                if (!empty($datas)) {
                    foreach ($datas as $data) {
                        $options[Set::extract($data, $attr)] = $data[0]['count'];
                    }
                }

                $attrData['options'] = $options;
            }

            Cache::write($cacheKey, $facets);
        }

        $this->set('facets', $facets);
    }
}