<?php

class TagModule extends AppModule
{
    const TAG_CLOUD_CACHE_KEY = 'tagCloud';

    public function index()
    {
        $cacheKey = static::TAG_CLOUD_CACHE_KEY;
        $tags = Cache::read($cacheKey);

        if (false === $tags) {
            App::import('Model', 'Tag');
            $this->Tag = new Tag();

            $tags = $this->Tag->find('all', array(
                'joins' => array(
                    array(
                        'alias' => 'ProductTag',
                        'table' => 'products_tags',
                        'type' => 'LEFT',
                        'conditions' => '`ProductTag`.`tag_id` = `Tag`.`id`'
                    )
                ),
                'fields' => array('Tag.id', 'Tag.title', 'count(ProductTag.id) as count'),
                'group' => array('Tag.id'),
                'order' => array('count(ProductTag.id)' => 'DESC'),
                'limit' => 25
            ));

            if (!empty($tags)) {
                $maxFont = 30;
                $minFont = 10;
                $deltaFont = $maxFont - $minFont;

                $maxApperance = current($tags);
                $minApperance = end($tags);
                $deltaApperance = $maxApperance[0]['count'] - $minApperance[0]['count'];

                foreach ($tags as &$tag) {
                    if ($maxApperance[0]['count'] == 0 || $deltaFont == 0 || $deltaApperance == 0) {
                        $fontSize = $minFont;
                    } else {
                        $fontSize = $minFont + intval((($tag[0]['count'] - $minApperance[0]['count']) / $deltaApperance) * $deltaFont);
                    }

                    $tag['Tag']['count'] = $tag[0]['count'];
                    $tag['Tag']['font_size'] = $fontSize;
                    unset($tag[0]);
                }

                //Randomly ordering
                $len = count($tags);
                for ($index = 0; $index < $len; $index++) {
                    $i = rand(0, $len - 1);
                    $j = rand(0, $len - 1);
                    if ($i != $j) {
                        //Swap them
                        $temp = $tags[$i];
                        $tags[$i] = $tags[$j];
                        $tags[$j] = $temp;
                    }
                }
            }

            Cache::write($cacheKey, $tags);
        }

        $this->set('tags', $tags);
    }
}