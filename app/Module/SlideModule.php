<?php

class SlideModule extends AppModule
{
    public function index()
    {
        App::import('Model', 'Slide');
        $this->Slide = new Slide();

        $slides = $this->Slide->find('all');
        $this->set('slides', $slides);
    }
}