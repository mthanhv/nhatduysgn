<?php

class SupplierModule extends AppModule
{
    public function index()
    {
        $this->loadModel('Supplier');

        $suppliers = $this->Supplier->find('all', array(
            'conditions' => array('Supplier.publish' => PUBLISHED),
            'order' => array('Supplier.id' => 'DESC'),
//			'limit' => 6
        ));

        $this->set('suppliers', $suppliers);
    }
}