<?php

class MenuItemsController extends AdminAppController
{
    var $uses = 'MenuItem';
    var $paginate = array('order' => 'MenuItem.id DESC');

    function index($menu = 0)
    {
        $this->MenuItem->loadRelation('belongsTo', 'Menu', false);
        $this->paginate = array(
            'MenuItem' => array(
                'order' => array(
                    'MenuItem.menu_id' => 'DESC',
                    'MenuItem.lft' => 'ASC'
                )
            )
        );
        $conditions = array();
        if (!empty($menu)) $conditions['MenuItem.menu_id'] = $menu;
        $menu_items = $this->paginate('MenuItem', $conditions);
        $this->set('menu_items', $menu_items);

        $menuTreeList = array();
        $menuIds = array();
        if (!empty($menu)) {
            $menuIds[] = $menu;
        } else {
            Controller::loadModel('Menu');
            $items = $this->Menu->find('all');
            foreach ($items as $item) {
                $menuIds[] = $item['Menu']['id'];
            }
        }
        foreach ($menuIds as $menuId) {
            $menuTreeList[$menuId] = $this->MenuItem->generateTreeList(array('menu_id' => $menuId), null, null, '----');
        }
        $this->set('menuTreeList', $menuTreeList);
    }

    function add()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $add_and_new = isset($this->request->data['FormAction']['add_and_new']) ? true : false;
            unset($this->request->data['FormAction']);

            $this->MenuItem->create();
            if (empty($this->request->data['MenuItem']['parent_id'])) $this->request->data['MenuItem']['parent_id'] = null;
            if ($this->MenuItem->save($this->request->data)) {
                //Update menu link included menu id
                $menu_id = $this->MenuItem->getLastInsertID();
                $link = $this->request->data['MenuItem']['link'];
//					$link = trim($link);
//					$link = str_replace("/i:{$menu_id}", '', $link);
//					$link = rtrim($link, '/');
//					$link = "{$link}/i:{$menu_id}";
                $this->MenuItem->save(array('id' => $menu_id, 'link' => $link));

                $this->Session->setFlash(__('The menu_item has been saved'), 'default', array('class' => 'success'));
                if ($add_and_new) $this->redirect(array('action' => 'add'));
                else $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The menu_item could not be saved. Please, try again'));
            }
        }

        $this->loadModel('Menu');
        $menus = $this->Menu->find('list', array('fields' => array('id', 'menu'), 'recursive' => -1));
        $menus = array(0 => __('Select menu')) + $menus;
        $this->set('menus', $menus);

        $menu_items = array(0 => __('Select parent menu'));
        if (isset($this->request->data['MenuItem']['menu_id'])) {
            $menu_items = $this->MenuItem->generateTreeList(array('menu_id' => $this->request->data['MenuItem']['menu_id']), null, null, '----');
            $menu_items = array(0 => __('Select parent menu')) + $menu_items;
        }
        $this->set('menu_items', $menu_items);

        $link_to_categories = Configure::read('LINK_TO_CATEGORIES');
        $link_to_categories = array(0 => __('Select data')) + $link_to_categories;
        $this->set('link_to_categories', $link_to_categories);
    }

    function edit($id = null)
    {
        $this->MenuItem->id = $id;
        if (!$this->MenuItem->exists()) {
            throw new NotFoundException(__('Invalid menu item'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->MenuItem->create();
            if (empty($this->request->data['MenuItem']['parent_id'])) $this->request->data['MenuItem']['parent_id'] = null;
            if ($this->MenuItem->save($this->request->data)) {
                //Update menu link included menu id
                $menu_id = $id;
                $link = $this->request->data['MenuItem']['link'];
//					$link = trim($link);
//					$link = str_replace("/i:{$menu_id}", '', $link);
//					$link = rtrim($link, '/');
//					$link = "{$link}/i:{$menu_id}";
                $this->MenuItem->save(array('id' => $menu_id, 'link' => $link));

                $this->Session->setFlash(__('The menu item has been saved'), 'default', array('class' => 'success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The menu item could not be saved. Please, try again'));
            }
        } else {
            $this->request->data = $this->MenuItem->read(null, $id);
        }

        $this->loadModel('Menu');
        $menus = $this->Menu->find('list', array('fields' => array('id', 'menu'), 'recursive' => -1));
        $menus = array(0 => __('Select menu')) + $menus;
        $this->set('menus', $menus);

        $menu_items = array(0 => __('Select parent menu'));
        if (isset($this->request->data['MenuItem']['menu_id'])) {
            $menu_items = $this->MenuItem->generateTreeList(array('menu_id' => $this->request->data['MenuItem']['menu_id']), null, null, '----');
            $menu_items = array(0 => __('Select parent menu')) + $menu_items;
        }
        $this->set('menu_items', $menu_items);

        $link_to_categories = Configure::read('LINK_TO_CATEGORIES');
        $link_to_categories = array(0 => __('Select data')) + $link_to_categories;
        $this->set('link_to_categories', $link_to_categories);
    }

    function delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $this->MenuItem->id = $id;
        if (!$this->MenuItem->exists()) {
            throw new NotFoundException(__('Invalid menu item'));
        }

        if ($this->MenuItem->delete($id)) {
            $this->Session->setFlash(__('MenuItem was deleted'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('MenuItem could not be deleted. Please try again'));
            $this->redirect(array('action' => 'index'));
        }
    }

    function deletemulti()
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $ids = isset($this->data['markedvalues']) ? explode(',', $this->data['markedvalues']) : '';
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->MenuItem->id = $id;
                if (!$id || !$this->MenuItem->exists()) continue;
                $this->MenuItem->delete($id);
            }
        }

        $this->Session->setFlash(__('MenuItems were deleted'));
        $this->redirect(array('action' => 'index'));
    }

    function toggle($id = null)
    {
        $this->MenuItem->id = $id;
        $menu_item = $this->MenuItem->find('first', array('conditions' => array('MenuItem.id' => $id), 'recursive' => -1));
        if (!$menu_item) {
            throw new NotFoundException(__('Invalid menu item'));
        }
        $published = empty($menu_item['MenuItem']['publish']) ? 1 : 0;

        $sub_menus = $this->MenuItem->children($id, false, array('MenuItem.id'));
        $sub_menu_ids = array();
        if (!empty($sub_menus)) $sub_menu_ids = Set::extract("{n}.MenuItem.id", $sub_menus);
        $sub_menu_ids[] = $id;

        $this->MenuItem->updateAll(array('MenuItem.publish' => $published), array('MenuItem.id' => $sub_menu_ids));
        $this->redirect(array('action' => 'index') + $this->request->params['named']);
    }

    function translate($id = null)
    {
        $this->layout = 'translation';

        $from = isset($this->request->params['named']['from']) ? $this->request->params['named']['from'] : '';
        $to = isset($this->request->params['named']['to']) ? $this->request->params['named']['to'] : '';
        $this->set('from', $from);
        $this->set('to', $to);

        $this->MenuItem->id = $id;
        $this->MenuItem->locale = $from;
        if (!$this->MenuItem->exists()) {
            throw new NotFoundException(__('Invalid menu item'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $locale = $this->request->data['MenuItem']['locale'];
            $request_data = $this->request->data[$locale];

            $this->MenuItem->create();
            $this->MenuItem->locale = $locale;
            if (empty($request_data['MenuItem']['parent_id'])) $request_data['MenuItem']['parent_id'] = null;
            if ($this->MenuItem->save($request_data)) {
                $this->Session->setFlash(__('The menu item has been saved'), 'default', array('class' => 'success'));
                $this->redirect($this->referer());
            } else {
                $this->Session->setFlash(__('The menu item could not be saved. Please, try again'));
            }
        } else {
            $this->MenuItem->locale = $from;
            $this->request->data[$from] = $this->MenuItem->read(null, $id);
            $this->MenuItem->locale = $to;
            $this->request->data[$to] = $this->MenuItem->read(null, $id);

            if (empty($this->request->data[$to])) $this->request->data[$to] = $this->request->data[$from];
        }

        $this->loadModel('Menu');
        $menus = $this->Menu->find('list', array('fields' => array('id', 'menu'), 'recursive' => -1));
        $menus = array(0 => __('Select menu')) + $menus;
        $this->set('menus', $menus);

        $menu_items = array(0 => __('Select parent menu'));
        if (isset($this->request->data[$from]['MenuItem']['menu_id']) || isset($this->request->data[$to]['MenuItem']['menu_id'])) {
            $menu_id = isset($this->request->data[$from]['MenuItem']['menu_id']) ? $this->request->data[$from]['MenuItem']['menu_id'] : $this->request->data[$to]['MenuItem']['menu_id'];
            $menu_items = $this->MenuItem->generateTreeList(array('menu_id' => $menu_id), null, null, '----');
            $menu_items = array(0 => __('Select parent menu')) + $menu_items;
        }
        $this->set('menu_items', $menu_items);
    }

    function up($id = null)
    {
        $this->MenuItem->id = $id;
        if (!$this->MenuItem->exists()) {
            throw new NotFoundException(__('Invalid menu item'));
        }

        if ($this->MenuItem->moveUp($id, 1)) {
            $this->Session->setFlash(__('Menu item was moved up.'), 'default', array('class' => 'success'));
        } else {
            $this->Session->setFlash(__('Menu item was moved up unsuccessfully.'));
        }
        $this->redirect(array('action' => 'index'));
    }

    function down($id = null)
    {
        $this->MenuItem->id = $id;
        if (!$this->MenuItem->exists()) {
            throw new NotFoundException(__('Invalid menu item'));
        }

        if ($this->MenuItem->moveDown($id, 1)) {
            $this->Session->setFlash(__('Menu item was moved down.'), 'default', array('class' => 'success'));
        } else {
            $this->Session->setFlash(__('Menu item was moved down unsuccessfully.'));
        }
        $this->redirect(array('action' => 'index'));
    }
}

?>