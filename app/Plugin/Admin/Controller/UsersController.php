<?php

class UsersController extends AdminAppController
{
    public $uses = 'User';

    function index()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $url = array('action' => 'index');
            if (!empty($this->request->data['User']['key'])) {
                $url['key'] = $this->Utility->safe_b64encode($this->request->data['User']['key']);
            }
            $this->redirect($url);
        }
        $key = isset($this->request->params['named']['key']) ? $this->Utility->safe_b64decode($this->request->params['named']['key']) : '';
        $key_escaped = str_replace(array('%', '_'), array('\%', '\_'), $key);
        $group = isset($this->request->params['named']['group']) ? $this->request->params['named']['group'] : '';

        $conditions = array();
        if (!empty($key)) {
            $conditions['OR'] = array('User.full_name LIKE' => "%{$key_escaped}%", 'User.user_name LIKE' => "%{$key_escaped}%", 'User.email LIKE' => "%{$key_escaped}%");

            //Pass information for view
            $this->request->data['User']['key'] = $key;
        }
        if (!empty($group)) {
            $this->paginate = array(
                'joins' => array(
                    array(
                        'alias' => 'UserGroup',
                        'table' => 'groups_users',
                        'type' => 'LEFT',
                        'conditions' => array('User.id = UserGroup.user_id')
                    )
                )
            );
            $conditions['UserGroup.group_id'] = $group;
        }

        $this->User->loadRelation('hasAndBelongsToMany', 'Group', false);
        $this->set('users', $this->paginate('User', $conditions));
    }

    function add()
    {
        $this->User->loadRelation('hasAndBelongsToMany', 'Group', false);

        if ($this->request->is('post') || $this->request->is('put')) {
            $add_and_new = isset($this->request->data['FormAction']['add_and_new']) ? true : false;
            unset($this->request->data['FormAction']);

            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $id = $this->User->getLastInsertID();

                //Update password. Make md5 for password
                $password = md5($this->request->data['User']['password']);
                $this->User->updateAll(array('User.password' => "'{$password}'"), array('User.id' => $id));

                $this->Session->setFlash(__('The user has been saved'), 'default', array('class' => 'success'));
                if ($add_and_new) $this->redirect(array('action' => 'add'));
                else $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again'));
            }
        }

        $this->request->data['User']['password'] = '';

        $groups = $this->User->Group->find('list');
        $this->set(compact('groups'));
    }

    function edit($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }

        //Only Admin (id 1) can modify its profile (for security reasons)
        if ($id == 1 && $this->Authen->getUserId() != 1) {
            $this->Session->setFlash(__('Only the admin can change its profile!'));
            return;
        }

        $this->User->loadRelation('hasAndBelongsToMany', 'Group', false);

        if ($this->request->is('post') || $this->request->is('put')) {
            //Never change the login
            if (isset($this->request->data['User']['user_name'])) unset($this->request->data['User']['user_name']);

            //Check if pwd changed
            if (isset($this->request->data['User']['password']) && $this->request->data['User']['password'] == '')
                unset($this->request->data['User']['password']);

            if ($this->User->save($this->request->data)) {
                //Check if pwd changed. Make md5 for password
                if (isset($this->request->data['User']['password']) && $this->request->data['User']['password'] != '') {
                    $password = md5($this->request->data['User']['password']);
                    $this->User->updateAll(array('User.password' => "'{$password}'"), array('User.id' => $id));
                }

                $this->Session->setFlash(__('The user has been saved'), 'default', array('class' => 'success'));
                $this->redirect(array('action' => 'edit', $id));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again'));
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
        }

        //Pass information to view
        $this->request->data['User']['password'] = '';

        $groups = $this->User->Group->find('list');
        $this->set(compact('groups'));
    }

    function delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $this->User->id = $id;
        if (!$id || $id == 1 || !$this->User->exists()) {
            $this->Session->setFlash(__('Invalid user'));
            $this->redirect(array('action' => 'index'));
        }

        $this->User->loadRelation('hasAndBelongsToMany', 'Group', false);

        $user = $this->User->read(null, $id);
        $gr = Set::extract($user, "Group.{n}.id");
        if (in_array(GROUP_ADMIN, $gr) and !in_array(GROUP_ADMIN, $this->Authen->getGroupIds())) {
            $this->Session->setFlash(__('You cannot delete a user in administrators group'));
            $this->redirect(array('action' => 'index'));
        }

        if ($this->User->delete($id)) {
            $this->Session->setFlash(__('User was deleted'), 'default', array('class' => 'success'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('User was not deleted'));
            $this->redirect(array('action' => 'index'));
        }
    }

    function deletemulti()
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $ids = isset($this->data['markedvalues']) ? explode(',', $this->data['markedvalues']) : '';
        if (!empty($ids)) {
            $this->User->loadRelation('hasAndBelongsToMany', 'Group', false);
            foreach ($ids as $id) {
                $this->User->id = $id;
                if (!$id || $id == 1 || !$this->User->exists()) continue;

                $user = $this->User->read(null, $id);
                $gr = Set::extract($user, "Group.{n}.id");
                if (in_array(GROUP_ADMIN, $gr) and !in_array(GROUP_ADMIN, $this->Authen->getGroupIds())) {
                    continue;
                } else {
                    $this->User->delete($id);
                }
            }
        }

        $this->Session->setFlash(__('Users was deleted'));
        $this->redirect(array('action' => 'index'));
    }
}

?>