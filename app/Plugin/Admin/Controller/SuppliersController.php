<?php

class SuppliersController extends AdminAppController
{
    var $uses = 'Supplier';
    var $paginate = array('order' => 'Supplier.id DESC');

    function index()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $url = array('action' => 'index');
            if (!empty($this->request->data['Supplier']['key'])) {
                $url['key'] = $this->Utility->safe_b64encode($this->request->data['Supplier']['key']);
            }
            $this->redirect($url);
        }
        $key = isset($this->request->params['named']['key']) ? $this->Utility->safe_b64decode($this->request->params['named']['key']) : '';
        $key_escaped = str_replace(array('%', '_'), array('\%', '\_'), $key);

        $conditions = array();
        if (!empty($key)) {
            $conditions['OR'] = array('Supplier.title LIKE' => "%{$key_escaped}%");

            //Pass information for view
            $this->request->data['Supplier']['key'] = $key;
        }

        $this->set('suppliers', $this->paginate('Supplier', $conditions));
    }

    function add()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $add_and_new = isset($this->request->data['FormAction']['add_and_new']) ? true : false;
            $is_image_uploaded = (!empty($this->request->data['Supplier']['image']) && isset($this->request->data['Supplier']['image']['error']) && $this->request->data['Supplier']['image']['error'] != UPLOAD_ERR_NO_FILE);
            unset($this->request->data['FormAction']);

            $this->Supplier->create();
            if ($is_image_uploaded) {
                $upload_options = array(
                    'location' => 'files/suppliers/',
                    'check_exists' => true,
                    'generate_unique_name' => false,
                    'clean_name' => true,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array(
                        0 => array(
                            'location' => 'files/suppliers/thumbs/',
                            'check_exists' => true,
                            'generate_unique_name' => false,
                            'use_master_file_name' => true,
                            'clean_name' => false,
                            'name' => null,
                            'ext' => null,
                            'width' => SUPPLIER_THUMB1_W,
                            'height' => SUPPLIER_THUMB1_H,
                        )
                    )
                );
                $data = $this->Utility->upload_file($this->request->data['Supplier']['image'], $upload_options);

                if (is_array($data) && isset($data['error']) && $data['error'] == false) {
                    $this->request->data['Supplier']['image'] = "{$data['data']['name']}.{$data['data']['ext']}";
                } else {
                    $this->Session->setFlash(__('The supplier could not be saved. Please, try again'));
                    $this->Supplier->validationErrors['image'][] = $data['data'];
                    return;
                }
            } else {
                $this->request->data['Supplier']['image'] = '';
            }

            if ($this->Supplier->save($this->request->data)) {
                $this->Session->setFlash(__('The supplier has been saved'), 'default', array('class' => 'success'));
                if ($add_and_new) $this->redirect(array('action' => 'add'));
                else $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The supplier could not be saved. Please, try again'));
            }
        }
    }

    function edit($id = null)
    {
        $this->Supplier->id = $id;
        if (!$this->Supplier->exists()) {
            throw new NotFoundException(__('Invalid supplier'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            // Dont choose image
            if (isset($this->request->data['Supplier']['image']['error']) == false || $this->request->data['Supplier']['image']['error'] == UPLOAD_ERR_NO_FILE) {
                unset($this->request->data['Supplier']['image']);
                if ($this->Supplier->save($this->request->data)) {
                    $this->Session->setFlash(__('The supplier has been saved'), 'default', array('class' => 'success'));
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('Supplier could not be saved . Please try again'));
                }
            } else {
                $upload_options = array(
                    'location' => 'files/suppliers/',
                    'check_exists' => true,
                    'generate_unique_name' => false,
                    'clean_name' => true,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array(
                        0 => array(
                            'location' => 'files/suppliers/thumbs/',
                            'check_exists' => true,
                            'generate_unique_name' => false,
                            'use_master_file_name' => true,
                            'clean_name' => false,
                            'name' => null,
                            'ext' => null,
                            'width' => SUPPLIER_THUMB1_W,
                            'height' => SUPPLIER_THUMB1_H,
                        )
                    )
                );
                $data = $this->Utility->upload_file($this->request->data['Supplier']['image'], $upload_options);
                if (is_array($data) && isset($data['error']) && $data['error'] == false) {
                    $this->request->data['Supplier']['image'] = "{$data['data']['name']}.{$data['data']['ext']}";
                    if ($this->Supplier->save($this->request->data)) {
                        $this->Session->setFlash(__('The supplier has been saved'), 'default', array('class' => 'success'));
                        $this->redirect(array('action' => 'index'));
                    } else {
                        $this->Session->setFlash(__('Supplier could not be saved . Please try again'));
                    }
                } else {
                    $this->Session->setFlash(__('The supplier could not be saved. Please, try again'));
                    $this->Supplier->validationErrors['image'][] = $data['data'];
                }
            }
        } else {
            $this->request->data = $this->Supplier->read(null, $id);
        }
    }

    function delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $this->Supplier->id = $id;
        if (!$this->Supplier->exists()) {
            throw new NotFoundException(__('Invalid supplier'));
        }

        if ($this->Supplier->delete($id)) {
            $this->Session->setFlash(__('Supplier was deleted'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('Supplier could not be deleted. Please try again'));
            $this->redirect(array('action' => 'index'));
        }
    }

    function deletemulti()
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $ids = isset($this->data['markedvalues']) ? explode(',', $this->data['markedvalues']) : '';
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->Supplier->id = $id;
                if (!$id || !$this->Supplier->exists()) continue;
                $this->Supplier->delete($id);
            }
        }

        $this->Session->setFlash(__('Suppliers were deleted'));
        $this->redirect(array('action' => 'index'));
    }

    function translate($id = null)
    {
        $this->layout = 'translation';

        $from = isset($this->request->params['named']['from']) ? $this->request->params['named']['from'] : '';
        $to = isset($this->request->params['named']['to']) ? $this->request->params['named']['to'] : '';
        $this->set('from', $from);
        $this->set('to', $to);

        $this->Supplier->id = $id;
        $this->Supplier->locale = $from;
        if (!$this->Supplier->exists()) {
            throw new NotFoundException(__('Invalid supplier'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $locale = $this->request->data['Supplier']['locale'];
            $request_data = $this->request->data[$locale];

            // Dont choose image
            if (isset($request_data['Supplier']['image']['error']) == false || $request_data['Supplier']['image']['error'] == UPLOAD_ERR_NO_FILE) {
                unset($request_data['Supplier']['image']);
                $this->Supplier->locale = $locale;
                if ($this->Supplier->save($request_data)) {
                    $this->Session->setFlash(__('The supplier has been saved'), 'default', array('class' => 'success'));
                    $this->redirect($this->referer());
                } else {
                    $this->Session->setFlash(__('Supplier could not be saved . Please try again'));
                }
            } else {
                $upload_options = array(
                    'location' => 'files/suppliers/',
                    'check_exists' => true,
                    'generate_unique_name' => false,
                    'clean_name' => true,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array(
                        0 => array(
                            'location' => 'files/suppliers/thumbs/',
                            'check_exists' => true,
                            'generate_unique_name' => false,
                            'use_master_file_name' => true,
                            'clean_name' => false,
                            'name' => null,
                            'ext' => null,
                            'width' => SUPPLIER_THUMB1_W,
                            'height' => SUPPLIER_THUMB1_H,
                        )
                    )
                );
                $data = $this->Utility->upload_file($request_data['Supplier']['image'], $upload_options);
                if (is_array($data) && isset($data['error']) && $data['error'] == false) {
                    $request_data['Supplier']['image'] = "{$data['data']['name']}.{$data['data']['ext']}";
                    $this->Supplier->locale = $locale;
                    if ($this->Supplier->save($request_data)) {
                        $this->Session->setFlash(__('The supplier has been saved'), 'default', array('class' => 'success'));
                        $this->redirect($this->referer());
                    } else {
                        $this->Session->setFlash(__('Supplier could not be saved . Please try again'));
                    }
                } else {
                    $this->Session->setFlash(__('The supplier could not be saved. Please, try again'));
                    $this->Supplier->validationErrors['image'][] = $data['data'];
                }
            }
        } else {
            $this->Supplier->locale = $from;
            $this->request->data[$from] = $this->Supplier->read(null, $id);
            $this->Supplier->locale = $to;
            $this->request->data[$to] = $this->Supplier->read(null, $id);

            if (empty($this->request->data[$to])) $this->request->data[$to] = $this->request->data[$from];
        }
    }
}

?>