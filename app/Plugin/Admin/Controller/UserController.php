<?php

class UserController extends AdminAppController
{
    public $uses = 'User';

    function denied()
    {
        $this->layout = 'Admin.user';
    }

    function login()
    {
        $this->layout = 'Admin.user';

        if ($this->Authen->isLogged()) {
            $this->Session->setFlash(__('You are already logged in'));
            $this->redirect(Configure::read('Authen.loggedAction'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $login = $this->request->data['User']['user_name'];
            $password = $this->request->data['User']['password'];

            $userdata = $this->User->getLoginData($login, $password);
            if (empty($userdata)) {
                $this->Session->setFlash(__('Invalid user name or password'));
            } else {
                if ($userdata['User']['id'] != 1 && $userdata['User']['disable']) {
                    $this->Session->setFlash(__('Your account is disabled'));
                } else {
                    if ($userdata['User']['passwordchangecode'] != '') {
                        // Clear password change code (if there is any)
                        $userdata['User']['passwordchangecode'] = '';
                    }
                    $userdata['User']['last_login_ip'] = $this->request->clientIp();
                    $userdata['User']['last_login_time'] = date('Y-m-d H:i:s');
                    $this->User->save($userdata);

                    $this->Authen->login($userdata['User']);
                    if (($next = $this->Authen->getPreviousUrl()) !== null) {
                        $this->Authen->setPreviousUrl(null);
                        $this->redirect($next);
                    } else {
                        $this->redirect(Configure::read('Authen.loggedAction'));
                    }
                }
            }
        }
    }

    function logout()
    {
        $this->layout = 'Admin.user';

        if ($this->Authen->isLogged()) {
            $this->Authen->logout();
        }
        $this->redirect('/');
    }

    function forgotpassword()
    {
        $this->layout = 'Admin.user';

        if ($this->Authen->isLogged()) {
            $this->redirect(Configure::read('Authen.loggedAction'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $email = $this->request->data['User']['email'];
            if (!empty($email)) {
                $user = $this->User->findByEmail($email);
                if (!empty($user)) {
                    $user['User']['passwordchangecode'] = uniqid() . time();
                    if ($this->User->save($user['User'])) {
                        $code = $user['User']['passwordchangecode'];

                        $emails = $this->Site->get('site_emails');
                        $emails = empty($emails) ? null : explode(',', $emails);

                        App::uses('CakeEmail', 'Network/Email');
                        $email = new CakeEmail('default');
                        $email->to($user['User']['email']);
                        if (!empty($emails)) {
                            $email->bcc($emails);
                        }
                        $email->emailFormat('html');
                        $email->template('Admin.lost_password');
                        $email->viewVars(array('email' => $user['User']['email'], 'code' => $code));
                        $email->subject(__('Request changing password'));
                        try {
                            if ($email->send()) {
                                $this->Session->setFlash(__('Your request was accepted. Please check your email for more instruction'), 'default', array('class' => 'success'));
                            } else {
                                $this->Session->setFlash(__('Can\'t send email. Please try again'));
                            }
                        } catch (Exception $e) {
                            $this->Session->setFlash(__('Can\'t send email. Please try again'));
                        }
                    } else {
                        $this->Session->setFlash(__('Can\'t save your request. Please try again'));
                    }
                } else {
                    $this->Session->setFlash(__('Account with your email doesn\'t exist. Please try again'));
                }
            } else {
                $this->Session->setFlash(__('Email is invalid. Please try again'));
            }
        }
    }

    function changepassword($passwordchangecode = null)
    {
        $this->layout = 'Admin.user';

        if ($this->Authen->isLogged()) {
            $this->redirect(Configure::read('Authen.loggedAction'));
        }

        if (empty($passwordchangecode)) {
            $this->Session->setFlash(__('Your code is invalid'));
            $this->redirect(Configure::read('Authen.loginAction'));
        }

        $user = $this->User->find('first', array('conditions' => array('passwordchangecode' => $passwordchangecode)));
        if (empty($user)) {
            $this->Session->setFlash(__('Your code is invalid'));
            $this->redirect(Configure::read('Authen.loginAction'));
        } else {
            $this->set('passwordchangecode', $passwordchangecode);
            if ($this->request->is('post') || $this->request->is('put')) {
                if (empty($this->request->data['User']['password']) || $this->request->data['User']['password'] != $this->request->data['User']['confirm_password']) {
                    $this->Session->setFlash(__('The passwords do not match'));
                } else {
                    $user['User']['password'] = $this->request->data['User']['password'];
                    if ($this->User->save($user)) {
                        //Update password. Make md5 for password
                        $user['User']['password'] = md5($this->request->data['User']['password']);
                        $this->User->save($user);

                        $this->Session->setFlash(__('Change password successfully. You may log in now'), 'default', array('class' => 'success'));
                        $this->redirect(Configure::read('Authen.loginAction'));
                    } else {
                        $this->Session->setFlash(__('Can not change your password. Please try again'));
                    }
                }
            }
        }
    }
}

?>