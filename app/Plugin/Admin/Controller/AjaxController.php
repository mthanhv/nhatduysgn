<?php

class AjaxController extends AdminAppController
{
    function menuItems($menu_id = null)
    {
        $this->autoRender = false;

        $this->loadModel('MenuItem');
        $menu_items = $this->MenuItem->generateTreeList(array('menu_id' => $menu_id), null, null, '----');
        $menu_items = array(0 => __('Select parent menu')) + $menu_items;

        echo json_encode($menu_items);
    }

    function linkTo($category_id = null)
    {
        $this->autoRender = false;

        $data = array();
        if ($category_id == LINK_TO_HOME) {
            $data = array(0 => __('Select data')) + $data;
            // Do no thing
        } else if ($category_id == LINK_TO_PRODUCT_LIST) {
            Controller::loadModel('ProductCategory');
            $data = $this->ProductCategory->generateTreeList(null, null, null, '----');
            $data = array(0 => __('Select data')) + $data;
        } else if ($category_id == LINK_TO_PRODUCT) {
            Controller::loadModel('Product');
            $data = $this->Product->find('list', array('fields' => array('Product.id', 'Product.title'), 'order' => array('Product.id' => 'DESC')));
            $data = array(0 => __('Select data')) + $data;
        } else if ($category_id == LINK_TO_ARTICLE_LIST) {
            Controller::loadModel('ArticleCategory');
            $data = $this->ArticleCategory->find('list', array('fields' => array('ArticleCategory.id', 'ArticleCategory.name'), 'order' => array('ArticleCategory.id' => 'DESC')));
            $data = array(0 => __('Select data')) + $data;
        } else if ($category_id == LINK_TO_ARTICLE) {
            Controller::loadModel('Article');
            $data = $this->Article->find('list', array('fields' => array('Article.id', 'Article.title'), 'order' => array('Article.id' => 'DESC')));
            $data = array(0 => __('Select data')) + $data;
        } else if ($category_id == LINK_TO_CUSTOM) {
            $data = array(0 => __('Select data')) + $data;
            // Do no thing
        }

        echo json_encode($data);
    }
}

?>