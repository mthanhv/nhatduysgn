<?php

class SlidesController extends AdminAppController
{
    var $uses = 'Slide';
    var $paginate = array('order' => 'Slide.id DESC');

    function index()
    {
        $this->set('slides', $this->paginate('Slide'));
    }

    function add()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $add_and_new = isset($this->request->data['FormAction']['add_and_new']) ? true : false;
            unset($this->request->data['FormAction']);

            $this->Slide->create();
            $upload_options = array(
                'location' => 'files/slides/',
                'check_exists' => true,
                'generate_unique_name' => false,
                'clean_name' => true,
                'name' => null,
                'ext' => null,
                'thumbs' => array(
                    0 => array(
                        'location' => 'files/slides/thumbs/',
                        'check_exists' => true,
                        'generate_unique_name' => false,
                        'use_master_file_name' => true,
                        'clean_name' => false,
                        'name' => null,
                        'ext' => null,
                        'width' => SLIDE_THUMB2_W,
                        'height' => SLIDE_THUMB2_H,
                    )
                )
            );
            $data = $this->Utility->upload_file($this->request->data['Slide']['image'], $upload_options);
            if (is_array($data) && isset($data['error']) && $data['error'] == false) {
                $this->request->data['Slide']['image'] = "{$data['data']['name']}.{$data['data']['ext']}";
                if ($this->Slide->save($this->request->data)) {
                    $this->Session->setFlash(__('The slide has been saved'), 'default', array('class' => 'success'));
                    if ($add_and_new) $this->redirect(array('action' => 'add'));
                    else $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The slide could not be saved. Please, try again'));
                }
            } else {
                $this->Session->setFlash(__('The slide could not be saved. Please, try again'));
                $this->Slide->validationErrors['image'][] = $data['data'];
            }
        }
    }

    function edit($id = null)
    {
        $this->Slide->id = $id;
        if (!$this->Slide->exists()) {
            throw new NotFoundException(__('Invalid slide'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            // Dont choose image
            if (isset($this->request->data['Slide']['image']['error']) == false || $this->request->data['Slide']['image']['error'] == UPLOAD_ERR_NO_FILE) {
                unset($this->request->data['Slide']['image']);
                if ($this->Slide->save($this->request->data)) {
                    $this->Session->setFlash(__('The slide has been saved'), 'default', array('class' => 'success'));
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('Slide could not be saved . Please try again'));
                }
            } else {
                $upload_options = array(
                    'location' => 'files/slides/',
                    'check_exists' => true,
                    'generate_unique_name' => false,
                    'clean_name' => true,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array(
                        0 => array(
                            'location' => 'files/slides/thumbs/',
                            'check_exists' => true,
                            'generate_unique_name' => false,
                            'use_master_file_name' => true,
                            'clean_name' => false,
                            'name' => null,
                            'ext' => null,
                            'width' => SLIDE_THUMB2_W,
                            'height' => SLIDE_THUMB2_H,
                        )
                    )
                );
                $data = $this->Utility->upload_file($this->request->data['Slide']['image'], $upload_options);
                if (is_array($data) && isset($data['error']) && $data['error'] == false) {
                    $this->request->data['Slide']['image'] = "{$data['data']['name']}.{$data['data']['ext']}";
                    if ($this->Slide->save($this->request->data)) {
                        $this->Session->setFlash(__('The slide has been saved'), 'default', array('class' => 'success'));
                        $this->redirect(array('action' => 'index'));
                    } else {
                        $this->Session->setFlash(__('Slide could not be saved . Please try again'));
                    }
                } else {
                    $this->Session->setFlash(__('The slide could not be saved. Please, try again'));
                    $this->Slide->validationErrors['image'][] = $data['data'];
                }
            }
        } else {
            $this->request->data = $this->Slide->read(null, $id);
        }
    }

    function delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $this->Slide->id = $id;
        if (!$this->Slide->exists()) {
            throw new NotFoundException(__('Invalid slide'));
        }

        if ($this->Slide->delete($id)) {
            $this->Session->setFlash(__('Slide was deleted'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('Slide could not be deleted. Please try again'));
            $this->redirect(array('action' => 'index'));
        }
    }

    function deletemulti()
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $ids = isset($this->data['markedvalues']) ? explode(',', $this->data['markedvalues']) : '';
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->Slide->id = $id;
                if (!$id || !$this->Slide->exists()) continue;
                $this->Slide->delete($id);
            }
        }

        $this->Session->setFlash(__('Slides were deleted'));
        $this->redirect(array('action' => 'index'));
    }
}

?>