<?php

class ContactsController extends AdminAppController
{
    var $uses = 'Contact';
    var $paginate = array('order' => 'Contact.id DESC');

    function index()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $url = array('action' => 'index');
            if (!empty($this->request->data['Contact']['key'])) {
                $url['key'] = $this->Utility->safe_b64encode($this->request->data['Contact']['key']);
            }
            $this->redirect($url);
        }
        $key = isset($this->request->params['named']['key']) ? $this->Utility->safe_b64decode($this->request->params['named']['key']) : '';
        $key_escaped = str_replace(array('%', '_'), array('\%', '\_'), $key);

        $conditions = array();
        if (!empty($key)) {
            $conditions['OR'] = array('Contact.name LIKE' => "%{$key_escaped}%", 'Contact.title LIKE' => "%{$key_escaped}%");

            //Pass information for view
            $this->request->data['Contact']['key'] = $key;
        }

        $this->set('contacts', $this->paginate('Contact', $conditions));
    }

    function view($id = null)
    {
        $this->Contact->id = $id;
        if (!$this->Contact->exists()) {
            throw new NotFoundException(__('Invalid contact'));
        }

        $contact = $this->Contact->read(null, $id);
        $this->set('contact', $contact);
    }

    function delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $this->Contact->id = $id;
        if (!$this->Contact->exists()) {
            throw new NotFoundException(__('Invalid contact'));
        }

        if ($this->Contact->delete($id)) {
            $this->Session->setFlash(__('Contact was deleted'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('Contact could not be deleted. Please try again'));
            $this->redirect(array('action' => 'index'));
        }
    }

    function deletemulti()
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $ids = isset($this->data['markedvalues']) ? explode(',', $this->data['markedvalues']) : '';
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->Contact->id = $id;
                if (!$id || !$this->Contact->exists()) continue;
                $this->Contact->delete($id);
            }
        }

        $this->Session->setFlash(__('Contacts were deleted'));
        $this->redirect(array('action' => 'index'));
    }
}

?>