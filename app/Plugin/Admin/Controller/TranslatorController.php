<?php

class TranslatorController extends AdminAppController
{
    function index()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $url = array('action' => 'index');
            if (!empty($this->request->data['Translation']['model'])) $url['model'] = $this->request->data['Translation']['model'];
            if (!empty($this->request->data['Translation']['from'])) $url['from'] = $this->request->data['Translation']['from'];
            if (!empty($this->request->data['Translation']['to'])) $url['to'] = $this->request->data['Translation']['to'];
            $this->redirect($url);
        }

        $model = isset($this->request->params['named']['model']) ? $this->request->params['named']['model'] : '';
        $from = isset($this->request->params['named']['from']) ? $this->request->params['named']['from'] : '';
        $to = isset($this->request->params['named']['to']) ? $this->request->params['named']['to'] : '';

        if (!empty($model)) {
            $this->set('model', $model);
            $this->request->data['Translation']['model'] = $model;
        }
        if (!empty($from)) {
            $this->set('from', $from);
            $this->request->data['Translation']['from'] = $from;
        }
        if (!empty($to)) {
            $this->set('to', $to);
            $this->request->data['Translation']['to'] = $to;
        }

        $datas = array();
        if (!empty($model) && !empty($from) && !empty($to)) {
            $this->loadModel($model);
            $this->$model->locale = $from;

            $primaryKey = $this->$model->primaryKey;
            $displayField = $this->$model->displayField;
            $this->set('primaryKey', $primaryKey);
            $this->set('displayField', $displayField);
            $this->paginate = array(
                $model => array(
                    'recursive' => -1,
                    'fields' => array($primaryKey, $displayField)
                )
            );
            $datas = $this->paginate($model);
        }
        $this->set('datas', $datas);

        $translation_models = array('Article' => __('Article'), 'MenuItem' => __('MenuItem'), 'Product' => __('Product'), 'Block' => __('Block'), 'Setting' => __('Setting'));
        $this->set('translation_models', $translation_models);
        $this->set('translation_from', Configure::read('AVAILABLE_LANGUAGES_FROM'));
        $this->set('translation_to', Configure::read('AVAILABLE_LANGUAGES_TO'));
    }

    function translate($id = null)
    {
        $model = isset($this->request->params['named']['model']) ? $this->request->params['named']['model'] : '';
        $from = isset($this->request->params['named']['from']) ? $this->request->params['named']['from'] : '';
        $to = isset($this->request->params['named']['to']) ? $this->request->params['named']['to'] : '';

        if (!empty($id) && !empty($model) && !empty($from) && !empty($to)) {
            $this->loadModel($model);
            $this->$model->id = $id;
            if (!$this->$model->exists()) {
                throw new NotFoundException(__('Invalid data'));
            }

            $this->set('model', $model);
            $this->set('from', $from);
            $this->set('to', $to);
            $this->set('translate_url', $this->$model->getTranslationUrl($id, $from, $to));
        } else {
            throw new NotFoundException(__('Invalid data'));
        }
    }

    function translated($model, $language, $id)
    {
        $this->autoRender = false;

        $translated = 0;
        if (!empty($model) && !empty($language) && !empty($id)) {
            $this->loadModel($model);
            $this->$model->locale = $language;
            if ($this->$model->find('count', array('conditions' => array("{$model}.{$this->$model->primaryKey}" => $id))) > 0) {
                $translated = 1;
            }
        }

        header('Content-type: image/png');
        header('Cache-Control: public, no-cache');
        echo file_get_contents(WWW_ROOT . "images/{$translated}.png");
        exit();
    }
}

?>