<?php

class ArticlesController extends AdminAppController
{
    var $uses = 'Article';
    var $paginate = array('order' => 'Article.id DESC');

    function index()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $url = array('action' => 'index');
            if (!empty($this->request->data['Article']['key'])) {
                $url['key'] = $this->Utility->safe_b64encode($this->request->data['Article']['key']);
            }
            $this->redirect($url);
        }
        $key = isset($this->request->params['named']['key']) ? $this->Utility->safe_b64decode($this->request->params['named']['key']) : '';
        $key_escaped = str_replace(array('%', '_'), array('\%', '\_'), $key);

        $conditions = array();
        if (!empty($key)) {
            $conditions['OR'] = array('Article.title LIKE' => "%{$key_escaped}%");

            //Pass information for view
            $this->request->data['Article']['key'] = $key;
        }

        $this->Article->loadRelation('belongsTo', 'ArticleCategory', false);
        $this->set('articles', $this->paginate('Article', $conditions));
    }

    function add()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $add_and_new = isset($this->request->data['FormAction']['add_and_new']) ? true : false;
            unset($this->request->data['FormAction']);

            $this->Article->create();
            $upload_options = array(
                'location' => 'files/articles/',
                'check_exists' => true,
                'generate_unique_name' => false,
                'clean_name' => true,
                'name' => null,
                'ext' => null,
                'thumbs' => array(
                    0 => array(
                        'location' => 'files/articles/thumbs/',
                        'check_exists' => true,
                        'generate_unique_name' => false,
                        'use_master_file_name' => true,
                        'clean_name' => false,
                        'name' => null,
                        'ext' => null,
                        'width' => ARTICLE_THUMB1_W,
                        'height' => 0,
                    )
                )
            );
            $data = $this->Utility->upload_file($this->request->data['Article']['image'], $upload_options);
            if (is_array($data) && isset($data['error']) && $data['error'] == false) {
                $this->request->data['Article']['image'] = "{$data['data']['name']}.{$data['data']['ext']}";
                if ($this->Article->save($this->request->data)) {
                    $this->Session->setFlash(__('The article has been saved'), 'default', array('class' => 'success'));
                    if ($add_and_new) $this->redirect(array('action' => 'add'));
                    else $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The article could not be saved. Please, try again'));
                }
            } else {
                $this->Session->setFlash(__('The article could not be saved. Please, try again'));
                $this->Article->validationErrors['image'][] = $data['data'];
            }
        }

        $this->Article->loadRelation('belongsTo', 'ArticleCategory', false);
        $article_categories = $this->Article->ArticleCategory->find('list');
        $this->set(compact('article_categories'));
    }

    function edit($id = null)
    {
        $this->Article->id = $id;
        if (!$this->Article->exists()) {
            throw new NotFoundException(__('Invalid article'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            // Dont choose image
            if (isset($this->request->data['Article']['image']['error']) == false || $this->request->data['Article']['image']['error'] == UPLOAD_ERR_NO_FILE) {
                unset($this->request->data['Article']['image']);
                if ($this->Article->save($this->request->data)) {
                    $this->Session->setFlash(__('The article has been saved'), 'default', array('class' => 'success'));
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('Article could not be saved . Please try again'));
                }
            } else {
                $upload_options = array(
                    'location' => 'files/articles/',
                    'check_exists' => true,
                    'generate_unique_name' => false,
                    'clean_name' => true,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array(
                        0 => array(
                            'location' => 'files/articles/thumbs/',
                            'check_exists' => true,
                            'generate_unique_name' => false,
                            'use_master_file_name' => true,
                            'clean_name' => false,
                            'name' => null,
                            'ext' => null,
                            'width' => ARTICLE_THUMB1_W,
                            'height' => 0,
                        )
                    )
                );
                $data = $this->Utility->upload_file($this->request->data['Article']['image'], $upload_options);
                if (is_array($data) && isset($data['error']) && $data['error'] == false) {
                    $this->request->data['Article']['image'] = "{$data['data']['name']}.{$data['data']['ext']}";
                    if ($this->Article->save($this->request->data)) {
                        $this->Session->setFlash(__('The article has been saved'), 'default', array('class' => 'success'));
                        $this->redirect(array('action' => 'index'));
                    } else {
                        $this->Session->setFlash(__('Article could not be saved . Please try again'));
                    }
                } else {
                    $this->Session->setFlash(__('The article could not be saved. Please, try again'));
                    $this->Article->validationErrors['image'][] = $data['data'];
                }
            }
        } else {
            $this->request->data = $this->Article->read(null, $id);
        }

        $this->Article->loadRelation('belongsTo', 'ArticleCategory', false);
        $article_categories = $this->Article->ArticleCategory->find('list');
        $this->set(compact('article_categories'));
    }

    function delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $this->Article->id = $id;
        if (!$this->Article->exists()) {
            throw new NotFoundException(__('Invalid article'));
        }

        if ($this->Article->delete($id)) {
            $this->Session->setFlash(__('Article was deleted'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('Article could not be deleted. Please try again'));
            $this->redirect(array('action' => 'index'));
        }
    }

    function deletemulti()
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $ids = isset($this->data['markedvalues']) ? explode(',', $this->data['markedvalues']) : '';
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->Article->id = $id;
                if (!$id || !$this->Article->exists()) continue;
                $this->Article->delete($id);
            }
        }

        $this->Session->setFlash(__('Articles were deleted'));
        $this->redirect(array('action' => 'index'));
    }

    function translate($id = null)
    {
        $this->layout = 'translation';

        $from = isset($this->request->params['named']['from']) ? $this->request->params['named']['from'] : '';
        $to = isset($this->request->params['named']['to']) ? $this->request->params['named']['to'] : '';
        $this->set('from', $from);
        $this->set('to', $to);

        $this->Article->id = $id;
        $this->Article->locale = $from;
        if (!$this->Article->exists()) {
            throw new NotFoundException(__('Invalid article'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $locale = $this->request->data['Article']['locale'];
            $request_data = $this->request->data[$locale];

            // Dont choose image
            if (isset($request_data['Article']['image']['error']) == false || $request_data['Article']['image']['error'] == UPLOAD_ERR_NO_FILE) {
                unset($request_data['Article']['image']);
                $this->Article->locale = $locale;
                if ($this->Article->save($request_data)) {
                    $this->Session->setFlash(__('The article has been saved'), 'default', array('class' => 'success'));
                    $this->redirect($this->referer());
                } else {
                    $this->Session->setFlash(__('Article could not be saved . Please try again'));
                }
            } else {
                $upload_options = array(
                    'location' => 'files/articles/',
                    'check_exists' => true,
                    'generate_unique_name' => false,
                    'clean_name' => true,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array(
                        0 => array(
                            'location' => 'files/articles/thumbs/',
                            'check_exists' => true,
                            'generate_unique_name' => false,
                            'use_master_file_name' => true,
                            'clean_name' => false,
                            'name' => null,
                            'ext' => null,
                            'width' => ARTICLE_THUMB1_W,
                            'height' => 0,
                        )
                    )
                );
                $data = $this->Utility->upload_file($request_data['Article']['image'], $upload_options);
                if (is_array($data) && isset($data['error']) && $data['error'] == false) {
                    $request_data['Article']['image'] = "{$data['data']['name']}.{$data['data']['ext']}";
                    $this->Article->locale = $locale;
                    if ($this->Article->save($request_data)) {
                        $this->Session->setFlash(__('The article has been saved'), 'default', array('class' => 'success'));
                        $this->redirect($this->referer());
                    } else {
                        $this->Session->setFlash(__('Article could not be saved . Please try again'));
                    }
                } else {
                    $this->Session->setFlash(__('The article could not be saved. Please, try again'));
                    $this->Article->validationErrors['image'][] = $data['data'];
                }
            }
        } else {
            $this->Article->locale = $from;
            $this->request->data[$from] = $this->Article->read(null, $id);
            $this->Article->locale = $to;
            $this->request->data[$to] = $this->Article->read(null, $id);

            if (empty($this->request->data[$to])) $this->request->data[$to] = $this->request->data[$from];
        }

        $this->Article->loadRelation('belongsTo', 'ArticleCategory', false);
        $article_categories = $this->Article->ArticleCategory->find('list');
        $this->set(compact('article_categories'));
    }
}

?>