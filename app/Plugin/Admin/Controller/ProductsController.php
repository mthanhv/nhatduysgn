<?php

class ProductsController extends AdminAppController
{
    var $uses = 'Product';
    var $paginate = array('order' => 'Product.id DESC');

    function index()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $url = array('action' => 'index');
            if (!empty($this->request->data['Product']['key'])) {
                $url['key'] = $this->Utility->safe_b64encode($this->request->data['Product']['key']);
            }
            $this->redirect($url);
        }
        $key = isset($this->request->params['named']['key']) ? $this->Utility->safe_b64decode($this->request->params['named']['key']) : '';
        $key_escaped = str_replace(array('%', '_'), array('\%', '\_'), $key);

        $conditions = array();
        if (!empty($key)) {
            $conditions['OR'] = array('Product.title LIKE' => "%{$key_escaped}%");

            //Pass information for view
            $this->request->data['Product']['key'] = $key;
        }

        $this->paginate = array(
            'Product' => array(
                'order' => 'Product.id DESC',
                'fields' => array('Product.id', 'Product.image', 'Product.title', 'Product.attachment', 'Product.created', 'ProductCategory.name', 'Product.publish', 'Product.ordering')
            )
        );

        $this->Product->loadRelation('belongsTo', 'ProductCategory', false);
        $this->set('products', $this->paginate('Product', $conditions));
    }

    function add()
    {
        $this->Product->loadRelation('hasAndBelongsToMany', 'Tag', false);

        if ($this->request->is('post') || $this->request->is('put')) {
            $add_and_new = isset($this->request->data['FormAction']['add_and_new']) ? true : false;
            unset($this->request->data['FormAction']);

            $choosed_thumb = (isset($this->request->data['Product']['image']['error']) && $this->request->data['Product']['image']['error'] != UPLOAD_ERR_NO_FILE);
            $choosed_attachment = (isset($this->request->data['Product']['attachment']['error']) && $this->request->data['Product']['attachment']['error'] != UPLOAD_ERR_NO_FILE);

            if ($choosed_thumb) {
                $upload_options = array(
                    'location' => 'files/products/',
                    'check_exists' => true,
                    'generate_unique_name' => false,
                    'clean_name' => true,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array(
                        0 => array(
                            'location' => 'files/products/thumbs/',
                            'check_exists' => true,
                            'generate_unique_name' => false,
                            'use_master_file_name' => true,
                            'clean_name' => false,
                            'name' => null,
                            'ext' => null,
                            'width' => 268,
                            'height' => 249,
                        )
                    )
                );
                $thumb_data = $this->Utility->upload_file($this->request->data['Product']['image'], $upload_options);
                if (is_array($thumb_data) && isset($thumb_data['error']) && $thumb_data['error'] == false) {
                    $this->request->data['Product']['image'] = "{$thumb_data['data']['name']}.{$thumb_data['data']['ext']}";
                } else {
                    $this->Product->validationErrors['image'][] = $thumb_data['data'];
                }
            } else {
                $this->request->data['Product']['image'] = '';
            }

            if ($choosed_attachment) {
                $attachment_upload_options = array(
                    'location' => "files/products/attachments/",
                    'check_exists' => true,
                    'generate_unique_name' => false,
                    'clean_name' => true,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array()
                );

                //Upload attachments
                $attachment_image_data = $this->Utility->upload_file($this->request->data['Product']['attachment'], $attachment_upload_options);
                if (is_array($attachment_image_data) && isset($attachment_image_data['error']) && $attachment_image_data['error'] == false) {
                    $this->request->data['Product']['attachment'] = "{$attachment_image_data['data']['name']}.{$attachment_image_data['data']['ext']}";
                } else {
                    $this->Product->validationErrors['attachment'][] = $attachment_image_data['data'];
                }
            } else {
                $this->request->data['Product']['attachment'] = '';
            }

            if (!empty($this->request->data['Product']['tags'])) {
                $tags = explode(',', $this->request->data['Product']['tags']);
                foreach ($tags as $tag) {
                    if (!empty($tag)) {
                        $cleanTags[] = trim($tag);
                    }
                }
                if (!empty($cleanTags)) {
                    $this->loadModel('Tag');
                    foreach ($cleanTags as $cleanTag) {
                        $tagData = $this->Tag->find('first', array('conditions' => array('title' => $cleanTag), 'recursive' => -1));
                        if (empty($tagData)) {
                            $this->Tag->create();
                            $this->Tag->save(array('title' => $cleanTag));
                            $tagId = $this->Tag->getLastInsertID();
                        } else {
                            $tagId = $tagData['Tag']['id'];
                        }
                        if (!empty($tagId)) {
                            $this->request->data['Tag']['Tag'][] = $tagId;
                        }
                    }
                }
            }

            if (empty($this->Product->validationErrors) && $this->Product->save($this->request->data)) {
                $this->Session->setFlash(__('The product has been saved'), 'default', array('class' => 'success'));
                if ($add_and_new) $this->redirect(array('action' => 'add'));
                else $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Product could not be saved . Please try again'));
            }
        }

        $this->Product->loadRelation('belongsTo', 'ProductCategory', false);
        $product_categories = $this->Product->ProductCategory->generateTreeList(null, null, null, '----');
        $this->set(compact('product_categories'));

        $this->Product->loadRelation('belongsTo', 'Brand', false);
        $brands = $this->Product->Brand->find('list', ['fields' => ['id', 'title']]);
        $this->set('brands', $brands);

        $this->Product->loadRelation('belongsTo', 'Supplier', false);
        $suppliers = $this->Product->Supplier->find('list', ['fields' => ['id', 'title']]);
        $this->set('suppliers', $suppliers);
    }

    function edit($id = null)
    {
        $this->Product->loadRelation('hasAndBelongsToMany', 'Tag', false);

        $this->Product->id = $id;
        if (!$this->Product->exists()) {
            throw new NotFoundException(__('Invalid product'));
        }

        $product = $this->Product->read(null, $id);

        if ($this->request->is('post') || $this->request->is('put')) {
            $choosed_thumb = (isset($this->request->data['Product']['image']['error']) && $this->request->data['Product']['image']['error'] != UPLOAD_ERR_NO_FILE);
            $choosed_attachment = (isset($this->request->data['Product']['attachment']['error']) && $this->request->data['Product']['attachment']['error'] != UPLOAD_ERR_NO_FILE);

            if ($choosed_thumb) {
                $upload_options = array(
                    'location' => 'files/products/',
                    'check_exists' => true,
                    'generate_unique_name' => false,
                    'clean_name' => true,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array(
                        0 => array(
                            'location' => 'files/products/thumbs/',
                            'check_exists' => true,
                            'generate_unique_name' => false,
                            'use_master_file_name' => true,
                            'clean_name' => false,
                            'name' => null,
                            'ext' => null,
                            'width' => 268,
                            'height' => 249,
                        )
                    )
                );
                $thumb_data = $this->Utility->upload_file($this->request->data['Product']['image'], $upload_options);
                if (is_array($thumb_data) && isset($thumb_data['error']) && $thumb_data['error'] == false) {
                    $this->request->data['Product']['image'] = "{$thumb_data['data']['name']}.{$thumb_data['data']['ext']}";
                } else {
                    $this->Product->validationErrors['image'][] = $thumb_data['data'];
                }
            } else {
                unset($this->request->data['Product']['image']);
            }

            if ($choosed_attachment) {
                $attachment_upload_options = array(
                    'location' => "files/products/attachments/",
                    'check_exists' => true,
                    'generate_unique_name' => false,
                    'clean_name' => true,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array()
                );

                //Upload attachments
                $attachment_image_data = $this->Utility->upload_file($this->request->data['Product']['attachment'], $attachment_upload_options);
                if (is_array($attachment_image_data) && isset($attachment_image_data['error']) && $attachment_image_data['error'] == false) {
                    $this->request->data['Product']['attachment'] = "{$attachment_image_data['data']['name']}.{$attachment_image_data['data']['ext']}";
                } else {
                    $this->Product->validationErrors['attachment'][] = $attachment_image_data['data'];
                    $this->request->data['Product']['attachment'] = $product['Product']['attachment'];
                }
            } else {
                unset($this->request->data['Product']['attachment']);
            }

            if (!empty($this->request->data['Product']['tags'])) {
                $tags = explode(',', $this->request->data['Product']['tags']);
                foreach ($tags as $tag) {
                    if (!empty($tag)) {
                        $cleanTags[] = trim($tag);
                    }
                }
                if (!empty($cleanTags)) {
                    $this->loadModel('Tag');
                    foreach ($cleanTags as $cleanTag) {
                        $tagData = $this->Tag->find('first', array('conditions' => array('title' => $cleanTag), 'recursive' => -1));
                        if (empty($tagData)) {
                            $this->Tag->create();
                            $this->Tag->save(array('title' => $cleanTag));
                            $tagId = $this->Tag->getLastInsertID();
                        } else {
                            $tagId = $tagData['Tag']['id'];
                        }
                        if (!empty($tagId)) {
                            $this->request->data['Tag']['Tag'][] = $tagId;
                        }
                    }
                }
            }

            if (empty($this->Product->validationErrors) && $this->Product->save($this->request->data)) {
                $this->Session->setFlash(__('The product has been saved'), 'default', array('class' => 'success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Product could not be saved . Please try again'));
            }
        } else {
            $this->request->data = $product;

            //Load tags
            if (!empty($this->request->data['Tag'])) {
                $tags = array_map(function ($arr) {
                    return $arr['title'];
                }, $this->request->data['Tag']);
                $this->request->data['Product']['tags'] = implode(', ', $tags);
            }

        }

        $this->Product->loadRelation('belongsTo', 'ProductCategory', false);
        $product_categories = $this->Product->ProductCategory->generateTreeList(null, null, null, '----');
        $this->set(compact('product_categories'));

        $this->Product->loadRelation('belongsTo', 'Brand', false);
        $brands = $this->Product->Brand->find('list', ['fields' => ['id', 'title']]);
        $this->set('brands', $brands);

        $this->Product->loadRelation('belongsTo', 'Supplier', false);
        $suppliers = $this->Product->Supplier->find('list', ['fields' => ['id', 'title']]);
        $this->set('suppliers', $suppliers);
    }

    function delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $this->Product->id = $id;
        if (!$this->Product->exists()) {
            throw new NotFoundException(__('Invalid product'));
        }

        $this->Product->loadRelation('hasAndBelongsToMany', 'Tag', false);

        if ($this->Product->delete($id)) {
            $this->Session->setFlash(__('Product was deleted'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('Product could not be deleted. Please try again'));
            $this->redirect(array('action' => 'index'));
        }
    }

    function deletemulti()
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $ids = isset($this->data['markedvalues']) ? explode(',', $this->data['markedvalues']) : '';
        if (!empty($ids)) {
            $this->Product->loadRelation('hasAndBelongsToMany', 'Tag', false);

            foreach ($ids as $id) {
                $this->Product->id = $id;
                if (!$id || !$this->Product->exists()) continue;
                $this->Product->delete($id);
            }
        }

        $this->Session->setFlash(__('Products were deleted'));
        $this->redirect(array('action' => 'index'));
    }

    function clonemulti()
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $ids = isset($this->data['markedvalues']) ? explode(',', $this->data['markedvalues']) : '';
        if (!empty($ids)) {
            $products = array();
            foreach ($ids as $id) {
                $this->Product->id = $id;
                if (!$id || !$this->Product->exists()) continue;
                $product = $this->Product->read(null, $id);
                $product['Product']['title'] = __('Copy of %s', $product['Product']['title']);
                unset($product['Product']['id']);
                unset($product['Product']['view_count']);
                unset($product['Product']['created']);
                unset($product['Product']['updated']);
                unset($product['Product']['attachment']);
                $products[] = $product;
            }

            if ($this->Product->saveMany($products)) {
                $this->Session->setFlash(__('Products were cloned'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Products were not cloned successfully'));
                $this->redirect(array('action' => 'index'));
            }
        }

        $this->Session->setFlash(__('Products were cloned'));
        $this->redirect(array('action' => 'index'));
    }

    function toggle($id = null)
    {
        $this->Product->id = $id;
        $product = $this->Product->find('first', array('conditions' => array('Product.id' => $id), 'recursive' => -1));
        if (!$product) {
            throw new NotFoundException(__('Invalid product'));
        }

        $product['Product']['publish'] = (($product['Product']['publish'] == PUBLISHED) ? UNPUBLISHED : PUBLISHED);
        $this->Product->save($product);

        $this->redirect(array('action' => 'index') + $this->request->params['named']);
    }

    function saveordering()
    {
        $this->autoRender = false;

        $result = array('error' => 0, 'message' => '');
        $orderings = empty($this->request->data['orderings']) ? array() : $this->request->data['orderings'];

        $datas = array();
        foreach ($orderings as $ordering) {
            $datas[] = array('id' => $ordering['proid'], 'ordering' => $ordering['ordering']);
        }

        if (!empty($datas)) {
            if ($this->Product->saveMany($datas)) {
                $result = array('error' => 0, 'message' => '');
            } else {
                $result = array('error' => 1, 'message' => json_encode($this->Product->validationErrors));
            }
        }

        echo json_encode($result);
    }

    function translate($id = null)
    {
        $this->layout = 'translation';

        $from = isset($this->request->params['named']['from']) ? $this->request->params['named']['from'] : '';
        $to = isset($this->request->params['named']['to']) ? $this->request->params['named']['to'] : '';
        $this->set('from', $from);
        $this->set('to', $to);

        $this->Product->id = $id;
        $this->Product->locale = $from;
        if (!$this->Product->exists()) {
            throw new NotFoundException(__('Invalid product'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $locale = $this->request->data['Product']['locale'];
            $request_data = $this->request->data[$locale];

            $choosed_thumb = (isset($request_data['Product']['image']['error']) && $request_data['Product']['image']['error'] != UPLOAD_ERR_NO_FILE);
            $choosed_attachment = (isset($request_data['Product']['attachment']['error']) && $request_data['Product']['attachment']['error'] != UPLOAD_ERR_NO_FILE);

            if ($choosed_thumb) {
                $upload_options = array(
                    'location' => 'files/products/',
                    'check_exists' => true,
                    'generate_unique_name' => false,
                    'clean_name' => true,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array(
                        0 => array(
                            'location' => 'files/products/thumbs/',
                            'check_exists' => true,
                            'generate_unique_name' => false,
                            'use_master_file_name' => true,
                            'clean_name' => false,
                            'name' => null,
                            'ext' => null,
                            'width' => 268,
                            'height' => 249,
                        )
                    )
                );
                $thumb_data = $this->Utility->upload_file($request_data['Product']['image'], $upload_options);
                if (is_array($thumb_data) && isset($thumb_data['error']) && $thumb_data['error'] == false) {
                    $request_data['Product']['image'] = "{$thumb_data['data']['name']}.{$thumb_data['data']['ext']}";
                } else {
                    $this->Product->validationErrors['image'][] = $thumb_data['data'];
                }
            } else {
                unset($request_data['Product']['image']);
            }

            if ($choosed_attachment) {
                $attachment_upload_options = array(
                    'location' => "files/products/attachments/",
                    'check_exists' => true,
                    'generate_unique_name' => false,
                    'clean_name' => true,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array()
                );

                //Upload attachments
                $attachment_image_data = $this->Utility->upload_file($this->request->data['Product']['attachment'], $attachment_upload_options);
                if (is_array($attachment_image_data) && isset($attachment_image_data['error']) && $attachment_image_data['error'] == false) {
                    $this->request->data['Product']['attachment'] = "{$attachment_image_data['data']['name']}.{$attachment_image_data['data']['ext']}";
                } else {
                    $this->Product->validationErrors['attachment'][] = $attachment_image_data['data'];
                }
            } else {
                unset($request_data['Product']['attachment']);
            }

            $this->Product->locale = $locale;
            if (empty($this->Product->validationErrors) && $this->Product->save($request_data)) {
                $this->Session->setFlash(__('The product has been saved'), 'default', array('class' => 'success'));
                $this->redirect($this->referer());
            } else {
                $this->Session->setFlash(__('Product could not be saved . Please try again'));
            }
        } else {
            $this->Product->locale = $from;
            $this->request->data[$from] = $this->Product->read(null, $id);
            $this->Product->locale = $to;
            $this->request->data[$to] = $this->Product->read(null, $id);

            if (empty($this->request->data[$to])) $this->request->data[$to] = $this->request->data[$from];
        }

        $this->Product->loadRelation('belongsTo', 'ProductCategory', false);
        $product_categories = $this->Product->ProductCategory->generateTreeList(null, null, null, '----');
        $this->set(compact('product_categories'));

        $this->Product->loadRelation('belongsTo', 'Brand', false);
        $brands = $this->Product->Brand->find('list', ['fields' => ['id', 'title']]);
        $this->set('brands', $brands);

        $this->Product->loadRelation('belongsTo', 'Supplier', false);
        $suppliers = $this->Product->Supplier->find('list', ['fields' => ['id', 'title']]);
        $this->set('suppliers', $suppliers);
    }

    function resetattachment($id = null)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $this->Product->id = $id;
        if (!$this->Product->exists()) {
            throw new NotFoundException(__('Invalid product'));
        }

        if ($this->Product->saveField('attachment', null, true)) {
            $this->Session->setFlash(__('Product attachment was deleted'));
            $this->redirect(array('action' => 'edit', $id));
        } else {
            $this->Session->setFlash(__('Product attachment could not be deleted. Please try again'));
            $this->redirect(array('action' => 'edit', $id));
        }
    }
}

?>