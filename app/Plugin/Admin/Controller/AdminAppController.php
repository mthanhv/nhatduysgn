<?php
App::uses('AppController', 'Controller');

class AdminAppController extends AppController
{
    var $helpers = array('Form', 'Html', 'Session', 'Authen', 'Site');
    var $components = array('Session', 'Cookie', 'Utility', 'Authen', 'Site');

    public function beforeFilter()
    {
        $this->layout = 'Admin.default';

        $this->setLanguage('eng', 'BE');

        $this->Authen->authenticate($this);
    }
}