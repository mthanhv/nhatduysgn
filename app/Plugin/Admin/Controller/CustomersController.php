<?php

class CustomersController extends AdminAppController
{
    var $uses = 'Customer';
    var $paginate = array('order' => 'Customer.id DESC');

    function index()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $url = array('action' => 'index');
            if (!empty($this->request->data['Customer']['key'])) {
                $url['key'] = $this->Utility->safe_b64encode($this->request->data['Customer']['key']);
            }
            $this->redirect($url);
        }
        $key = isset($this->request->params['named']['key']) ? $this->Utility->safe_b64decode($this->request->params['named']['key']) : '';
        $key_escaped = str_replace(array('%', '_'), array('\%', '\_'), $key);

        $conditions = array();
        if (!empty($key)) {
            $conditions['OR'] = array('Customer.title LIKE' => "%{$key_escaped}%");

            //Pass information for view
            $this->request->data['Customer']['key'] = $key;
        }

        $this->set('customers', $this->paginate('Customer', $conditions));
    }

    function add()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $add_and_new = isset($this->request->data['FormAction']['add_and_new']) ? true : false;
            $is_image_uploaded = (!empty($this->request->data['Customer']['image']) && isset($this->request->data['Customer']['image']['error']) && $this->request->data['Customer']['image']['error'] != UPLOAD_ERR_NO_FILE);
            unset($this->request->data['FormAction']);

            $this->Customer->create();
            if ($is_image_uploaded) {
                $upload_options = array(
                    'location' => 'files/customers/',
                    'check_exists' => true,
                    'generate_unique_name' => false,
                    'clean_name' => true,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array(
                        0 => array(
                            'location' => 'files/customers/thumbs/',
                            'check_exists' => true,
                            'generate_unique_name' => false,
                            'use_master_file_name' => true,
                            'clean_name' => false,
                            'name' => null,
                            'ext' => null,
                            'width' => CUSTOMER_THUMB1_W,
                            'height' => CUSTOMER_THUMB1_H,
                        )
                    )
                );
                $data = $this->Utility->upload_file($this->request->data['Customer']['image'], $upload_options);

                if (is_array($data) && isset($data['error']) && $data['error'] == false) {
                    $this->request->data['Customer']['image'] = "{$data['data']['name']}.{$data['data']['ext']}";
                } else {
                    $this->Session->setFlash(__('The customer could not be saved. Please, try again'));
                    $this->Customer->validationErrors['image'][] = $data['data'];
                    return;
                }
            } else {
                $this->request->data['Customer']['image'] = '';
            }

            if ($this->Customer->save($this->request->data)) {
                $this->Session->setFlash(__('The customer has been saved'), 'default', array('class' => 'success'));
                if ($add_and_new) $this->redirect(array('action' => 'add'));
                else $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The customer could not be saved. Please, try again'));
            }
        }
    }

    function edit($id = null)
    {
        $this->Customer->id = $id;
        if (!$this->Customer->exists()) {
            throw new NotFoundException(__('Invalid customer'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            // Dont choose image
            if (isset($this->request->data['Customer']['image']['error']) == false || $this->request->data['Customer']['image']['error'] == UPLOAD_ERR_NO_FILE) {
                unset($this->request->data['Customer']['image']);
                if ($this->Customer->save($this->request->data)) {
                    $this->Session->setFlash(__('The customer has been saved'), 'default', array('class' => 'success'));
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('Customer could not be saved . Please try again'));
                }
            } else {
                $upload_options = array(
                    'location' => 'files/customers/',
                    'check_exists' => true,
                    'generate_unique_name' => false,
                    'clean_name' => true,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array(
                        0 => array(
                            'location' => 'files/customers/thumbs/',
                            'check_exists' => true,
                            'generate_unique_name' => false,
                            'use_master_file_name' => true,
                            'clean_name' => false,
                            'name' => null,
                            'ext' => null,
                            'width' => CUSTOMER_THUMB1_W,
                            'height' => CUSTOMER_THUMB1_H,
                        )
                    )
                );
                $data = $this->Utility->upload_file($this->request->data['Customer']['image'], $upload_options);
                if (is_array($data) && isset($data['error']) && $data['error'] == false) {
                    $this->request->data['Customer']['image'] = "{$data['data']['name']}.{$data['data']['ext']}";
                    if ($this->Customer->save($this->request->data)) {
                        $this->Session->setFlash(__('The customer has been saved'), 'default', array('class' => 'success'));
                        $this->redirect(array('action' => 'index'));
                    } else {
                        $this->Session->setFlash(__('Customer could not be saved . Please try again'));
                    }
                } else {
                    $this->Session->setFlash(__('The customer could not be saved. Please, try again'));
                    $this->Customer->validationErrors['image'][] = $data['data'];
                }
            }
        } else {
            $this->request->data = $this->Customer->read(null, $id);
        }
    }

    function delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $this->Customer->id = $id;
        if (!$this->Customer->exists()) {
            throw new NotFoundException(__('Invalid customer'));
        }

        if ($this->Customer->delete($id)) {
            $this->Session->setFlash(__('Customer was deleted'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('Customer could not be deleted. Please try again'));
            $this->redirect(array('action' => 'index'));
        }
    }

    function deletemulti()
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $ids = isset($this->data['markedvalues']) ? explode(',', $this->data['markedvalues']) : '';
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->Customer->id = $id;
                if (!$id || !$this->Customer->exists()) continue;
                $this->Customer->delete($id);
            }
        }

        $this->Session->setFlash(__('Customers were deleted'));
        $this->redirect(array('action' => 'index'));
    }

    function translate($id = null)
    {
        $this->layout = 'translation';

        $from = isset($this->request->params['named']['from']) ? $this->request->params['named']['from'] : '';
        $to = isset($this->request->params['named']['to']) ? $this->request->params['named']['to'] : '';
        $this->set('from', $from);
        $this->set('to', $to);

        $this->Customer->id = $id;
        $this->Customer->locale = $from;
        if (!$this->Customer->exists()) {
            throw new NotFoundException(__('Invalid customer'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $locale = $this->request->data['Customer']['locale'];
            $request_data = $this->request->data[$locale];

            // Dont choose image
            if (isset($request_data['Customer']['image']['error']) == false || $request_data['Customer']['image']['error'] == UPLOAD_ERR_NO_FILE) {
                unset($request_data['Customer']['image']);
                $this->Customer->locale = $locale;
                if ($this->Customer->save($request_data)) {
                    $this->Session->setFlash(__('The customer has been saved'), 'default', array('class' => 'success'));
                    $this->redirect($this->referer());
                } else {
                    $this->Session->setFlash(__('Customer could not be saved . Please try again'));
                }
            } else {
                $upload_options = array(
                    'location' => 'files/customers/',
                    'check_exists' => true,
                    'generate_unique_name' => false,
                    'clean_name' => true,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array(
                        0 => array(
                            'location' => 'files/customers/thumbs/',
                            'check_exists' => true,
                            'generate_unique_name' => false,
                            'use_master_file_name' => true,
                            'clean_name' => false,
                            'name' => null,
                            'ext' => null,
                            'width' => CUSTOMER_THUMB1_W,
                            'height' => CUSTOMER_THUMB1_H,
                        )
                    )
                );
                $data = $this->Utility->upload_file($request_data['Customer']['image'], $upload_options);
                if (is_array($data) && isset($data['error']) && $data['error'] == false) {
                    $request_data['Customer']['image'] = "{$data['data']['name']}.{$data['data']['ext']}";
                    $this->Customer->locale = $locale;
                    if ($this->Customer->save($request_data)) {
                        $this->Session->setFlash(__('The customer has been saved'), 'default', array('class' => 'success'));
                        $this->redirect($this->referer());
                    } else {
                        $this->Session->setFlash(__('Customer could not be saved . Please try again'));
                    }
                } else {
                    $this->Session->setFlash(__('The customer could not be saved. Please, try again'));
                    $this->Customer->validationErrors['image'][] = $data['data'];
                }
            }
        } else {
            $this->Customer->locale = $from;
            $this->request->data[$from] = $this->Customer->read(null, $id);
            $this->Customer->locale = $to;
            $this->request->data[$to] = $this->Customer->read(null, $id);

            if (empty($this->request->data[$to])) $this->request->data[$to] = $this->request->data[$from];
        }
    }
}

?>