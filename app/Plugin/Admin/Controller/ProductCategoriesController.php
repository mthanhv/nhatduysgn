<?php

class ProductCategoriesController extends AdminAppController
{
    var $uses = 'ProductCategory';
    var $paginate = array('order' => 'ProductCategory.id DESC');

    function index()
    {
        $this->paginate = array(
            'ProductCategory' => array(
                'order' => array(
                    'ProductCategory.lft' => 'ASC'
                )
            )
        );
        $this->set('product_categories', $this->paginate('ProductCategory'));
    }

    function add()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $add_and_new = isset($this->request->data['FormAction']['add_and_new']) ? true : false;
            unset($this->request->data['FormAction']);

            $choosed_thumb = (isset($this->request->data['ProductCategory']['image']['error']) && $this->request->data['ProductCategory']['image']['error'] != UPLOAD_ERR_NO_FILE);

            if ($choosed_thumb) {
                $upload_options = array(
                    'location' => 'files/categories/',
                    'check_exists' => false,
                    'generate_unique_name' => true,
                    'clean_name' => false,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array(
                        0 => array(
                            'location' => 'files/categories/thumbs/',
                            'check_exists' => false,
                            'generate_unique_name' => false,
                            'use_master_file_name' => true,
                            'clean_name' => false,
                            'name' => null,
                            'ext' => null,
                            'width' => 150,
                            'height' => 150,
                        )
                    )
                );
                $thumb_data = $this->Utility->upload_file($this->request->data['ProductCategory']['image'], $upload_options);
                if (is_array($thumb_data) && isset($thumb_data['error']) && $thumb_data['error'] == false) {
                    $this->request->data['ProductCategory']['image'] = "{$thumb_data['data']['name']}.{$thumb_data['data']['ext']}";
                } else {
                    $this->ProductCategory->validationErrors['image'][] = $thumb_data['data'];
                }
            } else {
                $this->request->data['ProductCategory']['image'] = '';
            }

            if (empty($this->request->data['ProductCategory']['parent_id'])) $this->request->data['ProductCategory']['parent_id'] = null;
            if (empty($this->ProductCategory->validationErrors) && $this->ProductCategory->save($this->request->data)) {
                $this->Session->setFlash(__('The product category has been saved'), 'default', array('class' => 'success'));
                if ($add_and_new) $this->redirect(array('action' => 'add'));
                else $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The product category could not be saved. Please, try again'));
            }
        }

        $product_categories = $this->ProductCategory->generateTreeList(null, null, null, '----');
        $product_categories = array(0 => __('Select parent product category')) + $product_categories;
        $this->set('product_categories', $product_categories);
    }

    function edit($id = null)
    {
        $this->ProductCategory->id = $id;
        if (!$this->ProductCategory->exists()) {
            throw new NotFoundException(__('Invalid product category'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $choosed_thumb = (isset($this->request->data['ProductCategory']['image']['error']) && $this->request->data['ProductCategory']['image']['error'] != UPLOAD_ERR_NO_FILE);

            if ($choosed_thumb) {
                $upload_options = array(
                    'location' => 'files/categories/',
                    'check_exists' => false,
                    'generate_unique_name' => true,
                    'clean_name' => false,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array(
                        0 => array(
                            'location' => 'files/categories/thumbs/',
                            'check_exists' => false,
                            'generate_unique_name' => false,
                            'use_master_file_name' => true,
                            'clean_name' => false,
                            'name' => null,
                            'ext' => null,
                            'width' => 150,
                            'height' => 150,
                        )
                    )
                );
                $thumb_data = $this->Utility->upload_file($this->request->data['ProductCategory']['image'], $upload_options);
                if (is_array($thumb_data) && isset($thumb_data['error']) && $thumb_data['error'] == false) {
                    $this->request->data['ProductCategory']['image'] = "{$thumb_data['data']['name']}.{$thumb_data['data']['ext']}";
                } else {
                    $this->ProductCategory->validationErrors['image'][] = $thumb_data['data'];
                }
            } else {
                unset($this->request->data['ProductCategory']['image']);
            }

            if (empty($this->request->data['ProductCategory']['parent_id'])) $this->request->data['ProductCategory']['parent_id'] = null;
            if (empty($this->ProductCategory->validationErrors) && $this->ProductCategory->save($this->request->data)) {
                $this->Session->setFlash(__('The product category has been saved'), 'default', array('class' => 'success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The product category could not be saved. Please, try again'));
            }
        } else {
            $this->request->data = $this->ProductCategory->read(null, $id);
        }

        $product_categories = $this->ProductCategory->generateTreeList(null, null, null, '----');
        $product_categories = array(0 => __('Select parent product category')) + $product_categories;
        $this->set('product_categories', $product_categories);
    }

    function delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $this->ProductCategory->id = $id;
        if (!$this->ProductCategory->exists()) {
            throw new NotFoundException(__('Invalid product category'));
        }

        if ($this->ProductCategory->delete($id)) {
            $this->Session->setFlash(__('ProductCategory was deleted'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('ProductCategory could not be deleted. Please try again'));
            $this->redirect(array('action' => 'index'));
        }
    }

    function deletemulti()
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $ids = isset($this->data['markedvalues']) ? explode(',', $this->data['markedvalues']) : '';
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->ProductCategory->id = $id;
                if (!$id || !$this->ProductCategory->exists()) continue;
                $this->ProductCategory->delete($id);
            }
        }

        $this->Session->setFlash(__('ProductCategories were deleted'));
        $this->redirect(array('action' => 'index'));
    }

    function up($id = null)
    {
        $this->ProductCategory->id = $id;
        if (!$this->ProductCategory->exists()) {
            throw new NotFoundException(__('Invalid product category'));
        }

        if ($this->ProductCategory->moveUp($id, 1)) {
            $this->Session->setFlash(__('Product category was moved up.'), 'default', array('class' => 'success'));
        } else {
            $this->Session->setFlash(__('Product category was moved up unsuccessfully.'));
        }
        $this->redirect(array('action' => 'index'));
    }

    function down($id = null)
    {
        $this->ProductCategory->id = $id;
        if (!$this->ProductCategory->exists()) {
            throw new NotFoundException(__('Invalid product category'));
        }

        if ($this->ProductCategory->moveDown($id, 1)) {
            $this->Session->setFlash(__('Product category was moved down.'), 'default', array('class' => 'success'));
        } else {
            $this->Session->setFlash(__('Product category was moved down unsuccessfully.'));
        }
        $this->redirect(array('action' => 'index'));
    }
}

?>