<?php

class BlocksController extends AdminAppController
{
    var $uses = 'Block';
    var $paginate = array('order' => 'Block.id DESC');

    function index()
    {
        $this->paginate = array(
            'Block' => array(
                'order' => 'Block.id DESC',
                'fields' => array('Block.id', 'Block.title', 'Block.created')
            )
        );
        $this->set('blocks', $this->paginate('Block'));
    }

    function add()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $add_and_new = isset($this->request->data['FormAction']['add_and_new']) ? true : false;
            unset($this->request->data['FormAction']);

            $this->Block->create();
            if ($this->Block->save($this->request->data)) {
                $this->Session->setFlash(__('The block has been saved'), 'default', array('class' => 'success'));
                if ($add_and_new) $this->redirect(array('action' => 'add'));
                else $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The block could not be saved. Please, try again'));
            }
        }
    }

    function edit($id = null)
    {
        $this->Block->id = $id;
        if (!$this->Block->exists()) {
            throw new NotFoundException(__('Invalid block'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Block->save($this->request->data)) {
                $this->Session->setFlash(__('The block has been saved'), 'default', array('class' => 'success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Block could not be saved . Please try again'));
            }
        } else {
            $this->request->data = $this->Block->read(null, $id);
        }
    }

    function delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $this->Block->id = $id;
        if (!$this->Block->exists()) {
            throw new NotFoundException(__('Invalid block'));
        }

        if ($this->Block->delete($id)) {
            $this->Session->setFlash(__('Block was deleted'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('Block could not be deleted. Please try again'));
            $this->redirect(array('action' => 'index'));
        }
    }

    function deletemulti()
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $ids = isset($this->data['markedvalues']) ? explode(',', $this->data['markedvalues']) : '';
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->Block->id = $id;
                if (!$id || !$this->Block->exists()) continue;
                $this->Block->delete($id);
            }
        }

        $this->Session->setFlash(__('Blocks were deleted'));
        $this->redirect(array('action' => 'index'));
    }

    function translate($id = null)
    {
        $this->layout = 'translation';

        $from = isset($this->request->params['named']['from']) ? $this->request->params['named']['from'] : '';
        $to = isset($this->request->params['named']['to']) ? $this->request->params['named']['to'] : '';
        $this->set('from', $from);
        $this->set('to', $to);

        $this->Block->id = $id;
        $this->Block->locale = $from;
        if (!$this->Block->exists()) {
            throw new NotFoundException(__('Invalid block'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $locale = $this->request->data['Block']['locale'];
            $request_data = $this->request->data[$locale];

            $this->Block->locale = $locale;
            if ($this->Block->save($request_data)) {
                $this->Session->setFlash(__('The block has been saved'), 'default', array('class' => 'success'));
                $this->redirect($this->referer());
            } else {
                $this->Session->setFlash(__('Block could not be saved . Please try again'));
            }
        } else {
            $this->Block->locale = $from;
            $this->request->data[$from] = $this->Block->read(null, $id);
            $this->Block->locale = $to;
            $this->request->data[$to] = $this->Block->read(null, $id);

            if (empty($this->request->data[$to])) $this->request->data[$to] = $this->request->data[$from];
        }
    }
}

?>