<?php
App::uses('AppController', 'Controller');

class AdminController extends AdminAppController
{
    function index()
    {
        $this->loadModel('Product');
        $this->Product->loadRelation('belongsTo', 'ProductCategory');
        $products = $this->Product->find('all', array('order' => 'Product.id DESC', 'limit' => 10, 'fields' => array('Product.id', 'Product.image', 'Product.title', 'ProductCategory.name', 'Product.created')));
        $this->set('products', $products);

        $this->loadModel('Contact');
        $contacts = $this->Contact->find('all', array('order' => 'Contact.id DESC', 'limit' => 10));
        $this->set('contacts', $contacts);
    }
}