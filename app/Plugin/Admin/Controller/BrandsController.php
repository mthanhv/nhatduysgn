<?php

class BrandsController extends AdminAppController
{
    var $uses = 'Brand';
    var $paginate = array('order' => 'Brand.id DESC');

    function index()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $url = array('action' => 'index');
            if (!empty($this->request->data['Brand']['key'])) {
                $url['key'] = $this->Utility->safe_b64encode($this->request->data['Brand']['key']);
            }
            $this->redirect($url);
        }
        $key = isset($this->request->params['named']['key']) ? $this->Utility->safe_b64decode($this->request->params['named']['key']) : '';
        $key_escaped = str_replace(array('%', '_'), array('\%', '\_'), $key);

        $conditions = array();
        if (!empty($key)) {
            $conditions['OR'] = array('Brand.title LIKE' => "%{$key_escaped}%");

            //Pass information for view
            $this->request->data['Brand']['key'] = $key;
        }

        $this->set('brands', $this->paginate('Brand', $conditions));
    }

    function add()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $add_and_new = isset($this->request->data['FormAction']['add_and_new']) ? true : false;
            $is_image_uploaded = (!empty($this->request->data['Brand']['image']) && isset($this->request->data['Brand']['image']['error']) && $this->request->data['Brand']['image']['error'] != UPLOAD_ERR_NO_FILE);
            unset($this->request->data['FormAction']);

            $this->Brand->create();
            if ($is_image_uploaded) {
                $upload_options = array(
                    'location' => 'files/brands/',
                    'check_exists' => false,
                    'generate_unique_name' => true,
                    'clean_name' => false,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array(
                        0 => array(
                            'location' => 'files/brands/thumbs/',
                            'check_exists' => false,
                            'generate_unique_name' => false,
                            'use_master_file_name' => true,
                            'clean_name' => false,
                            'name' => null,
                            'ext' => null,
                            'width' => 300,
                            'height' => 220,
                        )
                    )
                );
                $data = $this->Utility->upload_file($this->request->data['Brand']['image'], $upload_options);

                if (is_array($data) && isset($data['error']) && $data['error'] == false) {
                    $this->request->data['Brand']['image'] = "{$data['data']['name']}.{$data['data']['ext']}";
                } else {
                    $this->Session->setFlash(__('The brand could not be saved. Please, try again'));
                    $this->Brand->validationErrors['image'][] = $data['data'];
                    return;
                }
            } else {
                $this->request->data['Brand']['image'] = '';
            }

            if ($this->Brand->save($this->request->data)) {
                $this->Session->setFlash(__('The brand has been saved'), 'default', array('class' => 'success'));
                if ($add_and_new) $this->redirect(array('action' => 'add'));
                else $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The brand could not be saved. Please, try again'));
            }
        }
    }

    function edit($id = null)
    {
        $this->Brand->id = $id;
        if (!$this->Brand->exists()) {
            throw new NotFoundException(__('Invalid brand'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            // Dont choose image
            if (isset($this->request->data['Brand']['image']['error']) == false || $this->request->data['Brand']['image']['error'] == UPLOAD_ERR_NO_FILE) {
                unset($this->request->data['Brand']['image']);
                if ($this->Brand->save($this->request->data)) {
                    $this->Session->setFlash(__('The brand has been saved'), 'default', array('class' => 'success'));
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('Brand could not be saved . Please try again'));
                }
            } else {
                $upload_options = array(
                    'location' => 'files/brands/',
                    'check_exists' => false,
                    'generate_unique_name' => true,
                    'clean_name' => false,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array(
                        0 => array(
                            'location' => 'files/brands/thumbs/',
                            'check_exists' => false,
                            'generate_unique_name' => false,
                            'use_master_file_name' => true,
                            'clean_name' => false,
                            'name' => null,
                            'ext' => null,
                            'width' => 300,
                            'height' => 220,
                        )
                    )
                );
                $data = $this->Utility->upload_file($this->request->data['Brand']['image'], $upload_options);
                if (is_array($data) && isset($data['error']) && $data['error'] == false) {
                    $this->request->data['Brand']['image'] = "{$data['data']['name']}.{$data['data']['ext']}";
                    if ($this->Brand->save($this->request->data)) {
                        $this->Session->setFlash(__('The brand has been saved'), 'default', array('class' => 'success'));
                        $this->redirect(array('action' => 'index'));
                    } else {
                        $this->Session->setFlash(__('Brand could not be saved . Please try again'));
                    }
                } else {
                    $this->Session->setFlash(__('The brand could not be saved. Please, try again'));
                    $this->Brand->validationErrors['image'][] = $data['data'];
                }
            }
        } else {
            $this->request->data = $this->Brand->read(null, $id);
        }
    }

    function delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $this->Brand->id = $id;
        if (!$this->Brand->exists()) {
            throw new NotFoundException(__('Invalid brand'));
        }

        if ($this->Brand->delete($id)) {
            $this->Session->setFlash(__('Brand was deleted'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('Brand could not be deleted. Please try again'));
            $this->redirect(array('action' => 'index'));
        }
    }

    function deletemulti()
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $ids = isset($this->data['markedvalues']) ? explode(',', $this->data['markedvalues']) : '';
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->Brand->id = $id;
                if (!$id || !$this->Brand->exists()) continue;
                $this->Brand->delete($id);
            }
        }

        $this->Session->setFlash(__('Brands were deleted'));
        $this->redirect(array('action' => 'index'));
    }

    function translate($id = null)
    {
        $this->layout = 'translation';

        $from = isset($this->request->params['named']['from']) ? $this->request->params['named']['from'] : '';
        $to = isset($this->request->params['named']['to']) ? $this->request->params['named']['to'] : '';
        $this->set('from', $from);
        $this->set('to', $to);

        $this->Brand->id = $id;
        $this->Brand->locale = $from;
        if (!$this->Brand->exists()) {
            throw new NotFoundException(__('Invalid brand'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $locale = $this->request->data['Brand']['locale'];
            $request_data = $this->request->data[$locale];

            // Dont choose image
            if (isset($request_data['Brand']['image']['error']) == false || $request_data['Brand']['image']['error'] == UPLOAD_ERR_NO_FILE) {
                unset($request_data['Brand']['image']);
                $this->Brand->locale = $locale;
                if ($this->Brand->save($request_data)) {
                    $this->Session->setFlash(__('The brand has been saved'), 'default', array('class' => 'success'));
                    $this->redirect($this->referer());
                } else {
                    $this->Session->setFlash(__('Brand could not be saved . Please try again'));
                }
            } else {
                $upload_options = array(
                    'location' => 'files/brands/',
                    'check_exists' => false,
                    'generate_unique_name' => true,
                    'clean_name' => false,
                    'name' => null,
                    'ext' => null,
                    'thumbs' => array(
                        0 => array(
                            'location' => 'files/brands/thumbs/',
                            'check_exists' => false,
                            'generate_unique_name' => false,
                            'use_master_file_name' => true,
                            'clean_name' => false,
                            'name' => null,
                            'ext' => null,
                            'width' => 300,
                            'height' => 220,
                        )
                    )
                );
                $data = $this->Utility->upload_file($request_data['Brand']['image'], $upload_options);
                if (is_array($data) && isset($data['error']) && $data['error'] == false) {
                    $request_data['Brand']['image'] = "{$data['data']['name']}.{$data['data']['ext']}";
                    $this->Brand->locale = $locale;
                    if ($this->Brand->save($request_data)) {
                        $this->Session->setFlash(__('The brand has been saved'), 'default', array('class' => 'success'));
                        $this->redirect($this->referer());
                    } else {
                        $this->Session->setFlash(__('Brand could not be saved . Please try again'));
                    }
                } else {
                    $this->Session->setFlash(__('The brand could not be saved. Please, try again'));
                    $this->Brand->validationErrors['image'][] = $data['data'];
                }
            }
        } else {
            $this->Brand->locale = $from;
            $this->request->data[$from] = $this->Brand->read(null, $id);
            $this->Brand->locale = $to;
            $this->request->data[$to] = $this->Brand->read(null, $id);

            if (empty($this->request->data[$to])) $this->request->data[$to] = $this->request->data[$from];
        }
    }
}

?>