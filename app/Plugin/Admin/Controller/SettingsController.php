<?php

class SettingsController extends AdminAppController
{
    var $uses = 'Setting';

    function edit()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Setting->save($this->request->data)) {
                $this->Site->delete();
                $this->Session->setFlash(__('The settings has been saved'), 'default', array('class' => 'success'));
                $this->redirect(array('action' => 'edit'));
            } else {
                $this->Session->setFlash(__('The setting could not be saved. Please, try again'));
            }
        } else {
            $this->request->data = $this->Setting->find('first');
        }
    }

    function translate()
    {
        $this->layout = 'translation';

        $from = isset($this->request->params['named']['from']) ? $this->request->params['named']['from'] : '';
        $to = isset($this->request->params['named']['to']) ? $this->request->params['named']['to'] : '';
        $this->set('from', $from);
        $this->set('to', $to);

        if ($this->request->is('post') || $this->request->is('put')) {
            $locale = $this->request->data['Setting']['locale'];
            $request_data = $this->request->data[$locale];

            $this->Setting->locale = $locale;
            if ($this->Setting->save($request_data)) {
                $this->Site->delete();
                $this->Session->setFlash(__('The settings has been saved'), 'default', array('class' => 'success'));
                $this->redirect($this->referer());
            } else {
                $this->Session->setFlash(__('The setting could not be saved. Please, try again'));
            }
        } else {
            $this->Setting->locale = $from;
            $this->request->data[$from] = $this->Setting->find('first');
            $this->Setting->locale = $to;
            $this->request->data[$to] = $this->Setting->find('first');

            if (empty($this->request->data[$to])) $this->request->data[$to] = $this->request->data[$from];
        }
    }
}

?>