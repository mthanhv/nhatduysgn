<?php echo $this->Html->script(array('Admin.jquery'), array('inline'=>false)); ?>
<?php echo $this->Html->scriptStart(array('inline'=>false)); ?>
	$(document).ready(function() {
		$("#mark-all").click(function(){
			$('input.mark').attr('checked', this.checked);
		});
		$("#delete-multi").click(function(){
			var ids = '';
			$("input.mark:checked").each(function(){
				ids += (this.value + ',');
			});
			if(ids == ''){
				alert("<?php echo __('Please select customer');?>");
			} else {
				if(confirm("<?php echo __('Are you sure you want to delete the selected customers ?');?>")){
					ids = ids.substring(0, ids.length - 1);
					$("#marked-values").val(ids);
					$("#deletemulti-form").submit();
				}
			}
			return false;
		});
	});
<?php echo $this->Html->scriptEnd(); ?>
<div class="header-bar">
	<div class="body-header">
		<div class="left header-bar-title"><?php echo $this->Html->link(__('Customers'), array('plugin'=>'admin', 'controller'=>'customers', 'action'=>'index'));?></div>
		<div class="right header-bar-action">
			<?php echo $this->Html->link($this->Html->image('Admin.add.png'), array('plugin'=>'admin', 'controller'=>'customers', 'action'=>'add'), array('title'=>__('Add new customer'), 'class'=>'add-customer', 'escape' => false)); ?>
			<?php echo $this->Html->link($this->Html->image('Admin.delete.png'), '#', array('title'=>__('Delete selected customers'), 'id'=>'delete-multi', 'escape' => false)); ?>
			<form method="POST" action="/admin/customers/deletemulti" id="deletemulti-form">
				<input type="hidden" id="marked-values" name="markedvalues" value=""/>
			</form>
		</div>
		<div class="clear"></div>
	 </div>
</div>
<div class='form-message'>
	<?php echo $this->Session->flash();?>
</div>
<div class="customers search">
	<?php echo $this->Form->create('Customer', array('novalidate'));?>
	<?php echo $this->Form->input('key', array('div'=>'inline-block', 'label'=>false, 'class'=>'w300', 'placeholder'=>__('E.g. Customer title')));?>
	<?php echo $this->Form->submit(__('Search'), array('div'=>'inline-block'));?>
	<?php echo $this->Form->end();?>
</div>
<div class="customers index">
	<table cellpadding="0" cellspacing="0" class="w100p">
        <thead>
		<tr>
			<th class="center w20"><input type="checkbox" id="mark-all"></th>
			<th class="center w60"><?php echo __('Thumb');?></th>
			<th><?php echo $this->Paginator->sort('title');?></th>
			<th class="actions w70"><?php echo __('Actions');?></th>
		</tr>
        </thead>
        <tbody>
		<?php foreach ($customers as $customer): ?>
		<tr>
			<td class="center w20"><input class="mark" type="checkbox" value="<?php echo $customer['Customer']['id'];?>"></td>
			<td class="center w60"><?php echo $this->Html->image('/files/customers/thumbs/'.$customer['Customer']['image'], array('class'=>'w50')); ?>&nbsp;</td>
			<td><?php echo h($customer['Customer']['title']); ?>&nbsp;</td>
			<td class="actions w70">
				<?php echo $this->Html->link($this->Html->image('Admin.action-edit.png'), array('action' => 'edit', $customer['Customer']['id']), array('title'=>__('Edit'), 'class'=>'edit-customer', 'escape'=>false)); ?>
				<?php echo $this->Form->postLink($this->Html->image('Admin.action-delete.png'), array('action' => 'delete', $customer['Customer']['id']), array('title'=>__('Delete'), 'class'=>'action-delete', 'escape'=>false), __("Are you sure you want to delete customer '%s' ?", $customer['Customer']['title'])); ?>
			</td>
		</tr>
		<?php endforeach; ?>
        </tbody>
        <tfoot>
        <tr>
            <td class="center" colspan="8">
                <div class="paging-count">
                    <span><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} of {:count}')));?></span>
                </div>
                <div class="paging">
                    <?php
                    echo $this->Paginator->first('<<');
                    echo $this->Paginator->prev('<', array(), null, array('class' => 'prev disabled display-none'));
                    echo $this->Paginator->numbers(array('separator' => ''));
                    echo $this->Paginator->next('>', array(), null, array('class' => 'next disabled display-none'));
                    echo $this->Paginator->last('>>');
                    ?>
                </div>
                <div class="clear"></div>
            </td>
        </tr>
        </tfoot>
	</table>
</div>