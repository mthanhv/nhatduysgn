<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php echo $this->Html->charset(); ?>
		<title><?php echo $this->Site->get('site_name'); ?>: <?php echo $title_for_layout; ?></title>
		<?php echo $this->Html->meta('icon');?>
		<?php echo $this->Html->css('Admin.standard');?>
		<?php echo $this->Html->css('Admin.style');?>
		<?php echo $this->fetch('meta');?>
		<?php echo $this->fetch('css');?>
		<?php echo $this->fetch('script');?>
	</head>
	<body>
		<div id="header">
			<div id="headercontent">
				<div class="left">
                    <?php echo $this->element('Admin.menu');?>
				</div>
				<div class="right">
                    <?php echo $this->Html->link($this->Site->get('site_name'), '/', array('class' => 'page-title'));?>
					<?php if ($this->Authen->isLogged()) {?>
						<span class="welcome"><?php echo __('welcome: %s, %s', $this->Authen->getFullName(), $this->Html->link(__('logout'), array('controller' => 'user', 'action' => 'logout'), array('class' => 'logout-btn', 'title' => __('Logout')), __('Are you sure you want to logout ?')));?></span>
					<?php } ?>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div id="main">
			<div id="maincontent">
				<?php echo $this->fetch('content'); ?>
			</div>
		</div>
		<div id="footer">
			<div id="footercontent"><?php echo $this->Site->get('site_name');?> © 2015</div>
		</div>
		<?php echo $this->element('sql_dump'); ?>
	</body>
</html>
