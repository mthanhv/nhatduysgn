<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php echo $this->Html->charset(); ?>
		<title><?php echo $this->Site->get('site_name'); ?>: <?php echo $title_for_layout; ?></title>
		<?php echo $this->Html->meta('icon');?>
		<?php echo $this->Html->css('Admin.standard');?>
		<?php echo $this->Html->css('Admin.style');?>
		<?php echo $this->Html->css('Admin.adminpopup');?>
		<?php echo $this->fetch('meta');?>
		<?php echo $this->fetch('css');?>
		<?php echo $this->fetch('script');?>
	</head>
	<body>
		<div class="adminpopup">
			<?php echo $this->fetch('content'); ?>
		</div>
		<?php //echo $this->element('sql_dump'); ?>
	</body>
</html>
