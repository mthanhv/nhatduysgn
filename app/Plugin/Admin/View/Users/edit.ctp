<div class="header-bar">
	<div class="body-header">
		<div class="left header-bar-title"><?php echo $this->Html->link(__('Users'), array('plugin'=>'admin', 'controller'=>'users', 'action'=>'index'));?></div>
		<div class="right header-bar-action">
			<?php echo $this->Html->link($this->Html->image('Admin.back.png'), array('plugin'=>'admin', 'controller'=>'users', 'action'=>'index'), array('title'=>__('Back'), 'class'=>'back', 'escape' => false)); ?>
		</div>
		<div class="clear"></div>
	 </div>
</div>
<div class='form-message'>
	<?php echo $this->Session->flash();?>
</div>
<div class="users form">
	<?php echo $this->Form->create('User', array('novalidate'));?>
	<fieldset>
		<?php echo $this->Form->input('user_name', array('class'=>'w200 readonly', 'readonly'=>'readonly'));?>
		<?php echo $this->Form->input('password', array('label'=>__('Password (leave blank to keep it)'), 'class'=>'w200'));?>
		<?php echo $this->Form->input('full_name', array('class'=>'w200'));?>
		<?php echo $this->Form->input('email', array('class'=>'w200'));?>
		<?php echo $this->Form->input('Group', array('class'=>'w200', 'label'=>__('Choose group'), 'multiple'=>false));?>
	</fieldset>
	<?php echo $this->Form->end(__('Save'));?>
</div>