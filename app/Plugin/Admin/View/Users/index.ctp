<?php echo $this->Html->script(array('Admin.jquery'), array('inline'=>false)); ?>
<?php echo $this->Html->scriptStart(array('inline'=>false)); ?>
	$(document).ready(function() {
		$("#mark-all").click(function(){
			$('input.mark').attr('checked', this.checked);
		});
		$("#delete-multi").click(function(){
			var ids = '';
			$("input.mark:checked").each(function(){
				ids += (this.value + ',');
			});
			if(ids == ''){
				alert("<?php echo __('Please select user');?>");
			} else {
				if(confirm("<?php echo __('Are you sure you want to delete the selected users ?');?>")){
					ids = ids.substring(0, ids.length - 1);
					$("#marked-values").val(ids);
					$("#deletemulti-form").submit();
				}
			}
			return false;
		});
	});
<?php echo $this->Html->scriptEnd(); ?>
<div class="header-bar">
	<div class="body-header">
		<div class="left header-bar-title"><?php echo $this->Html->link(__('Users'), array('plugin'=>'admin', 'controller'=>'users', 'action'=>'index'));?></div>
		<div class="right header-bar-action">
			<?php echo $this->Html->link($this->Html->image('Admin.add.png'), array('plugin'=>'admin', 'controller'=>'users', 'action'=>'add'), array('title'=>__('Add new user'), 'class'=>'add-user', 'escape' => false)); ?>
			<?php echo $this->Html->link($this->Html->image('Admin.delete.png'), '#', array('title'=>__('Delete selected users'), 'id'=>'delete-multi', 'escape' => false)); ?>
			<form method="POST" action="/admin/users/deletemulti" id="deletemulti-form">
				<input type="hidden" id="marked-values" name="markedvalues" value=""/>
			</form>
		</div>
		<div class="clear"></div>
	 </div>
</div>
<div class='form-message'>
	<?php echo $this->Session->flash();?>
</div>
<div class="users search">
	<?php echo $this->Form->create('User', array('novalidate'));?>
	<?php echo $this->Form->input('key', array('div'=>'inline-block', 'label'=>false, 'class'=>'w300', 'placeholder'=>__('E.g. Full name, User name, Email')));?>
	<?php echo $this->Form->submit(__('Search'), array('div'=>'inline-block'));?>
	<?php echo $this->Form->end();?>
</div>
<div class="users index">		
	<table cellpadding="0" cellspacing="0" class="w100p">
        <thead>
		<tr>
			<th class="center w20"><input type="checkbox" id="mark-all"></th>
			<th><?php echo $this->Paginator->sort('full_name');?></th>
			<th><?php echo $this->Paginator->sort('user_name');?></th>	
			<th><?php echo $this->Paginator->sort('email');?></th>
			<th class="w90 center"><?php echo __('Group');?></th>
			<th class="w90 center"><?php echo $this->Paginator->sort('created');?></th>
			<th class="actions w70 center"><?php echo __('Actions');?></th>
		</tr>
        </thead>
        <tbody>
		<?php foreach ($users as $user): ?>
		<tr>
			<td class="center w20"><input class="mark" type="checkbox" value="<?php echo $user['User']['id'];?>"></td>
			<td><?php echo h($user['User']['full_name']); ?>&nbsp;</td>
			<td><?php echo h($user['User']['user_name']); ?>&nbsp;</td>		
			<td><?php $email = $user['User']['email']; echo "<a href='mailto:{$email}' title='".__('Send email to this user')."'>{$email}</a>"; ?>&nbsp;</td>
			<td class="w90 center">
				<?php
					$gr = (count($user['Group'])) ? array() : array(__('Guest'));
					foreach($user['Group'] as $k => $group) $gr[] = $this->Html->link(__($group['name']), array('plugin'=>'admin', 'controller'=>'users', 'action'=>'index', 'group'=>$group['id']), array('title'=>__('Filter users by this group')));
					echo implode(' + ', $gr);
				?>&nbsp;
			</td>
			<td class="w90 center"><?php echo $this->Time->format('d-m-Y', $user['User']['created']); ?>&nbsp;</td>
			<td class="actions w70 center">
				<?php echo $this->Html->link($this->Html->image('Admin.action-edit.png'), array('action' => 'edit', $user['User']['id']), array('title'=>__('Edit'), 'class'=>'edit-user', 'escape'=>false)); ?>
				<?php echo $this->Form->postLink($this->Html->image('Admin.action-delete.png'), array('action' => 'delete', $user['User']['id']), array('title'=>__('Delete'), 'class'=>'action-delete', 'escape'=>false), __("Are you sure you want to delete user '%s' ?", $user['User']['user_name'])); ?>
			</td>
		</tr>
		<?php endforeach; ?>
        </tbody>
        <tfoot>
        <tr>
            <td class="center" colspan="8">
                <div class="paging-count">
                    <span><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} of {:count}')));?></span>
                </div>
                <div class="paging">
                    <?php
                    echo $this->Paginator->first('<<');
                    echo $this->Paginator->prev('<', array(), null, array('class' => 'prev disabled display-none'));
                    echo $this->Paginator->numbers(array('separator' => ''));
                    echo $this->Paginator->next('>', array(), null, array('class' => 'next disabled display-none'));
                    echo $this->Paginator->last('>>');
                    ?>
                </div>
                <div class="clear"></div>
            </td>
        </tr>
        </tfoot>
	</table>
</div>