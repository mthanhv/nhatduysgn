<div style="font-family:Arial;font-size:13px;">
    <p><?php echo __('Hi %s', $email); ?>, </p>
    <p><?php echo __('A request has been made to reset your account password. Please follow the link below to reset your password');?></p>
    <p>
        <?php
        $url = $this->Html->url(array('plugin'=>'admin', 'controller'=>'user', 'action'=>'changepassword', $code), true);
        echo $this->Html->link(__('Click here to change password'), $url);
        ?>
    </p>
    <p><?php echo __('Change password code: %s', $code);?></p>
    <p><?php echo __('Best regards,');?><br/></p>
    <span style="font-size:11px"><?php echo __('Please do not respond to this message as it is automatically.');?></span>
    <hr style="border:none;border-top:1px solid #DDD;"/>
    <span style="font-size:11px"><?php echo __('Should you have any enquiries with regards to our services, please do not hesitate to contact us');?></span>
</div>