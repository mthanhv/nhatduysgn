<?php echo $this->Html->script(array('tinymce/tinymce.min.js'), array('inline'=>false)); ?>
<?php echo $this->Html->scriptStart(array('inline'=>false)); ?>
	tinymce.init({
		relative_urls: false,
		menubar: false,
		statusbar: false,
		selector: "textarea.editor",
		theme: "modern",
		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			"save table contextmenu directionality emoticons template paste textcolor responsivefilemanager"
		],
        content_css: "/css/editor.css",
		toolbar: "bold italic fontselect fontsizeselect forecolor backcolor | alignleft aligncenter alignright alignjustify | outdent indent | link image media table | code preview fullscreen",
		image_advtab: true,

		external_filemanager_path: "/filemanager/",
		filemanager_title: "Filemanager" ,
		external_plugins: {"filemanager" : "/filemanager/plugin.min.js"}
	});
<?php echo $this->Html->scriptEnd(); ?>
<div class="header-bar">
	<div class="body-header">
		<div class="left header-bar-title"><?php echo $this->Html->link(__('Suppliers'), array('plugin'=>'admin', 'controller'=>'suppliers', 'action'=>'index'));?></div>
		<div class="right header-bar-action">
			<?php echo $this->Html->link($this->Html->image('Admin.back.png'), array('plugin'=>'admin', 'controller'=>'suppliers', 'action'=>'index'), array('title'=>__('Back'), 'class'=>'back', 'escape' => false)); ?>
		</div>
		<div class="clear"></div>
	 </div>
</div>
<div class="form-message">
	<?php echo $this->Session->flash();?>
</div>
<div class="suppliers form" id="supplier-edit">
	<?php echo $this->Form->create('Supplier', array('type'=>'file', 'novalidate'));?>
		<fieldset>
			<?php echo $this->Form->input('id', array('type'=>'hidden'));?>
			<?php echo $this->Form->input('image', array('type'=>'file', 'class'=>'w200', 'label'=>sprintf('Logo (%dpx x %dpx)', SUPPLIER_THUMB1_W, SUPPLIER_THUMB1_H)));?>
			<?php echo $this->Form->input('title', array('class'=>'w200'));?>
			<?php echo $this->Form->input('publish', array('label'=>__('Publish'), 'multiple'=>false, 'options'=>Configure::read('PUBLISH_STATUS')));?>
		</fieldset>
	<?php echo $this->Form->end(__('Update'));?>
</div>
