<div class="header-bar">
	<div class="body-header">
		<div class="left header-bar-title"><?php echo $this->Html->link(__('Slides'), array('plugin'=>'admin', 'controller'=>'slides', 'action'=>'index'));?></div>
		<div class="right header-bar-action">
			<?php echo $this->Html->link($this->Html->image('Admin.back.png'), array('plugin'=>'admin', 'controller'=>'slides', 'action'=>'index'), array('title'=>__('Back'), 'class'=>'back', 'escape' => false)); ?>
		</div>
		<div class="clear"></div>
	 </div>
</div>
<div class="form-message">
	<?php echo $this->Session->flash();?>
</div>
<div class="slides form" id="slide-edit">
	<?php echo $this->Form->create('Slide', array('type'=>'file', 'novalidate'));?>
		<fieldset>
			<?php echo $this->Form->input('id', array('type'=>'hidden', 'label'=>sprintf('Image (%dpx x %dpx)', SLIDE_THUMB1_W, SLIDE_THUMB1_H)));?>
			<?php echo $this->Form->input('image', array('type'=>'file', 'class'=>'w200'));?>
			<?php echo $this->Form->input('link', array('class'=>'w200'));?>
		</fieldset>
	<?php echo $this->Form->end(__('Update'));?>
</div>
