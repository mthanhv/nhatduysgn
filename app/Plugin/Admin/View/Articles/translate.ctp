<?php echo $this->Html->script(array('jquery', 'tinymce/tinymce.min.js'), array('inline'=>false));?>
<?php echo $this->Html->scriptStart(array('inline'=>false)); ?>
	tinymce.init({
		relative_urls: false,
		menubar: false,
		statusbar: false,
		selector: "textarea.editor",
		theme: "modern",
		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			"save table contextmenu directionality emoticons template paste textcolor responsivefilemanager"
		],
		content_css: "/css/editor.css",
		toolbar: "bold italic fontselect fontsizeselect forecolor backcolor | alignleft aligncenter alignright alignjustify | outdent indent | link image media table | code preview fullscreen",
		image_advtab: true,

		external_filemanager_path: "/filemanager/",
		filemanager_title: "Filemanager" ,
		external_plugins: {"filemanager" : "/filemanager/plugin.min.js"}
	});
	$(document).ready(function() {
		$('div.readonly').css("position", "relative");
		$('div.readonly').append("<div onclick='return false;' style='padding: 0; margin: 0; border: none; width: 99%; height: 100%; background: #CCCCCC; position: absolute; top: 0; left: 0; opacity: 0.5; filter: alpha(opacity=50);'></div>");
	});
<?php echo $this->Html->scriptEnd(); ?>
<div class="form-message">
	<?php echo $this->Session->flash();?>
</div>
<div class="left w50p">
	<div class="language-from"><?php echo $from;?></div>
	<div class="articles form" id="article-edit">
		<?php echo $this->Form->create('Article', array('type'=>'file', 'novalidate'));?>
			<fieldset>
				<?php echo $this->Form->input('locale', array('type'=>'hidden', 'value'=>$from));?>
				<?php echo $this->Form->input($from.'.Article.id', array('type'=>'hidden'));?>
				<?php echo $this->Form->input($from.'.Article.image', array('type'=>'file', 'class'=>'w200', 'div'=>'input file readonly', 'label'=>sprintf('Image (%dpx x %dpx)', ARTICLE_THUMB1_W, ARTICLE_THUMB1_H)));?>
				<?php echo $this->Form->input($from.'.Article.title', array('class'=>'w200'));?>
                <?php echo $this->Form->input($from.'.Article.article_category_id', array('options'=>$article_categories));?>
				<?php echo $this->Form->input($from.'.Article.content', array('class'=>'w200 editor'));?>
				<?php echo $this->Form->input($from.'.Article.meta_keywords', array('class'=>'w200'));?>
				<?php echo $this->Form->input($from.'.Article.meta_description', array('class'=>'w200'));?>
			</fieldset>
		<?php echo $this->Form->end(__('Update'));?>
	</div>
</div>
<div class="right w50p">
	<div class="language-to"><?php echo $to;?></div>
	<div class="articles form" id="article-edit">
		<?php echo $this->Form->create('Article', array('type'=>'file', 'novalidate'));?>
			<fieldset>
				<?php echo $this->Form->input('locale', array('type'=>'hidden', 'value'=>$to));?>
				<?php echo $this->Form->input($to.'.Article.id', array('type'=>'hidden'));?>
				<?php echo $this->Form->input($to.'.Article.image', array('type'=>'file', 'class'=>'w200', 'div'=>'input file readonly', 'label'=>sprintf('Image (%dpx x %dpx)', ARTICLE_THUMB1_W, ARTICLE_THUMB1_H)));?>
				<?php echo $this->Form->input($to.'.Article.title', array('class'=>'w200'));?>
                <?php echo $this->Form->input($to.'.Article.article_category_id', array('options'=>$article_categories));?>
				<?php echo $this->Form->input($to.'.Article.content', array('class'=>'w200 editor'));?>
				<?php echo $this->Form->input($to.'.Article.meta_keywords', array('class'=>'w200'));?>
				<?php echo $this->Form->input($to.'.Article.meta_description', array('class'=>'w200'));?>
			</fieldset>
		<?php echo $this->Form->end(__('Update'));?>
	</div>
</div>
<div class="clear"></div>