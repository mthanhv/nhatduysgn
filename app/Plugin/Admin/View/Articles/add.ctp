<?php echo $this->Html->script(array('tinymce/tinymce.min.js'), array('inline'=>false)); ?>
<?php echo $this->Html->scriptStart(array('inline'=>false)); ?>
	tinymce.init({
		relative_urls: false,
		menubar: false,
		statusbar: false,
		selector: "textarea.editor",
		theme: "modern",
		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			"save table contextmenu directionality emoticons template paste textcolor responsivefilemanager"
		],
		content_css: "/css/editor.css",
		toolbar: "bold italic fontselect fontsizeselect forecolor backcolor | alignleft aligncenter alignright alignjustify | outdent indent | link image media table | code preview fullscreen",
		image_advtab: true,

		external_filemanager_path: "/filemanager/",
		filemanager_title: "Filemanager" ,
		external_plugins: {"filemanager" : "/filemanager/plugin.min.js"}
	});
<?php echo $this->Html->scriptEnd(); ?>
<div class="header-bar">
	<div class="body-header">
		<div class="left header-bar-title"><?php echo $this->Html->link(__('Articles'), array('plugin'=>'admin', 'controller'=>'articles', 'action'=>'index'));?></div>
		<div class="right header-bar-action">
			<?php echo $this->Html->link($this->Html->image('Admin.back.png'), array('plugin'=>'admin', 'controller'=>'articles', 'action'=>'index'), array('title'=>__('Back'), 'class'=>'back', 'escape' => false)); ?>
		</div>
		<div class="clear"></div>
	 </div>
</div>
<div class="form-message">
	<?php echo $this->Session->flash();?>
</div>
<div class="articles form" id="article-add">
	<?php echo $this->Form->create('Article', array('type'=>'file', 'novalidate'));?>
		<fieldset>
			<?php echo $this->Form->input('image', array('type'=>'file', 'class'=>'w200', 'label'=>sprintf('Image (%dpx x %dpx)', ARTICLE_THUMB1_W, ARTICLE_THUMB1_H)));?>
			<?php echo $this->Form->input('title', array('class'=>'w200'));?>
            <?php echo $this->Form->input('article_category_id', array('options'=>$article_categories));?>
			<?php echo $this->Form->input('content', array('class'=>'w200 editor'));?>
			<?php echo $this->Form->input('meta_keywords', array('class'=>'w400', 'rows'=>3));?>
			<?php echo $this->Form->input('meta_description', array('class'=>'w400', 'rows'=>3));?>
			<?php echo $this->Form->submit(__('Create'), array('div'=>'submit inline-block', 'name'=>'FormAction[add]'));?>
			<?php echo $this->Form->submit(__('Create & New'), array('div'=>'submit inline-block', 'name'=>'FormAction[add_and_new]'));?>
		</fieldset>
	<?php echo $this->Form->end();?>
</div>
