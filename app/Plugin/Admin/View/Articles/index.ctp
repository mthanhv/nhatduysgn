<?php echo $this->Html->script(array('Admin.jquery'), array('inline'=>false)); ?>
<?php echo $this->Html->scriptStart(array('inline'=>false)); ?>
	$(document).ready(function() {
		$("#mark-all").click(function(){
			$('input.mark').attr('checked', this.checked);
		});
		$("#delete-multi").click(function(){
			var ids = '';
			$("input.mark:checked").each(function(){
				ids += (this.value + ',');
			});
			if(ids == ''){
				alert("<?php echo __('Please select article');?>");
			} else {
				if(confirm("<?php echo __('Are you sure you want to delete the selected articles ?');?>")){
					ids = ids.substring(0, ids.length - 1);
					$("#marked-values").val(ids);
					$("#deletemulti-form").submit();
				}
			}
			return false;
		});
	});
<?php echo $this->Html->scriptEnd(); ?>
<div class="header-bar">
	<div class="body-header">
		<div class="left header-bar-title"><?php echo $this->Html->link(__('Articles'), array('plugin'=>'admin', 'controller'=>'articles', 'action'=>'index'));?></div>
		<div class="right header-bar-action">
			<?php echo $this->Html->link($this->Html->image('Admin.add.png'), array('plugin'=>'admin', 'controller'=>'articles', 'action'=>'add'), array('title'=>__('Add new article'), 'class'=>'add-article', 'escape' => false)); ?>
			<?php echo $this->Html->link($this->Html->image('Admin.delete.png'), '#', array('title'=>__('Delete selected articles'), 'id'=>'delete-multi', 'escape' => false)); ?>
			<form method="POST" action="/admin/articles/deletemulti" id="deletemulti-form">
				<input type="hidden" id="marked-values" name="markedvalues" value=""/>
			</form>
		</div>
		<div class="clear"></div>
	 </div>
</div>
<div class='form-message'>
	<?php echo $this->Session->flash();?>
</div>
<div class="articles search">
	<?php echo $this->Form->create('Article', array('novalidate'));?>
	<?php echo $this->Form->input('key', array('div'=>'inline-block', 'label'=>false, 'class'=>'w300', 'placeholder'=>__('E.g. Article title')));?>
	<?php echo $this->Form->submit(__('Search'), array('div'=>'inline-block'));?>
	<?php echo $this->Form->end();?>
</div>
<div class="articles index">		
	<table cellpadding="0" cellspacing="0" class="w100p">
        <thead>
		<tr>
			<th class="center w20"><input type="checkbox" id="mark-all"></th>
			<th class="center w60"><?php echo __('Thumb');?></th>
			<th><?php echo $this->Paginator->sort('title');?></th>
			<th class="w90"><?php echo $this->Paginator->sort('ArticleCategory.name', __('Category'));?></th>
			<th class="w90"><?php echo $this->Paginator->sort('created');?></th>
			<th class="actions w70"><?php echo __('Actions');?></th>
		</tr>
        </thead>
        <tbody>
		<?php foreach ($articles as $article): ?>
		<tr>
			<td class="center w20"><input class="mark" type="checkbox" value="<?php echo $article['Article']['id'];?>"></td>
			<td class="center w60"><?php echo $this->Html->image('/files/articles/thumbs/'.$article['Article']['image'], array('class'=>'w50')); ?>&nbsp;</td>
			<td><?php echo h($article['Article']['title']); ?>&nbsp;</td>
			<td class="w90"><?php echo h($article['ArticleCategory']['name']); ?>&nbsp;</td>
			<td class="w90"><?php echo $this->Time->format('d-m-Y', $article['Article']['created']); ?>&nbsp;</td>
			<td class="actions w70">
				<?php echo $this->Html->link($this->Html->image('Admin.action-edit.png'), array('action' => 'edit', $article['Article']['id']), array('title'=>__('Edit'), 'class'=>'edit-article', 'escape'=>false)); ?>
				<?php echo $this->Form->postLink($this->Html->image('Admin.action-delete.png'), array('action' => 'delete', $article['Article']['id']), array('title'=>__('Delete'), 'class'=>'action-delete', 'escape'=>false), __("Are you sure you want to delete article '%s' ?", $article['Article']['title'])); ?>
			</td>
		</tr>
		<?php endforeach; ?>
        </tbody>
        <tfoot>
        <tr>
            <td class="center" colspan="8">
                <div class="paging-count">
                    <span><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} of {:count}')));?></span>
                </div>
                <div class="paging">
                    <?php
                    echo $this->Paginator->first('<<');
                    echo $this->Paginator->prev('<', array(), null, array('class' => 'prev disabled display-none'));
                    echo $this->Paginator->numbers(array('separator' => ''));
                    echo $this->Paginator->next('>', array(), null, array('class' => 'next disabled display-none'));
                    echo $this->Paginator->last('>>');
                    ?>
                </div>
                <div class="clear"></div>
            </td>
        </tr>
        </tfoot>
	</table>
</div>