<?php echo $this->Html->script(array('jquery'), array('inline'=>false));?>
<?php echo $this->Html->scriptStart(array('inline'=>false)); ?>
	$(document).ready(function() {
		$('div.readonly').css("position", "relative");
		$('div.readonly').append("<div onclick='return false;' style='padding: 0; margin: 0; border: none; width: 99%; height: 100%; background: #CCCCCC; position: absolute; top: 0; left: 0; opacity: 0.5; filter: alpha(opacity=50);'></div>");
	});
<?php echo $this->Html->scriptEnd(); ?>
<div class="form-message">
	<?php echo $this->Session->flash();?>
</div>
<div class="left w50p">
	<div class="language-from"><?php echo $from;?></div>
	<div class="product-categories form" id="product-category-edit">
		<?php echo $this->Form->create('ProductCategory', array('type'=>'file', 'novalidate'));?>
			<fieldset>
				<?php echo $this->Form->input('locale', array('type'=>'hidden', 'value'=>$from));?>
				<?php echo $this->Form->input($from.'.ProductCategory.id', array('type'=>'hidden'));?>
				<?php echo $this->Form->input($from.'.ProductCategory.name', array('class'=>'w200'));?>
<!--                --><?php //echo $this->Form->input($from.'.ProductCategory.image', array('type'=>'file', 'class'=>'w400', 'div'=>'input file readonly'));?>
                <?php echo $this->Form->input($from.'.ProductCategory.parent_id', array('class'=>'w200', 'label'=>__('Choose parent product category'), 'multiple'=>false, 'options'=>$product_categories, 'div'=>'input select readonly'));?>
<!--                --><?php //echo $this->Form->input($from.'.ProductCategory.featured', array('div'=>'input checkbox readonly'));?>
<!--                --><?php //echo $this->Form->input($from.'.ProductCategory.description', array('class'=>'w400 editor'));?>
				<?php echo $this->Form->input($from.'.ProductCategory.publish', array('label'=>__('Publish'), 'multiple'=>false, 'options'=>Configure::read('PUBLISH_STATUS'), 'div'=>'input select readonly'));?>
			</fieldset>
		<?php echo $this->Form->end(__('Update'));?>
	</div>
</div>
<div class="right w50p">
	<div class="language-to"><?php echo $to;?></div>
	<div class="product-categories form" id="product-category-edit">
		<?php echo $this->Form->create('ProductCategory', array('type'=>'file', 'novalidate'));?>
			<fieldset>
				<?php echo $this->Form->input('locale', array('type'=>'hidden', 'value'=>$to));?>
                <?php echo $this->Form->input($to.'.ProductCategory.id', array('type'=>'hidden'));?>
                <?php echo $this->Form->input($to.'.ProductCategory.name', array('class'=>'w200'));?>
<!--                --><?php //echo $this->Form->input($to.'.ProductCategory.image', array('type'=>'file', 'class'=>'w400', 'div'=>'input file readonly'));?>
                <?php echo $this->Form->input($to.'.ProductCategory.parent_id', array('class'=>'w200', 'label'=>__('Choose parent product category'), 'multiple'=>false, 'options'=>$product_categories, 'div'=>'input select readonly'));?>
<!--                --><?php //echo $this->Form->input($to.'.ProductCategory.featured', array('div'=>'input checkbox readonly'));?>
<!--                --><?php //echo $this->Form->input($to.'.ProductCategory.description', array('class'=>'w400 editor'));?>
				<?php echo $this->Form->input($to.'.ProductCategory.publish', array('label'=>__('Publish'), 'multiple'=>false, 'options'=>Configure::read('PUBLISH_STATUS'), 'div'=>'input select readonly'));?>
			</fieldset>
		<?php echo $this->Form->end(__('Update'));?>
	</div>
</div>
<div class="clear"></div>
