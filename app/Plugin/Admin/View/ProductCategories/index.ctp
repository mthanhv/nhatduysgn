<?php echo $this->Html->script(array('Admin.jquery'), array('inline'=>false)); ?>
<?php echo $this->Html->scriptStart(array('inline'=>false)); ?>
	$(document).ready(function() {
		$("#mark-all").click(function(){
			$('input.mark').attr('checked', this.checked);
		});
		$("#delete-multi").click(function(){
			var ids = '';
			$("input.mark:checked").each(function(){
				ids += (this.value + ',');
			});
			if(ids == ''){
				alert("<?php echo __('Please select product category');?>");
			} else {
				if(confirm("<?php echo __('Are you sure you want to delete the selected product categories ?');?>")){
					ids = ids.substring(0, ids.length - 1);
					$("#marked-values").val(ids);
					$("#deletemulti-form").submit();
				}
			}
			return false;
		});
	});
<?php echo $this->Html->scriptEnd(); ?>
<div class="header-bar">
	<div class="body-header">
		<div class="left header-bar-title"><?php echo $this->Html->link(__('Product categories'), array('plugin'=>'admin', 'controller'=>'product_categories', 'action'=>'index'));?></div>
		<div class="right header-bar-action">
			<?php echo $this->Html->link($this->Html->image('Admin.add.png'), array('plugin'=>'admin', 'controller'=>'product_categories', 'action'=>'add'), array('title'=>__('Add new product category'), 'class'=>'add-product-category', 'escape' => false)); ?>
			<?php echo $this->Html->link($this->Html->image('Admin.delete.png'), '#', array('title'=>__('Delete selected product categories'), 'id'=>'delete-multi', 'escape' => false)); ?>
			<form method="POST" action="/admin/product_categories/deletemulti" id="deletemulti-form">
				<input type="hidden" id="marked-values" name="markedvalues" value=""/>
			</form>
		</div>
		<div class="clear"></div>
	 </div>
</div>
<div class='form-message'>
	<?php echo $this->Session->flash();?>
</div>
<div class="product_categories index">		
	<table cellpadding="0" cellspacing="0" class="w100p">
        <thead>
            <tr>
                <th class="center w20"><input type="checkbox" id="mark-all"></th>
<!--                <th class="center w60">--><?php //echo __('Thumb');?><!--</th>-->
                <th><?php echo $this->Paginator->sort('ProductCategory.name');?></th>
                <th class="actions w90 center"><?php echo __('Actions');?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($product_categories as $product_category): ?>
            <tr>
                <td class="center w20"><input class="mark" type="checkbox" value="<?php echo $product_category['ProductCategory']['id'];?>"></td>
<!--                <td class="center w60">--><?php //echo $this->Html->image('/files/categories/thumbs/'.$product_category['ProductCategory']['image'], array('class'=>'w50')); ?><!--&nbsp;</td>-->
                <td><?php echo (empty($product_category['ProductCategory']['parent_id']) ? h($product_category['ProductCategory']['name']) : h('----'.$product_category['ProductCategory']['name'])); ?>&nbsp;</td>
                <td class="actions w90 center">
                    <?php echo $this->Html->link($this->Html->image('Admin.move-up.png'), array('action' => 'up', $product_category['ProductCategory']['id']), array('title'=>__('Move up'), 'class'=>'move-up-product-category', 'escape'=>false)); ?>
                    <?php echo $this->Html->link($this->Html->image('Admin.move-down.png'), array('action' => 'down', $product_category['ProductCategory']['id']), array('title'=>__('Move down'), 'class'=>'move-down-product-category', 'escape'=>false)); ?>
                    <?php echo $this->Html->link($this->Html->image('Admin.action-edit.png'), array('action' => 'edit', $product_category['ProductCategory']['id']), array('title'=>__('Edit'), 'class'=>'edit-product-category', 'escape'=>false)); ?>
                    <?php echo $this->Form->postLink($this->Html->image('Admin.action-delete.png'), array('action' => 'delete', $product_category['ProductCategory']['id']), array('title'=>__('Delete'), 'class'=>'action-delete', 'escape'=>false), __("Are you sure you want to delete product category '%s' ?", $product_category['ProductCategory']['name'])); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td class="center" colspan="4">
                    <div class="paging-count">
                        <span><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} of {:count}')));?></span>
                    </div>
                    <div class="paging">
                        <?php
                        echo $this->Paginator->first('<<');
                        echo $this->Paginator->prev('<', array(), null, array('class' => 'prev disabled display-none'));
                        echo $this->Paginator->numbers(array('separator' => ''));
                        echo $this->Paginator->next('>', array(), null, array('class' => 'next disabled display-none'));
                        echo $this->Paginator->last('>>');
                        ?>
                    </div>
                    <div class="clear"></div>
                </td>
            </tr>
        </tfoot>
	</table>
</div>