<?php echo $this->Html->script(array('jquery'), array('inline'=>false));?>
<?php echo $this->Html->scriptStart(array('inline'=>false)); ?>
	$(document).ready(function() {
		$('div.readonly').css("position", "relative");
		$('div.readonly').append("<div onclick='return false;' style='padding: 0; margin: 0; border: none; width: 99%; height: 100%; background: #CCCCCC; position: absolute; top: 0; left: 0; opacity: 0.5; filter: alpha(opacity=50);'></div>");
	});
<?php echo $this->Html->scriptEnd(); ?>
<div class="form-message">
	<?php echo $this->Session->flash();?>
</div>
<div class="left w50p">
	<div class="language-from"><?php echo $from;?></div>
	<div class="menu-items form" id="menu-item-add">
		<?php echo $this->Form->create('MenuItem', array('novalidate'));?>
			<fieldset>
				<?php echo $this->Form->input('locale', array('type'=>'hidden', 'value'=>$from));?>
				<?php echo $this->Form->input($from.'.MenuItem.id', array('type'=>'hidden'));?>
				<?php echo $this->Form->input($from.'.MenuItem.title', array('class'=>'w200'));?>
				<?php echo $this->Form->input($from.'.MenuItem.link', array('class'=>'w200', 'div'=>'input text required readonly'));?>
				<?php echo $this->Form->input($from.'.MenuItem.menu_id', array('class'=>'w200', 'label'=>__('Choose menu'), 'multiple'=>false, 'options'=>$menus, 'div'=>'input select readonly'));?>
				<?php echo $this->Form->input($from.'.MenuItem.parent_id', array('class'=>'w200', 'label'=>__('Choose menu'), 'multiple'=>false, 'options'=>$menu_items, 'div'=>'input select readonly'));?>
				<?php echo $this->Form->input($from.'.MenuItem.publish', array('class'=>'w200', 'label'=>__('Publish'), 'multiple'=>false, 'options'=>Configure::read('PUBLISH_STATUS'), 'div'=>'input select readonly'));?>		
			</fieldset>
		<?php echo $this->Form->end(__('Update'));?>
	</div>
</div>
<div class="right w50p">
	<div class="language-to"><?php echo $to;?></div>
	<div class="menu-items form" id="menu-item-add">
		<?php echo $this->Form->create('MenuItem', array('novalidate'));?>
			<fieldset>
				<?php echo $this->Form->input('locale', array('type'=>'hidden', 'value'=>$to));?>
				<?php echo $this->Form->input($to.'.MenuItem.id', array('type'=>'hidden'));?>
				<?php echo $this->Form->input($to.'.MenuItem.title', array('class'=>'w200'));?>
				<?php echo $this->Form->input($to.'.MenuItem.link', array('class'=>'w200', 'div'=>'input text required readonly'));?>
				<?php echo $this->Form->input($to.'.MenuItem.menu_id', array('class'=>'w200', 'label'=>__('Choose menu'), 'multiple'=>false, 'options'=>$menus, 'div'=>'input select readonly'));?>
				<?php echo $this->Form->input($to.'.MenuItem.parent_id', array('class'=>'w200', 'label'=>__('Choose menu'), 'multiple'=>false, 'options'=>$menu_items, 'div'=>'input select readonly'));?>
				<?php echo $this->Form->input($to.'.MenuItem.publish', array('class'=>'w200', 'label'=>__('Publish'), 'multiple'=>false, 'options'=>Configure::read('PUBLISH_STATUS'), 'div'=>'input select readonly'));?>		
			</fieldset>
		<?php echo $this->Form->end(__('Update'));?>
	</div>
</div>
<div class="clear"></div>
