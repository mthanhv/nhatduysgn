<?php echo $this->Html->script(array('Admin.jquery'), array('inline'=>false)); ?>
<?php echo $this->Html->scriptStart(array('inline'=>false)); ?>
	$(document).ready(function() {
		$("#mark-all").click(function(){
			$('input.mark').attr('checked', this.checked);
		});
		$("#delete-multi").click(function(){
			var ids = '';
			$("input.mark:checked").each(function(){
				ids += (this.value + ',');
			});
			if(ids == ''){
				alert("<?php echo __('Please select menu_item');?>");
			} else {
				if(confirm("<?php echo __('Are you sure you want to delete the selected menu_items ?');?>")){
					ids = ids.substring(0, ids.length - 1);
					$("#marked-values").val(ids);
					$("#deletemulti-form").submit();
				}
			}
			return false;
		});
	});
<?php echo $this->Html->scriptEnd(); ?>
<div class="header-bar">
	<div class="body-header">
		<div class="left header-bar-title"><?php echo $this->Html->link(__('MenuItems'), array('plugin'=>'admin', 'controller'=>'menu_items', 'action'=>'index'));?></div>
		<div class="right header-bar-action">
			<?php echo $this->Html->link($this->Html->image('Admin.add.png'), array('plugin'=>'admin', 'controller'=>'menu_items', 'action'=>'add'), array('title'=>__('Add new menu item'), 'class'=>'add-menu-item', 'escape' => false)); ?>
			<?php echo $this->Html->link($this->Html->image('Admin.delete.png'), '#', array('title'=>__('Delete selected menu items'), 'id'=>'delete-multi', 'escape' => false)); ?>
			<form method="POST" action="/admin/menu_items/deletemulti" id="deletemulti-form">
				<input type="hidden" id="marked-values" name="markedvalues" value=""/>
			</form>
		</div>
		<div class="clear"></div>
	 </div>
</div>
<div class='form-message'>
	<?php echo $this->Session->flash();?>
</div>
<div class="menu_items index">		
	<table cellpadding="0" cellspacing="0" class="w100p">
        <thead>
            <tr>
                <th class="center w20"><input type="checkbox" id="mark-all"></th>
                <th><?php echo $this->Paginator->sort('title');?></th>
                <th><?php echo $this->Paginator->sort('link');?></th>
                <th><?php echo $this->Paginator->sort('Menu.menu', __('Menu'));?></th>
                <th class="center w50"><?php echo $this->Paginator->sort('publish');?></th>
                <th class="actions w90 center"><?php echo __('Actions');?></th>
            </tr>
        </thead>
		<tbody>
            <?php foreach ($menu_items as $menu_item): ?>
            <tr>
                <td class="center w20"><input class="mark" type="checkbox" value="<?php echo $menu_item['MenuItem']['id'];?>"></td>
                <td><?php echo h($menuTreeList[$menu_item['MenuItem']['menu_id']][$menu_item['MenuItem']['id']]); ?>&nbsp;</td>
                <td><?php echo $this->Html->link($menu_item['MenuItem']['link'], $menu_item['MenuItem']['link'], array('skip'=>true)); ?>&nbsp;</td>
                <td><?php echo $this->Html->link($menu_item['Menu']['menu'], array('action'=>'index', $menu_item['MenuItem']['menu_id'])); ?>&nbsp;</td>
                <td class="center w50"><?php echo $this->Html->link($this->Html->image("Admin.{$menu_item['MenuItem']['publish']}.png"), array('controller'=>'menu_items', 'action'=>'toggle', $menu_item['MenuItem']['id']) + $this->request->params['named'], array('escape'=>false)); ?>&nbsp;</td>
                <td class="actions w90 center">
                    <?php echo $this->Html->link($this->Html->image('Admin.move-up.png'), array('action' => 'up', $menu_item['MenuItem']['id']), array('title'=>__('Move up'), 'class'=>'move-up-menu-item', 'escape'=>false)); ?>
                    <?php echo $this->Html->link($this->Html->image('Admin.move-down.png'), array('action' => 'down', $menu_item['MenuItem']['id']), array('title'=>__('Move down'), 'class'=>'move-down-menu-item', 'escape'=>false)); ?>
                    <?php echo $this->Html->link($this->Html->image('Admin.action-edit.png'), array('action' => 'edit', $menu_item['MenuItem']['id']), array('title'=>__('Edit'), 'class'=>'edit-menu-item', 'escape'=>false)); ?>
                    <?php echo $this->Form->postLink($this->Html->image('Admin.action-delete.png'), array('action' => 'delete', $menu_item['MenuItem']['id']), array('title'=>__('Delete'), 'class'=>'action-delete', 'escape'=>false), __("Are you sure you want to delete menu item '%s' ?", $menu_item['MenuItem']['title'])); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td class="center" colspan="6">
                    <div class="paging-count">
                        <span><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} of {:count}')));?></span>
                    </div>
                    <div class="paging">
                        <?php
                            echo $this->Paginator->first('<<');
                            echo $this->Paginator->prev('<', array(), null, array('class' => 'prev disabled display-none'));
                            echo $this->Paginator->numbers(array('separator' => ''));
                            echo $this->Paginator->next('>', array(), null, array('class' => 'next disabled display-none'));
                            echo $this->Paginator->last('>>');
                        ?>
                    </div>
                    <div class="clear"></div>
                </td>
            </tr>
        </tfoot>
	</table>
</div>