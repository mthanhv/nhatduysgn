<?php echo $this->Html->script(array('Admin.jquery'), array('inline'=>false)); ?>
<?php echo $this->Html->scriptStart(array('inline'=>false)); ?>
	$(document).ready(function() {
		$("#select-menu").change(function(){
			var menu_id = this.value;
			$.ajax({
				type: "GET",
				url: '<?php echo Router::url(array('controller'=>'ajax', 'action'=>'menuItems'));?>/'+menu_id,
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function(data){
					$('#select-parent-id').empty();
					$.each(data, function(key, value) {
						$('#select-parent-id').append($("<option></option>").attr("value", key).text(value));
					});
				}
			});
		});
		$("#link-to").change(function(){
			var link_to_category = this.value;
			
			$.ajax({
				type: "GET",
				url: '<?php echo Router::url(array('controller'=>'ajax', 'action'=>'linkTo'));?>/'+link_to_category,
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function(data){
					$('#link-to-data').empty();
					$.each(data, function(key, value) {
						$('#link-to-data').append($("<option></option>").attr("value", key).text(value));
					});
				}
			});
			
			$("#link-value").val('');
			if (link_to_category == <?php echo LINK_TO_HOME; ?>) {
				$("#link-value").val('/');
			}
		});
		$("#link-to-data").change(function(){
			var link_to_data = this.value;
			var link_to_category = $("#link-to").val();
			if (link_to_category == <?php echo LINK_TO_HOME; ?>) {
				$("#link-value").val('/');
			} else if (link_to_category == <?php echo LINK_TO_PRODUCT_LIST; ?>) {
				$("#link-value").val('/products/index/'+link_to_data);
			} else if (link_to_category == <?php echo LINK_TO_PRODUCT; ?>) {
				$("#link-value").val('/products/view/'+link_to_data);
			} else if (link_to_category == <?php echo LINK_TO_ARTICLE_LIST; ?>) {
				$("#link-value").val('/articles/index/'+link_to_data);
			} else if (link_to_category == <?php echo LINK_TO_ARTICLE; ?>) {
				$("#link-value").val('/articles/view/'+link_to_data);
			} else if (link_to_category == <?php echo LINK_TO_CUSTOM; ?>) {
				$("#link-value").val('');
			} else {
				$("#link-value").val('');
			}
		});
	});
<?php echo $this->Html->scriptEnd(); ?>
<div class="header-bar">
	<div class="body-header">
		<div class="left header-bar-title"><?php echo $this->Html->link(__('MenuItems'), array('plugin'=>'admin', 'controller'=>'menu_items', 'action'=>'index'));?></div>
		<div class="right header-bar-action">
			<?php echo $this->Html->link($this->Html->image('Admin.back.png'), array('plugin'=>'admin', 'controller'=>'menu_items', 'action'=>'index'), array('title'=>__('Back'), 'class'=>'back', 'escape' => false)); ?>
		</div>
		<div class="clear"></div>
	 </div>
</div>
<div class="form-message">
	<?php echo $this->Session->flash();?>
</div>
<div class="menu-items form" id="menu-item-edit">
	<?php echo $this->Form->create('MenuItem', array('novalidate'));?>
		<fieldset>
			<?php echo $this->Form->input('id', array('type'=>'hidden'));?>
			<?php echo $this->Form->input('title', array('class'=>'w200'));?>
			<?php echo $this->Form->input('link_to', array('id'=>'link-to', 'class'=>'w200', 'div'=>'input select inline-block', 'options'=>$link_to_categories));?>
			<?php echo $this->Form->input('link_data', array('id'=>'link-to-data', 'options'=>array(0=>__('Select data')), 'class'=>'w300', 'div'=>'input select inline-block'));?>
			<?php echo $this->Form->input('link', array('id'=>'link-value', 'class'=>'w200'));?>
			<?php echo $this->Form->input('menu_id', array('label'=>__('Choose menu'), 'multiple'=>false, 'options'=>$menus, 'id'=>'select-menu'));?>
			<?php echo $this->Form->input('parent_id', array('label'=>__('Parent menu item'), 'multiple'=>false, 'options'=>$menu_items, 'id'=>'select-parent-id'));?>
			<?php echo $this->Form->input('publish', array('label'=>__('Publish'), 'multiple'=>false, 'options'=>Configure::read('PUBLISH_STATUS')));?>		
		</fieldset>
	<?php echo $this->Form->end(__('Update'));?>
</div>
