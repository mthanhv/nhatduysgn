<?php echo $this->Html->script(array('Admin.jquery'), array('inline'=>false)); ?>
<?php echo $this->Html->scriptStart(array('inline'=>false)); ?>
	$(document).ready(function() {
		$("#mark-all").click(function(){
			$('input.mark').attr('checked', this.checked);
		});
		$("#delete-multi").click(function(){
			var ids = '';
			$("input.mark:checked").each(function(){
				ids += (this.value + ',');
			});
			if(ids == ''){
				alert("<?php echo __('Please select contact');?>");
			} else {
				if(confirm("<?php echo __('Are you sure you want to delete the selected contacts ?');?>")){
					ids = ids.substring(0, ids.length - 1);
					$("#marked-values").val(ids);
					$("#deletemulti-form").submit();
				}
			}
			return false;
		});
	});
<?php echo $this->Html->scriptEnd(); ?>
<div class="header-bar">
	<div class="body-header">
		<div class="left header-bar-title"><?php echo $this->Html->link(__('Submissions'), array('plugin'=>'admin', 'controller'=>'contacts', 'action'=>'index'));?></div>
		<div class="right header-bar-action">
			<?php echo $this->Html->link($this->Html->image('Admin.delete.png'), '#', array('title'=>__('Delete selected contacts'), 'id'=>'delete-multi', 'escape' => false)); ?>
			<form method="POST" action="/admin/contacts/deletemulti" id="deletemulti-form">
				<input type="hidden" id="marked-values" name="markedvalues" value=""/>
			</form>
		</div>
		<div class="clear"></div>
	 </div>
</div>
<div class='form-message'>
	<?php echo $this->Session->flash();?>
</div>
<div class="contacts search">
	<?php echo $this->Form->create('Contact', array('novalidate'));?>
	<?php echo $this->Form->input('key', array('div'=>'inline-block', 'label'=>false, 'class'=>'w300', 'placeholder'=>__('E.g. Contact name, Contact title')));?>
	<?php echo $this->Form->submit(__('Search'), array('div'=>'inline-block'));?>
	<?php echo $this->Form->end();?>
</div>
<div class="contacts index">		
	<table cellpadding="0" cellspacing="0" class="w100p">
        <thead>
		<tr>
			<th class="center w20"><input type="checkbox" id="mark-all"></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('phone');?></th>
			<th><?php echo $this->Paginator->sort('title');?></th>
			<th><?php echo $this->Paginator->sort('email');?></th>
			<th class="w150 center"><?php echo $this->Paginator->sort('created');?></th>
			<th class="actions w70 center"><?php echo __('Actions');?></th>
		</tr>
        </thead>
        <tbody>
		<?php foreach ($contacts as $contact): ?>
		<tr>
			<td class="center w20"><input class="mark" type="checkbox" value="<?php echo $contact['Contact']['id'];?>"></td>
			<td><?php echo h($contact['Contact']['name']); ?>&nbsp;</td>
			<td><?php echo h($contact['Contact']['phone']); ?>&nbsp;</td>
			<td><?php echo h($contact['Contact']['title']); ?>&nbsp;</td>
			<td><?php echo $this->Html->link($contact['Contact']['email'], "mailto: {$contact['Contact']['email']}", array('skip'=>true)); ?>&nbsp;</td>
			<td class="w150 center"><?php echo $this->Time->format('d-m-Y h:m:s', $contact['Contact']['created']); ?>&nbsp;</td>
			<td class="actions w70 center">
				<?php echo $this->Form->postLink($this->Html->image('Admin.action-delete.png'), array('action' => 'delete', $contact['Contact']['id']), array('title'=>__('Delete'), 'class'=>'action-delete', 'escape'=>false), __("Are you sure you want to delete contact '%s' ?", $contact['Contact']['title'])); ?>
				<?php echo $this->Html->link($this->Html->image('Admin.action-view.png'), array('action' => 'view', $contact['Contact']['id']), array('title'=>__('View'), 'class'=>'view-contact', 'escape'=>false)); ?>
			</td>
		</tr>
		<?php endforeach; ?>
        </tbody>
        <tfoot>
        <tr>
            <td class="center" colspan="8">
                <div class="paging-count">
                    <span><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} of {:count}')));?></span>
                </div>
                <div class="paging">
                    <?php
                    echo $this->Paginator->first('<<');
                    echo $this->Paginator->prev('<', array(), null, array('class' => 'prev disabled display-none'));
                    echo $this->Paginator->numbers(array('separator' => ''));
                    echo $this->Paginator->next('>', array(), null, array('class' => 'next disabled display-none'));
                    echo $this->Paginator->last('>>');
                    ?>
                </div>
                <div class="clear"></div>
            </td>
        </tr>
        </tfoot>
	</table>
</div>