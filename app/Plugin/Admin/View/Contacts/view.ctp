<div class="header-bar">
	<div class="body-header">
		<div class="left header-bar-title"><?php echo $this->Html->link(__('Submissions'), array('plugin'=>'admin', 'controller'=>'contacts', 'action'=>'index'));?></div>
		<div class="right header-bar-action">
			<?php echo $this->Html->link($this->Html->image('Admin.back.png'), array('plugin'=>'admin', 'controller'=>'contacts', 'action'=>'index'), array('title'=>__('Back'), 'class'=>'back', 'escape' => false)); ?>
		</div>
		<div class="clear"></div>
	 </div>
</div>
<div class="contacts" id="contact-view">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
        <tbody>
        <tr>
            <td style="padding: 5px; border: 1px solid #cccccc;" align="right" valign="top"><?php echo __('Name');?></td>
            <td style="padding: 5px; border: 1px solid #cccccc;" valign="top"><?php echo h($contact['Contact']['name']);?></td>
        </tr>
        <tr>
            <td style="padding: 5px; border: 1px solid #cccccc;" align="right" valign="top"><?php echo __('Phone');?></td>
            <td style="padding: 5px; border: 1px solid #cccccc;" valign="top"><?php echo h($contact['Contact']['phone']);?></td>
        </tr>
        <tr>
            <td style="padding: 5px; border: 1px solid #cccccc;" align="right" valign="top"><?php echo __('Address');?></td>
            <td style="padding: 5px; border: 1px solid #cccccc;" valign="top"><?php echo h($contact['Contact']['address']);?></td>
        </tr>
        <tr>
            <td style="padding: 5px; border: 1px solid #cccccc;" align="right" valign="top"><?php echo __('Email address');?></td>
            <td style="padding: 5px; border: 1px solid #cccccc;" valign="top"><?php echo h($contact['Contact']['email']);?></td>
        </tr>
        <tr>
            <td style="padding: 5px; border: 1px solid #cccccc;" align="right" valign="top"><?php echo __('Title');?></td>
            <td style="padding: 5px; border: 1px solid #cccccc;" valign="top"><?php echo h($contact['Contact']['title']);?></td>
        </tr>
        <tr>
            <td style="padding: 5px; border: 1px solid #cccccc;" align="right" valign="top"><?php echo __('Demand');?></td>
            <td style="padding: 5px; border: 1px solid #cccccc;" valign="top"><?php echo h($contact['Contact']['content']);?></td>
        </tr>
        </tbody>
    </table>
</div>