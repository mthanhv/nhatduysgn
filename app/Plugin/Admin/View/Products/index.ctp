<?php echo $this->Html->script(array('Admin.jquery'), array('inline'=>false)); ?>
<?php echo $this->Html->scriptStart(array('inline'=>false)); ?>
	$(document).ready(function() {
		$("#mark-all").click(function(){
			$('input.mark').attr('checked', this.checked);
		});
		$("#delete-multi").click(function(){
			var ids = '';
			$("input.mark:checked").each(function(){
				ids += (this.value + ',');
			});
			if(ids == ''){
				alert("<?php echo __('Please select product');?>");
			} else {
				if(confirm("<?php echo __('Are you sure you want to delete the selected products ?');?>")){
					ids = ids.substring(0, ids.length - 1);
					$("#deletemulti-form #marked-values").val(ids);
					$("#deletemulti-form").submit();
				}
			}
			return false;
		});
		$("#clone-multi").click(function(){
			var ids = '';
			$("input.mark:checked").each(function(){
				ids += (this.value + ',');
			});
			if(ids == ''){
				alert("<?php echo __('Please select product');?>");
			} else {
				ids = ids.substring(0, ids.length - 1);
				$("#clonemulti-form #marked-values").val(ids);
				$("#clonemulti-form").submit();
			}
			return false;
		});
		$("#save-ordering").click(function(){
			var proid = 0;
			var ordering = 0;
			var orderings = new Array();
			$("input.ordering").each(function(){
				proid = $(this).attr('proid');
				ordering = this.value;
				orderings.push({'proid': proid, 'ordering': ordering});
			});
			
			$.ajax({
				url: '<?php echo $this->Html->url(array('plugin'=>'admin', 'controller'=>'products', 'action'=>'saveordering'));?>',
				type: 'post',
				data: {'orderings': orderings},
				dataType: 'json',
				success: function(data){
					location.reload();
				}
			});
			return false;
		});
	});
<?php echo $this->Html->scriptEnd(); ?>
<div class="header-bar">
	<div class="body-header">
		<div class="left header-bar-title"><?php echo $this->Html->link(__('Products'), array('plugin'=>'admin', 'controller'=>'products', 'action'=>'index'));?></div>
		<div class="right header-bar-action">
			<?php echo $this->Html->link($this->Html->image('Admin.add.png'), array('plugin'=>'admin', 'controller'=>'products', 'action'=>'add'), array('title'=>__('Add new product'), 'class'=>'add-product', 'escape' => false)); ?>
			<?php echo $this->Html->link($this->Html->image('Admin.clone.png'), '#', array('title'=>__('Clone selected products'), 'id'=>'clone-multi', 'escape' => false)); ?>
			<?php echo $this->Html->link($this->Html->image('Admin.delete.png'), '#', array('title'=>__('Delete selected products'), 'id'=>'delete-multi', 'escape' => false)); ?>
			<form method="POST" action="/admin/products/deletemulti" id="deletemulti-form">
				<input type="hidden" id="marked-values" name="markedvalues" value=""/>
			</form>
			<form method="POST" action="/admin/products/clonemulti" id="clonemulti-form">
				<input type="hidden" id="marked-values" name="markedvalues" value=""/>
			</form>
		</div>
		<div class="clear"></div>
	 </div>
</div>
<div class='form-message'>
	<?php echo $this->Session->flash();?>
</div>
<div class="products search">
	<?php echo $this->Form->create('Product', array('novalidate'));?>
	<?php echo $this->Form->input('key', array('div'=>'inline-block', 'label'=>false, 'class'=>'w300', 'placeholder'=>__('E.g. Product title')));?>
	<?php echo $this->Form->submit(__('Search'), array('div'=>'inline-block'));?>
	<?php echo $this->Form->end();?>
</div>
<div class="products index">		
	<table cellpadding="0" cellspacing="0" class="w100p">
        <thead>
            <tr>
                <th class="center w20"><input type="checkbox" id="mark-all"></th>
                <th class="center w60"><?php echo __('Thumb');?></th>
                <th><?php echo $this->Paginator->sort('title');?></th>
                <th><?php echo $this->Paginator->sort('attachment');?></th>
                <th class="w150"><?php echo $this->Paginator->sort('ProductCategory.name', __('Category'));?></th>
                <th class="center w50"><?php echo $this->Paginator->sort('publish');?></th>
                <th class="center w50"><?php echo $this->Paginator->sort('ordering');?><div id="save-ordering"><?php echo $this->Html->image('Admin.action-save.png');?></div></th>
                <th class="w90 center"><?php echo $this->Paginator->sort('created');?></th>
                <th class="actions w70 center"><?php echo __('Actions');?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($products as $product): ?>
            <tr>
                <td class="center w20"><input class="mark" type="checkbox" value="<?php echo $product['Product']['id'];?>"></td>
                <td class="center w60"><?php echo $this->Html->image('/files/products/thumbs/'.$product['Product']['image'], array('class'=>'w50')); ?>&nbsp;</td>
                <td><?php echo h($product['Product']['title']); ?>&nbsp;</td>
                <td><?php echo $this->Html->link($product['Product']['attachment'], "/files/products/attachments/{$product['Product']['attachment']}"); ?>&nbsp;</td>
                <td class="w150"><?php echo h($product['ProductCategory']['name']); ?>&nbsp;</td>
                <td class="center w50"><?php echo $this->Html->link($this->Html->image("Admin.{$product['Product']['publish']}.png"), array('controller'=>'products', 'action'=>'toggle', $product['Product']['id']) + $this->request->params['named'], array('escape'=>false)); ?>&nbsp;</td>
                <td class="center w50"><input type="text" class="ordering w30 center" proid="<?php echo $product['Product']['id'];?>" value="<?php echo $product['Product']['ordering'];?>"></td>
                <td class="w90 center"><?php echo $this->Time->format('d-m-Y', $product['Product']['created']); ?>&nbsp;</td>
                <td class="actions w70 center">
                    <?php echo $this->Html->link($this->Html->image('Admin.action-edit.png'), array('action' => 'edit', $product['Product']['id']), array('title'=>__('Edit'), 'class'=>'edit-product', 'escape'=>false)); ?>
                    <?php echo $this->Form->postLink($this->Html->image('Admin.action-delete.png'), array('action' => 'delete', $product['Product']['id']), array('title'=>__('Delete'), 'class'=>'action-delete', 'escape'=>false), __("Are you sure you want to delete product '%s' ?", $product['Product']['title'])); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td class="center" colspan="9">
                    <div class="paging-count">
                        <span><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} of {:count}')));?></span>
                    </div>
                    <div class="paging">
                        <?php
                        echo $this->Paginator->first('<<');
                        echo $this->Paginator->prev('<', array(), null, array('class' => 'prev disabled display-none'));
                        echo $this->Paginator->numbers(array('separator' => ''));
                        echo $this->Paginator->next('>', array(), null, array('class' => 'next disabled display-none'));
                        echo $this->Paginator->last('>>');
                        ?>
                    </div>
                    <div class="clear"></div>
                </td>
            </tr>
        </tfoot>
	</table>
</div>