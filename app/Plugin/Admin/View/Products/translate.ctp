<?php echo $this->Html->script(array('jquery', 'tinymce/tinymce.min.js'), array('inline'=>false));?>
<?php echo $this->Html->scriptStart(array('inline'=>false)); ?>
	tinymce.init({
		relative_urls: false,
		menubar: false,
		statusbar: false,
		selector: "textarea.editor",
		theme: "modern",
		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			"save table contextmenu directionality emoticons template paste textcolor responsivefilemanager"
		],
		content_css: "/css/editor.css",
		toolbar: "bold italic fontselect fontsizeselect forecolor backcolor | alignleft aligncenter alignright alignjustify | outdent indent | link image media table | code preview fullscreen",
		image_advtab: true,

		external_filemanager_path: "/filemanager/",
		filemanager_title: "Filemanager" ,
		external_plugins: {"filemanager" : "/filemanager/plugin.min.js"}
	});
	$(document).ready(function() {
		$('div.readonly').css("position", "relative");
		$('div.readonly').append("<div onclick='return false;' style='padding: 0; margin: 0; border: none; width: 99%; height: 100%; background: #CCCCCC; position: absolute; top: 0; left: 0; opacity: 0.5; filter: alpha(opacity=50);'></div>");
	});
<?php echo $this->Html->scriptEnd(); ?>
<div class="form-message">
	<?php echo $this->Session->flash();?>
</div>
<div class="left w50p">
	<div class="language-from"><?php echo $from;?></div>
	<div class="products form" id="product-edit">
		<?php echo $this->Form->create('Product', array('type'=>'file', 'novalidate'));?>
			<fieldset>
				<?php echo $this->Form->input('locale', array('type'=>'hidden', 'value'=>$from));?>
				<?php echo $this->Form->input($from.'.Product.id', array('type'=>'hidden'));?>
                <?php echo $this->Form->input($from.'.Product.title', array('class'=>'w400'));?>
                <?php echo $this->Form->input($from.'.Product.code', array('class'=>'w200', 'div'=>'input text readonly'));?>
				<?php echo $this->Form->input($from.'.Product.image', array('type'=>'file', 'class'=>'w400', 'div'=>'input file readonly'));?>
				<?php echo $this->Form->input($from.'.Product.attachment', array('type'=>'file', 'div'=>'input file readonly', 'label'=>__('Attachment')));?>
				<?php echo $this->Form->input($from.'.Product.featured', array('div'=>'input checkbox readonly'));?>
                <?php echo $this->Form->input($from.'.Product.product_category_id', array('type'=>'hidden', 'value'=>key($product_categories)));?>
                <?php echo $this->Form->input($from.'.Product.brand_id', array('type'=>'hidden', 'value'=>key($brands)));?>
                <?php echo $this->Form->input($from.'.Product.supplier_id', array('type'=>'hidden', 'value'=>key($suppliers)));?>
				<?php echo $this->Form->input($from.'.Product.publish', array('label'=>__('Publish'), 'multiple'=>false, 'options'=>Configure::read('PUBLISH_STATUS'), 'div'=>'input select readonly'));?>
				<?php echo $this->Form->input($from.'.Product.ordering', array('class'=>'w200', 'div'=>'input text readonly'));?>
				<?php echo $this->Form->input($from.'.Product.origin', array('class'=>'w200', 'div'=>'input text readonly'));?>
				<?php echo $this->Form->input($from.'.Product.content', array('class'=>'w400 editor'));?>
				<?php echo $this->Form->input($from.'.Product.specification', array('class'=>'w400 editor'));?>
				<?php echo $this->Form->input($from.'.Product.meta_keywords', array('class'=>'w400', 'rows'=>3));?>
				<?php echo $this->Form->input($from.'.Product.meta_description', array('class'=>'w400', 'rows'=>3));?>
			</fieldset>
		<?php echo $this->Form->end(__('Update'));?>
	</div>
</div>
<div class="right w50p">
	<div class="language-to"><?php echo $to;?></div>
	<div class="products form" id="product-edit">
		<?php echo $this->Form->create('Product', array('type'=>'file', 'novalidate'));?>
			<fieldset>
				<?php echo $this->Form->input('locale', array('type'=>'hidden', 'value'=>$to));?>
				<?php echo $this->Form->input($to.'.Product.id', array('type'=>'hidden'));?>
                <?php echo $this->Form->input($to.'.Product.title', array('class'=>'w400'));?>
                <?php echo $this->Form->input($to.'.Product.code', array('class'=>'w200', 'div'=>'input text readonly'));?>
				<?php echo $this->Form->input($to.'.Product.image', array('type'=>'file', 'class'=>'w400', 'div'=>'input file readonly'));?>
				<?php echo $this->Form->input($to.'.Product.attachment', array('type'=>'file', 'div'=>'input file readonly', 'label'=>__('Attachment')));?>
				<?php echo $this->Form->input($to.'.Product.featured', array('div'=>'input checkbox readonly'));?>
                <?php echo $this->Form->input($to.'.Product.product_category_id', array('type'=>'hidden', 'value'=>key($product_categories)));?>
                <?php echo $this->Form->input($to.'.Product.brand_id', array('type'=>'hidden', 'value'=>key($brands)));?>
                <?php echo $this->Form->input($to.'.Product.supplier_id', array('type'=>'hidden', 'value'=>key($suppliers)));?>
				<?php echo $this->Form->input($to.'.Product.publish', array('label'=>__('Publish'), 'multiple'=>false, 'options'=>Configure::read('PUBLISH_STATUS'), 'div'=>'input select readonly'));?>
				<?php echo $this->Form->input($to.'.Product.ordering', array('class'=>'w200', 'div'=>'input text readonly'));?>
				<?php echo $this->Form->input($to.'.Product.origin', array('class'=>'w200', 'div'=>'input text readonly'));?>
				<?php echo $this->Form->input($to.'.Product.content', array('class'=>'w400 editor'));?>
				<?php echo $this->Form->input($to.'.Product.specification', array('class'=>'w400 editor'));?>
				<?php echo $this->Form->input($to.'.Product.meta_keywords', array('class'=>'w400', 'rows'=>3));?>
				<?php echo $this->Form->input($to.'.Product.meta_description', array('class'=>'w400', 'rows'=>3));?>
			</fieldset>
		<?php echo $this->Form->end(__('Update'));?>
	</div>
</div>
<div class="clear"></div>