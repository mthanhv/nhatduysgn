<?php echo $this->Html->script(array('Admin.jquery'), array('inline'=>false)); ?>
<?php echo $this->Html->scriptStart(array('inline'=>false)); ?>
	$(document).ready(function() {
		$("#language-from").change(function(){
			var selected_value = $("#language-from").val();
			$("#language-to option").removeAttr('disabled');
			$("#language-to option[value=" + selected_value + "]").attr('disabled','disabled');
		});
        $("#translation-model").change(function(){
            $("#translation-form").submit();
        });
    })
<?php echo $this->Html->scriptEnd(); ?>
<div class="header-bar">
	<div class="body-header">
		<div class="left header-bar-title"><?php echo $this->Html->link(__('Dashboard'), array('plugin'=>'admin', 'controller'=>'admin', 'action'=>'index'));?></div>
		<div class="right header-bar-action">
			<?php echo $this->Html->link($this->Html->image('Admin.back.png'), array('plugin'=>'admin', 'controller'=>'admin', 'action'=>'index'), array('title'=>__('Back'), 'class'=>'back', 'escape' => false)); ?>
		</div>
		<div class="clear"></div>
	 </div>
</div>
<?php echo $this->Form->create('Translation', array('novalidate', 'id'=>'translation-form'));?>
	<?php echo $this->Form->input('from', array('options' => $translation_from, 'id' => 'language-from', 'div' => 'input select inline-block'));?>
	<?php echo $this->Form->input('to', array('options' => $translation_to, 'id' => 'language-to', 'div' => 'input select inline-block'));?>
	<?php echo $this->Form->input('model', array('options' => $translation_models, 'id' => 'translation-model', 'div' => 'input select inline-block'));?>
	<?php echo $this->Form->submit(__('Send'), array('div' => 'submit inline-block'));?>
<?php echo $this->Form->end();?>
<?php if (!empty($datas)) { ?>
<div class="datas index">		
	<table cellpadding="0" cellspacing="0" class="w100p">
        <thead>
		<tr>
			<th class="center w20"><?php echo __('Id');?></th>
			<th><?php echo __('Title');?></th>
			<th class="w100"><?php echo __('Translated ?');?></th>
			<th class="actions w100 center"><?php echo __('Actions');?></th>
		</tr>
        </thead>
        <tbody>
		<?php foreach ($datas as $data): ?>
		<tr>
			<td class="center w20"><?php echo h($data[$model][$primaryKey]); ?>&nbsp;</td>
			<td><?php echo h($data[$model][$displayField]); ?>&nbsp;</td>
			<td class="w100"><?php echo $this->Html->image("/admin/translator/translated/{$model}/{$to}/{$data[$model][$primaryKey]}"); ?>&nbsp;</td>
			<td class="actions w100 center">
				<?php echo $this->Html->link(__('Translate'), array('action' => 'translate', $data[$model][$primaryKey]) + $this->request->params['named'], array('id' => 'translator'));?>
			</td>
		</tr>
		<?php endforeach; ?>
        </tbody>
        <tfoot>
        <tr>
            <td class="center" colspan="8">
                <div class="paging-count">
                    <span><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} of {:count}')));?></span>
                </div>
                <div class="paging">
                    <?php
                    echo $this->Paginator->first('<<');
                    echo $this->Paginator->prev('<', array(), null, array('class' => 'prev disabled display-none'));
                    echo $this->Paginator->numbers(array('separator' => ''));
                    echo $this->Paginator->next('>', array(), null, array('class' => 'next disabled display-none'));
                    echo $this->Paginator->last('>>');
                    ?>
                </div>
                <div class="clear"></div>
            </td>
        </tr>
        </tfoot>
	</table>
</div>
<?php } ?>