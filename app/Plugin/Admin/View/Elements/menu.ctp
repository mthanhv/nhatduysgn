<ul class="menu left-menu">
	<li><?php echo $this->Html->link(__('Dashboard'), array('plugin' => 'admin', 'controller'=> 'admin', 'action'=>'index')); ?></li>
	<li><?php echo $this->Html->link(__('Menu Items'), array('plugin' => 'admin', 'controller'=> 'menu_items', 'action'=>'index')); ?></li>
    <li><?php echo $this->Html->link(__('Products'), array('plugin' => 'admin', 'controller'=> 'products', 'action'=>'index')); ?></li>
    <li><?php echo $this->Html->link(__('Product Categories'), array('plugin' => 'admin', 'controller' => 'product_categories', 'action' => 'index')); ?></li>
    <li><?php echo $this->Html->link(__('Brands'), array('plugin' => 'admin', 'controller' => 'brands', 'action' => 'index')); ?></li>
    <li><?php echo $this->Html->link(__('Suppliers'), array('plugin' => 'admin', 'controller' => 'suppliers', 'action' => 'index')); ?></li>
    <li><?php echo $this->Html->link(__('Customers'), array('plugin' => 'admin', 'controller' => 'customers', 'action' => 'index')); ?></li>
    <li><?php echo $this->Html->link(__('Articles'), array('plugin' => 'admin','controller'=> 'articles', 'action'=>'index')); ?></li>
	<li><?php echo $this->Html->link(__('Submissions'), array('plugin' => 'admin', 'controller'=> 'contacts', 'action'=>'index')); ?></li>
	<li><?php echo $this->Html->link(__('Blocks'), array('plugin' => 'admin', 'controller' => 'blocks', 'action' => 'index')); ?></li>
	<li><?php echo $this->Html->link(__('Slides'), array('plugin' => 'admin', 'controller'=> 'slides', 'action'=>'index')); ?></li>
    <li><?php echo $this->Html->link(__('Users'), array('plugin' => 'admin', 'controller'=> 'users', 'action'=>'index')); ?></li>
	<li><?php echo $this->Html->link(__('Settings'), array('plugin' => 'admin', 'controller' => 'settings', 'action' => 'edit')); ?></li>
	<!-- <li><?php echo $this->Html->link(__('Translation'), array('plugin' => 'admin', 'controller' => 'translator', 'action' => 'index')); ?></li> -->
</ul>