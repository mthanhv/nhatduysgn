<div class="user forgot-password-form">
	<?php echo $this->Form->create('User', array('url' => array('plugin' => 'admin', 'controller' => 'user', 'action' => 'forgotpassword'), 'novalidate'));?>
		<fieldset>
			<div class="forgot-password-message"><?php echo $this->Session->flash();?></div>
			<?php echo $this->Form->input('email', array('label' => __('Email'), 'class'=>'w250'));?>
			<?php echo $this->Form->submit(__('Submit'));?>
			<div class="login">
				<?php echo $this->Html->link(__('Login'), array('plugin' => 'admin', 'controller' => 'user', 'action' => 'login'));?>
			</div>
		</fieldset>
	<?php echo $this->Form->end(); ?>
</div>