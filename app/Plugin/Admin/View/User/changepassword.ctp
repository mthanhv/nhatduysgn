<div class="user change-password-form">
	<?php echo $this->Form->create('User', array('url' => array('plugin' => 'admin', 'controller' => 'user', 'action' => 'changepassword', $passwordchangecode), 'novalidate'));?>
		<fieldset>
			<div class="change-password-message"><?php echo $this->Session->flash();?></div>
			<?php echo $this->Form->input('password', array('type' => 'password', 'label' => __('Password'), 'class'=>'w250'));?>
			<?php echo $this->Form->input('confirm_password', array('type' => 'password', 'label' => __('Retype your password'), 'class'=>'w250'));?>
			<?php echo $this->Form->submit(__('Submit'));?>
			<div class="login">
				<?php echo $this->Html->link(__('Login ?'), array('plugin' => 'admin', 'controller' => 'user', 'action' => 'login'));?>
			</div>
		</fieldset>
	<?php echo $this->Form->end(); ?>
</div>