<div class="user login-form">
	<?php echo $this->Form->create('User', array('url' => array('plugin' => 'admin', 'controller' => 'user', 'action' => 'login'), 'novalidate'));?>
		<fieldset>
			<div class="login-message"><?php echo $this->Session->flash();?></div>
			<?php echo $this->Form->input('user_name', array('label' => __('User name'), 'class'=>'w250'));?>
			<?php echo $this->Form->input('password', array('label' => __('Password'), 'class'=>'w250'));?>
			<?php echo $this->Form->submit(__('Login'));?>
			<div class="forgot-password">
				<?php echo $this->Html->link(__('Forgot password ?'), array('plugin' => 'admin', 'controller' => 'user', 'action' => 'forgotpassword'));?>
			</div>
		</fieldset>
	<?php echo $this->Form->end(); ?>
</div>