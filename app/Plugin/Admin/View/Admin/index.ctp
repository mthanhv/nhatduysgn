<div class="contacts lastest">
	<h3><?php echo __('Top 10 lastest contacts');?></h3>
	<table cellpadding="0" cellspacing="0" class="w100p">
		<tr>
			<th class="center w20"><?php echo __('#');?></th>
			<th><?php echo __('Name');?></th>
			<th><?php echo __('Phone');?></th>
			<th><?php echo __('Title');?></th>
			<th><?php echo __('Email');?></th>
			<th class="w90 center"><?php echo __('Created');?></th>
			<th class="w20 actions center"></th>
		</tr>
		<?php foreach ($contacts as $index => $contact): ?>
		<tr>
			<td class="center w20"><?php echo ($index + 1);?></td>
			<td><?php echo h($contact['Contact']['name']); ?>&nbsp;</td>
			<td><?php echo h($contact['Contact']['phone']); ?>&nbsp;</td>
			<td><?php echo h($contact['Contact']['title']); ?>&nbsp;</td>
			<td><?php echo $this->Html->link($contact['Contact']['email'], "mailto: {$contact['Contact']['email']}", array('skip'=>true)); ?>&nbsp;</td>
			<td class="w90 center"><?php echo $this->Time->format('d-m-Y', $contact['Contact']['created']); ?>&nbsp;</td>
			<td class="w20 actions center">
				<?php echo $this->Html->link($this->Html->image('Admin.action-view.png'), array('controller' => 'contacts', 'action' => 'view', $contact['Contact']['id']), array('title'=>__('View'), 'class'=>'view-contact', 'escape'=>false)); ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
</div>
<div class="products lastest">
	<h3><?php echo __('Top 10 lastest products');?></h3>
	<table cellpadding="0" cellspacing="0" class="w100p">
		<tr>
			<th class="center w20"><?php echo __('#');?></th>
			<th class="center w60"><?php echo __('Thumb');?></th>
			<th><?php echo __('Title');?></th>
			<th class="w200"><?php echo __('Category');?></th>
			<th class="w90 center"><?php echo __('Created');?></th>
		</tr>
		<?php foreach ($products as $index => $product): ?>
		<tr>
			<td class="center w20"><?php echo ($index + 1);?></td>
			<td class="center w60"><?php echo $this->Html->image('/files/products/thumbs/'.$product['Product']['image'], array('class'=>'w50')); ?>&nbsp;</td>
			<td><?php echo h($product['Product']['title']); ?>&nbsp;</td>
			<td class="w200"><?php echo h($product['ProductCategory']['name']); ?>&nbsp;</td>
			<td class="w90 center"><?php echo $this->Time->format('d-m-Y', $product['Product']['created']); ?>&nbsp;</td>
		</tr>
		<?php endforeach; ?>
	</table>
</div>