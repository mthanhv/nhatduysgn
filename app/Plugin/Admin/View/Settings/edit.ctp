<div class="header-bar">
	<div class="body-header">
		<div class="left header-bar-title"><?php echo $this->Html->link(__('Dashboard'), array('plugin'=>'admin', 'controller'=>'admin', 'action'=>'index'));?></div>
		<div class="right header-bar-action">
			<?php echo $this->Html->link($this->Html->image('Admin.back.png'), array('plugin'=>'admin', 'controller'=>'admin', 'action'=>'index'), array('title'=>__('Back'), 'class'=>'back', 'escape' => false)); ?>
		</div>
		<div class="clear"></div>
	 </div>
</div>
<div class='form-message'>
	<?php echo $this->Session->flash();?>
</div>
<div class="settings form">
	<?php echo $this->Form->create('Setting', array('novalidate'));?>
	<fieldset>
		<?php echo $this->Form->input('id', array('value'=>1, 'type'=>'hidden'));?>
		<?php echo $this->Form->input('site_name', array('class'=>'w400'));?>
		<?php echo $this->Form->input('site_slogan', array('class'=>'w400'));?>
        <?php echo $this->Form->input('site_emails', array('class'=>'w400'));?>
		<?php echo $this->Form->input('site_keywords', array('class'=>'w400', 'rows'=>5));?>
		<?php echo $this->Form->input('site_description', array('class'=>'w400', 'rows'=>8));?>
	</fieldset>
	<?php echo $this->Form->end(__('Save'));?>
</div>