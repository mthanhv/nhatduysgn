<?php echo $this->Html->script(array('jquery'), array('inline'=>false));?>
<?php echo $this->Html->scriptStart(array('inline'=>false)); ?>
	$(document).ready(function() {
		$('div.readonly').css("position", "relative");
		$('div.readonly').append("<div onclick='return false;' style='padding: 0; margin: 0; border: none; width: 99%; height: 100%; background: #CCCCCC; position: absolute; top: 0; left: 0; opacity: 0.5; filter: alpha(opacity=50);'></div>");
	});
<?php echo $this->Html->scriptEnd(); ?>
<div class="form-message">
	<?php echo $this->Session->flash();?>
</div>
<div class="left w50p">
	<div class="language-from"><?php echo $from;?></div>
	<div class="settings form">
		<?php echo $this->Form->create('Setting', array('novalidate'));?>
		<fieldset>
			<?php echo $this->Form->input('locale', array('type'=>'hidden', 'value'=>$from));?>
			<?php echo $this->Form->input($from.'.Setting.id', array('value'=>1, 'type'=>'hidden'));?>
			<?php echo $this->Form->input($from.'.Setting.site_name', array('class'=>'w400'));?>
			<?php echo $this->Form->input($from.'.Setting.site_slogan', array('class'=>'w400'));?>
			<?php echo $this->Form->input($from.'.Setting.site_emails', array('class'=>'w400', 'div'=>'input text readonly'));?>
			<?php echo $this->Form->input($from.'.Setting.site_keywords', array('class'=>'w400', 'rows'=>3));?>
			<?php echo $this->Form->input($from.'.Setting.site_description', array('class'=>'w400', 'rows'=>3));?>
		</fieldset>
		<?php echo $this->Form->end(__('Save'));?>
	</div>
</div>
<div class="right w50p">
	<div class="language-to"><?php echo $to;?></div>
	<div class="settings form">
		<?php echo $this->Form->create('Setting', array('novalidate'));?>
		<fieldset>
			<?php echo $this->Form->input('locale', array('type'=>'hidden', 'value'=>$to));?>
			<?php echo $this->Form->input($to.'.Setting.id', array('value'=>1, 'type'=>'hidden'));?>
			<?php echo $this->Form->input($to.'.Setting.site_name', array('class'=>'w400'));?>
			<?php echo $this->Form->input($to.'.Setting.site_slogan', array('class'=>'w400'));?>
			<?php echo $this->Form->input($to.'.Setting.site_emails', array('class'=>'w400', 'div'=>'input text readonly'));?>
			<?php echo $this->Form->input($to.'.Setting.site_keywords', array('class'=>'w400', 'rows'=>3));?>
			<?php echo $this->Form->input($to.'.Setting.site_description', array('class'=>'w400', 'rows'=>3));?>
		</fieldset>
		<?php echo $this->Form->end(__('Save'));?>
	</div>
</div>
<div class="clear"></div>