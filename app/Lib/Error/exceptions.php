<?php
class MissingModuleException extends CakeException {
	protected $_messageTemplate = 'Module class %s could not be found.';

	public function __construct($message, $code = 404) {
		parent::__construct($message, $code);
	}
}