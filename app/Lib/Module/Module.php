<?php
App::uses('ClassRegistry', 'Utility');

class Module extends Object {
	public $name = null;
	public $methods = array();
	public $viewVars = array();
	public $uses = true;
	protected $_controller = null;
	protected $_position = null;

	public function __construct(Controller $controller = null) {
		if ($this->name === null) {
			$this->name = substr(get_class($this), 0, -6);
		}
		$this->_controller = $controller;
		
		$childMethods = get_class_methods($this);
		$parentMethods = get_class_methods('Module');
		$this->methods = array_diff($childMethods, $parentMethods);
		
		parent::__construct();
	}
	
	public function invokeAction($action, $position, $data, $options) {
		try {
			$this->_position = $position;
			
			$method = new ReflectionMethod($this, $action);
			
			if ($method->name[0] === '_' || !$method->isPublic() || !in_array($method->name, $this->methods)) {
				throw new PrivateActionException(array(
					'module' => $this->name . "Module",
					'action' => $action
				));
			}
			return $method->invokeArgs($this, $data);
		} catch (ReflectionException $e) {
			throw new MissingActionException(array(
				'module' => $this->name . "Module",
				'action' => $action
			));
		}
	}
	
	public function set($one, $two = null) {
		if (is_array($one)) {
			if (is_array($two)) {
				$data = array_combine($one, $two);
			} else {
				$data = $one;
			}
		} else {
			$data = array($one => $two);
		}
		$this->viewVars = $data + $this->viewVars;
	}
	
	public function __get($name) {
		if (isset($this->$name)) return $this->$name;
		else if (isset($this->_controller->$name)) return $this->_controller->$name;
		else return null;
	}
	
	public function loadModel($modelClass = null, $id = null) {
		if ($modelClass === null) {
			$modelClass = $this->modelClass;
		}

		$this->uses = ($this->uses) ? (array)$this->uses : array();
		if (!in_array($modelClass, $this->uses, true)) {
			$this->uses[] = $modelClass;
		}

		list($plugin, $modelClass) = pluginSplit($modelClass, true);

		$this->{$modelClass} = ClassRegistry::init(array(
			'class' => $plugin . $modelClass, 'alias' => $modelClass, 'id' => $id
		));
		if (!$this->{$modelClass}) {
			throw new MissingModelException($modelClass);
		}
		return true;
	}
}
