<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package        app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    var $helpers = array('Form', 'Html', 'Session', 'Utility', 'Authen', 'Site');
    var $components = array('Session', 'Cookie', 'Utility', 'Authen', 'Site');

    public function beforeFilter()
    {
        $language = $this->detectLanguage('FE');
        $this->setLanguage($language, 'FE');

        $this->Authen->authenticate($this);

        $this->module('Menu', 'index', 'navigation', array('menu' => MAIN_MENU), array(
            'before' => '<div id="main-menu" class="moduletable">',
            'after' => '</div>'
        ));

        $this->module('ProductCategory', 'index', 'left', array(), array(
            'before' => '<div class="category-tree"><h2>' . __('Categories') . '</h2>',
            'after' => '</div>',
        ));

        $this->module('Brand', 'index', 'left', array(), array(
            'before' => '<div class="brands-products small-device"><h2>' . __('Brands') . '</h2>',
            'after' => '</div>',
        ));

        $this->module('Tag', 'index', 'left', array(), array(
            'before' => '<div class="tags-module small-device"><h2>' . __('Tags') . '</h2>',
            'after' => '</div>'
        ));

        $this->module('Article', 'latest', 'footer_top', array('category_id' => ARTICLE_CATEGORY_NEWS, 'limit' => 4), array(
            'before' => '<div class="single-widget"><h2>' . __('Latest Articles') . '</h2>',
            'after' => '</div>'
        ));

        $this->module('Supplier', 'index', 'suppliers', array(), array());

        if (isset($this->request->params['named']['i'])) $this->Session->write('Menu.selected', $this->request->params['named']['i']);
    }

    protected function detectLanguage($component = null)
    {
        $language = null;

        if (!$language && isset($this->params['language'])) {
            $language = $this->params['language'];
        }

        if (!$language && $this->Session->check("Config.language{$component}")) {
            $language = $this->Session->read("Config.language{$component}");
        }

        if (!$language && $this->Cookie->check("Config.language{$component}")) {
            $language = $this->Cookie->read("Config.language{$component}");
        }

        if (!$language && Configure::check('Config.language')) {
            $language = Configure::read('Config.language');
        }

        return $language;
    }

    public function setLanguage($language = null, $component = null)
    {
        Configure::write('Config.language', $language);
        $this->Session->write("Config.language", $language);

        $this->Session->write("Config.language{$component}", $language);
        $this->Cookie->write("Config.language{$component}", $language, false, '30 days');
    }

    public function getLanguage()
    {
        return Configure::read('Config.language');
    }
}
