<?php

class ArticlesController extends AppController
{
    var $uses = 'Article';

    function index()
    {
        $conditions['article_category_id'] = ARTICLE_CATEGORY_NEWS;
        $this->paginate = array(
            'Article' => array(
                'limit' => 3,
                'order' => array('Article.id' => 'DESC')
            )
        );

        $this->set('articles', $this->paginate('Article', $conditions));
    }

    function projects()
    {
        $conditions['article_category_id'] = ARTICLE_CATEGORY_PROJECT;
        $this->paginate = array(
            'Article' => array(
                'limit' => 10,
                'order' => array('Article.id' => 'DESC')
            )
        );

        $this->set('articles', $this->paginate('Article', $conditions));
    }

    function events()
    {
        $conditions['article_category_id'] = ARTICLE_CATEGORY_EVENT;
        $this->paginate = array(
            'Article' => array(
                'limit' => 10,
                'order' => array('Article.id' => 'DESC')
            )
        );

        $this->set('articles', $this->paginate('Article', $conditions));
    }

    function view($id = null)
    {
        $this->Article->id = $id;
        if (!$this->Article->exists()) {
            throw new NotFoundException(__('Invalid article'));
        }

        $this->Article->loadRelation('belongsTo', 'ArticleCategory', false);
        $article = $this->Article->read(null, $id);
        $this->set('article', $article);

        $this->set('meta_keywords', $article['Article']['meta_keywords']);
        $this->set('meta_description', $article['Article']['meta_description']);

        //Only load relate articles for new category
        if (isset($article['ArticleCategory']['id']) && $article['ArticleCategory']['id'] != ARTICLE_CATEGORY_ABOUTUS) {
            //Load relate articles
            $items = $this->Article->find('all',
                array(
                    'fields' => array('Article.id', 'Article.title'),
                    'conditions' => array(
                        'Article.article_category_id' => $article['ArticleCategory']['id'],
                        'Article.id >=' => ($article['Article']['id'] - 1),
                        'Article.id !=' => $article['Article']['id']
                    ),
                    'order' => array('Article.id' => 'ASC'),
                    'limit' => 2,
                    'recursive' => -1,
                )
            );
            $related_articles = array();
            foreach ($items as $item) {
                if (!isset($related_articles['prev']) && $item['Article']['id'] < $article['Article']['id']) {
                    $related_articles['prev'] = $item;
                } else if (!isset($related_articles['next']) && $item['Article']['id'] > $article['Article']['id']) {
                    $related_articles['next'] = $item;
                }
            }
            $this->set('related_articles', $related_articles);
        }
    }
}

?>