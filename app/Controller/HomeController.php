<?php

class HomeController extends AppController
{
    public function index()
    {
        $this->layout = 'home';

        $this->module('Slide', 'index', 'slider');

        $this->module('Product', 'featured', 'right', array('category_id' => null, 'limit' => 20), array(
            'before' => '<div class="featured-items"><h2 class="title text-center">' . __('Featured Items') . '</h2>',
            'after' => '</div>'
        ));

        $this->module('Product', 'latest', 'right', array('category_id' => null, 'limit' => 8), array(
            'before' => '<div class="latest-items-module"><h2 class="title text-center">' . __('Latest Items') . '</h2>',
            'after' => '</div>'
        ));

        $this->module('Customer', 'index', 'customers', array(), array());
    }

    public function language($language_code = 'eng', $redirect_url = null)
    {
        if (in_array($language_code, Configure::read('AVAILABLE_LANGUAGE_CODES'))) {
            $this->setLanguage($language_code, 'FE');
            $this->Site->delete();
        }
        $referer = $this->referer();
        if (!empty($redirect_url)) {
            $referer = $this->Utility->safe_b64decode($redirect_url);
        }
        if (empty($referer)) $this->redirect('/');
        else $this->redirect($referer);
    }
}

?>