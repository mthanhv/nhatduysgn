<?php

class ContactsController extends AppController
{
    var $uses = 'Contact';

    function contactus()
    {
        $this->layout = 'contact';

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Contact->create();
            $this->request->data['Contact']['ip'] = $this->request->clientIp();
            if ($this->Contact->save($this->request->data)) {
                $this->Session->setFlash(__('Thanks! We appreciate that you’ve taken the time to write us. We’ll get back to you very soon.'));

                $data = $this->request->data['Contact'];

                $emails = $this->Site->get('site_emails');
                $emails = empty($emails) ? null : explode(',', $emails);

                App::uses('CakeEmail', 'Network/Email');
                $email = new CakeEmail('default');
                $email->to($data['email']);
                if (!empty($emails)) {
                    $email->bcc($emails);
                }
                $email->emailFormat('html');
                $email->template('submission_submitted');
                $email->viewVars(array('data' => $data));
                $email->subject(__('Re: %s', $data['title']));
                try {
                    if ($email->send()) {
                    } else {
                    }
                } catch (Exception $e) {
                }

                $this->redirect(array('controller' => 'contacts', 'action' => 'contactus'));
            }
        }
    }
}

?>