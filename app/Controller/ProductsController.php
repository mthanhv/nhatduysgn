<?php

class ProductsController extends AppController
{
    var $uses = 'Product';

    function index()
    {
        $conditions = $joins = array();
        $conditions['Product.publish'] = PUBLISHED;

        $category_id = isset($this->request->params['named']['category']) ? $this->request->params['named']['category'] : null;
        if (!empty($category_id)) {
            $this->loadModel('ProductCategory');

            //Check does product category exist ?
            $this->ProductCategory->id = $category_id;
            if (!$this->ProductCategory->exists()) {
                throw new NotFoundException(__('Invalid product category'));
            }

            //Get sub category id of current id
            $sub_categories = $this->ProductCategory->children($category_id, false, array('ProductCategory.id'));
            $sub_category_ids = array();
            if (!empty($sub_categories)) $sub_category_ids = Set::extract($sub_categories, "{n}.ProductCategory.id");
            $sub_category_ids[] = $category_id;
            $conditions['Product.product_category_id'] = $sub_category_ids;

            $this->set('product_category', $this->ProductCategory->read(null, $category_id));
        }

        $brand_id = isset($this->request->params['named']['brand']) ? $this->request->params['named']['brand'] : null;
        if (!empty($brand_id)) {
            $conditions['Product.brand_id'] = $brand_id;
        }

        $supplier_id = isset($this->request->params['named']['supplier']) ? $this->request->params['named']['supplier'] : null;
        if (!empty($supplier_id)) {
            $conditions['Product.supplier_id'] = $supplier_id;
        }

        $tag_id = isset($this->request->params['named']['tag']) ? $this->request->params['named']['tag'] : null;
        if (!empty($tag_id)) {
            $conditions['ProductTag.tag_id'] = $tag_id;
            $joins[] = [
                'alias' => 'ProductTag',
                'table' => 'products_tags',
                'type' => 'INNER',
                'conditions' => array('Product.id = ProductTag.product_id')
            ];
        }

        $key = isset($this->request->params['named']['key']) ? $this->Utility->safe_b64decode($this->request->params['named']['key']) : null;
        if (!empty($key)) {
            $key_escaped = str_replace(array('%', '_'), array('\%', '\_'), $key);
            $joins[] = [
                'alias' => 'ProductCategory',
                'table' => 'product_categories',
                'type' => 'LEFT',
                'conditions' => array('Product.product_category_id = ProductCategory.id')
            ];
            $joins[] = [
                'alias' => 'Brand',
                'table' => 'brands',
                'type' => 'LEFT',
                'conditions' => array('Product.brand_id = Brand.id')
            ];
            $joins[] = [
                'alias' => 'Supplier',
                'table' => 'suppliers',
                'type' => 'LEFT',
                'conditions' => array('Product.supplier_id = Supplier.id')
            ];
            $joins[] = [
                'alias' => 'ProductTag',
                'table' => 'products_tags',
                'type' => 'LEFT',
                'conditions' => array('Product.id = ProductTag.product_id')
            ];
            $joins[] = [
                'alias' => 'Tag',
                'table' => 'tags',
                'type' => 'LEFT',
                'conditions' => array('Tag.id = ProductTag.tag_id')
            ];

            $conditions['OR'] = array(
                'Product.title LIKE' => "%{$key_escaped}%",
                'Product.code LIKE' => "%{$key_escaped}%",
                'ProductCategory.name LIKE' => "%{$key_escaped}%",
                'Brand.title LIKE' => "%{$key_escaped}%",
                'Supplier.title LIKE' => "%{$key_escaped}%",
                'Tag.title LIKE' => "%{$key_escaped}%",
            );
        }

        $this->paginate = array(
            'Product' => array(
                'limit' => 24,
                'order' => array('Product.ordering' => 'DESC', 'Product.id' => 'DESC'),
                'fields' => array('DISTINCT Product.id', 'Product.image', 'Product.title'),
                'joins' => $joins
            )
        );

        $this->set('products', $this->paginate('Product', $conditions));
    }

    function view($id = null)
    {
        $this->Product->id = $id;
        if (!$this->Product->exists()) {
            throw new NotFoundException(__('Invalid product'));
        }

        $this->module('Product', 'recentv2', 'recent_items', ['max_items' => RECENT_VIEW_ITEMS_MAX], [
            'before' => '<div id="recent-items-module"><h2 class="title text-center">' . __('Recent Items') . '</h2>',
            'after' => '</div>'
        ]);

        $recentViewIds = $this->Session->read(RECENT_VIEW_ITEMS_KEY);
        $recentViewIds[$id] = time();
        $this->Session->write(RECENT_VIEW_ITEMS_KEY, $recentViewIds);

        $this->Product->loadRelation('belongsTo', 'Supplier', false);
        $this->Product->loadRelation('belongsTo', 'Brand', false);
        $this->Product->loadRelation('belongsTo', 'ProductCategory', false);
        $this->Product->loadRelation('hasAndBelongsToMany', 'Tag', false);

        $product = $this->Product->read(null, $id);
        if ($product['Product']['publish'] == UNPUBLISHED) {
            throw new NotFoundException(__('Invalid product'));
        }

        $this->set('product', $product);

        $this->set('title_for_layout', $product['Product']['title']);
        $this->set('meta_keywords', $product['Product']['meta_keywords']);
        $this->set('meta_description', $product['Product']['meta_description']);

        $active_tab = !empty($product['Product']['content']) ? PRODUCT_DETAIL_ACTIVE_CONTENT : PRODUCT_DETAIL_ACTIVE_SPECIFICATION;
        $this->set('active_tab', $active_tab);

        //Get sub category id of current id
        $this->loadModel('ProductCategory');
        $sub_categories = $this->ProductCategory->children($product['Product']['product_category_id'], false, array('ProductCategory.id'));
        $sub_category_ids = array();
        if (!empty($sub_categories)) $sub_category_ids = Set::extract($sub_categories, "{n}.ProductCategory.id");
        $sub_category_ids[] = $product['Product']['product_category_id'];

        //Load relate products
        $related_products = $this->Product->find('all', array(
            'limit' => 12,
            'order' => array('Product.id' => 'DESC'),
            'conditions' => array(
                'Product.id <' => $product['Product']['id'],
                'Product.product_category_id' => $sub_category_ids,
                'Product.publish' => PUBLISHED
            ),
            'fields' => array('Product.id', 'Product.image', 'Product.title')
        ));

        $this->set('related_products', $related_products);
    }

    function search()
    {
        $url = ['action' => 'index'];
        if (($this->request->is('post') || $this->request->is('put')) && !empty($this->request->data['Product']['key'])) {
            $url['key'] = $this->Utility->safe_b64encode($this->request->data['Product']['key']);
        }
        $this->redirect($url);
    }
}

?>