<?php
class GroupUser extends AppModel {
	public $useTable = 'groups_users';
	
	public $relationships = array(
		'belongsTo' => array(
			'User' => array(
				'className'  => 'User',
				'foreignKey' => 'user_id'
			)
		),
		'belongsTo' => array(
			'Group' => array(
				'className'  => 'Group',
				'foreignKey' => 'group_id'
			)
		)
	);
}
?>