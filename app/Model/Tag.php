<?php
class Tag extends AppModel {
	public $displayField = 'title';

	public $actsAs = array(
//		'Translate'	=> array('title')
	);
	
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Title is required',
				'allowEmpty' => false
			)
		)
	);
	
	public $relationships = array(
		'hasAndBelongsToMany' => array(
			'Product' => array(
				'className' => 'Product',
				'joinTable' => 'products_tags',
				'foreignKey' => 'tag_id',
				'associationForeignKey' => 'product_id'
			)
		)
	);
}
?>