<?php
class Slide extends AppModel {
	public $validate = array(
		'image' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Image is required',
				'allowEmpty' => false
			)
		)
	);
	
	public $relationships = array(
	);
}
?>