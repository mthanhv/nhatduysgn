<?php
class User extends AppModel {
	public $displayField = 'full_name';
	
	public $validate = array(
		'user_name' => array(
			'alphanumeric' => array(
				'rule' => 'alphaNumeric',  
				'message' => 'Only alphabets and numbers allowed',
				'allowEmpty' => false
			),
			'isunique' => array(
				'rule' => 'isUnique',
				'message' => 'User name was taken',
				'allowEmpty' => false,
				'on' => 'create'
			)
		),
		'password' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Password is required',
				'allowEmpty' => false,
				'on' => 'create'
			)
		),
		'full_name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Full name is required',
				'allowEmpty' => false
			)
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'Email is invalid',
				'allowEmpty' => false
			)
		)
	);
	
	public $relationships = array(
		'hasAndBelongsToMany' => array(
			'Group' => array(
				'className' => 'Group',
				'joinTable' => 'groups_users',
				'foreignKey' => 'user_id',
				'associationForeignKey' => 'group_id'
			)
		)
	);

    
    function getLoginData($login = '', $password = '') {
		$hashed = md5($password);
		
		$this->loadRelation('hasAndBelongsToMany', 'Group');
        $data = $this->find('first', array('conditions'=>array('user_name'=>$login, 'password'=>$hashed), 'recursive' => 1));
        
        if (!empty($data)) {
			$data['User']['group_ids'] = array();
			$data['User']['group_names'] = array();
            if (!empty($data['Group'])) {
                foreach($data['Group'] as $group) {
                    $data['User']['group_ids'][] = $group['id'];
                    $data['User']['group_names'][] = $group['name'];
                }
            }
            unset($data['User']['password']); // not useful to save the encrypted password in session
            return $data;
        } else {
            return false;
		}
    }
}
?>