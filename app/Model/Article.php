<?php
class Article extends AppModel {
	public $displayField = 'title';
	
	public $actsAs = array(
//		'Translate'	=> array('title', 'content', 'meta_keywords', 'meta_description')
	);
	
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Title is required',
				'allowEmpty' => false
			)
		),
		'content' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Content is required',
				'allowEmpty' => false
			)
		)
	);
	
	public $relationships = array(
		'belongsTo' => array(
			'ArticleCategory' => array(
				'className'  => 'ArticleCategory',
				'foreignKey' => 'article_category_id'
			)
		)
	);
}
?>