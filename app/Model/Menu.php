<?php
class Menu extends AppModel {
	public $displayField = 'menu';
	
	public $validate = array(
		'menu' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Menu is required',
				'allowEmpty' => false,
				'required' => true
			)
		)
	);
	
	public $relationships = array(
		'hasMany' => array(
			'MenuItem' => array(
				'className' => 'MenuItem',
				'foreignKey' => 'menu_id'
			)
		)
	);
}
?>