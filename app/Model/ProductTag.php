<?php
class ProductTag extends AppModel {
	public $useTable = 'products_tags';
	
	public $relationships = array(
		'belongsTo' => array(
			'Product' => array(
				'className'  => 'Product',
				'foreignKey' => 'product_id'
			)
		),
		'belongsTo' => array(
			'Tag' => array(
				'className'  => 'Tag',
				'foreignKey' => 'tag_id'
			)
		)
	);
}
?>