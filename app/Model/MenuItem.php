<?php
class MenuItem extends AppModel {
	public $displayField = 'title';
	
	public $actsAs = array(
//		'Translate'	=> array('title'),
		'Tree'
	);
	
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Title is required',
				'allowEmpty' => false
			)
		),
	);
	
	public $relationships = array(
		'belongsTo' => array(
			'Menu' => array(
				'className'  => 'Menu',
				'foreignKey' => 'menu_id'
			)
		)
	);
	
	function getTranslationUrl($id, $from, $to) {
		return Router::url(array('plugin' => 'admin', 'controller' => 'menu_items', 'action' => 'translate', $id, 'from' => $from, 'to' => $to));
	}
}
?>