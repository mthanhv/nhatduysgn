<?php
class Setting extends AppModel {
	public $displayField = 'site_name';
	
	public $actsAs = array(
//		'Translate'	=> array('site_name', 'site_slogan', 'site_keywords', 'site_description')
	);
	
	public $validate = array(
		'site_name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Site name is required',
				'allowEmpty' => false
			)
		),
		'site_slogan' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Site slogan is required',
				'allowEmpty' => false
			)
		),
	);
	
	public $relationships = array();
}
?>