<?php
class ProductCategory extends AppModel {
	public $displayField = 'name';

	public $actsAs = array(
		'Tree'
	);
	
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Name is required',
				'allowEmpty' => false,
				'required' => true
			)
		)
	);
	
	public $relationships = array(
		'hasMany' => array(
			'Product' => array(
				'className' => 'Product',
				'foreignKey' => 'product_category_id'
			)
		)
	);
}
?>