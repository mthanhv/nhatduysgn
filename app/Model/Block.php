<?php
class Block extends AppModel {
	public $displayField = 'title';
	
	public $actsAs = array(
		//'Translate'	=> array('title', 'content')
	);
	
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Title is required',
				'allowEmpty' => false
			)
		),
		'content' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Content is required',
				'allowEmpty' => false
			)
		)
	);
	
	public $relationships = array();
}
?>