<?php
class Group extends AppModel {
	public $displayField = 'name';
	
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Name is required',
				'allowEmpty' => false,
				'required' => true,
			)
		)
	);
	
	public $relationships = array(
		'hasMany' => array(
			'Rule' => array(
				'className' => 'Rule',
				'foreignKey' => 'group_id'
			)
		),
		'hasAndBelongsToMany' => array(
			'User' => array(
				'className' => 'User',
				'joinTable' => 'groups_users',
				'foreignKey' => 'group_id',
				'associationForeignKey'=> 'user_id'
			)
		)
	);
}
?>