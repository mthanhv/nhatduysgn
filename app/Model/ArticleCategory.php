<?php
class ArticleCategory extends AppModel {
	public $displayField = 'name';
	
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Name is required',
				'allowEmpty' => false,
				'required' => true
			)
		)
	);
	
	public $relationships = array(
		'hasMany' => array(
			'Article' => array(
				'className' => 'Article',
				'foreignKey' => 'article_category_id'
			)
		)
	);
}
?>