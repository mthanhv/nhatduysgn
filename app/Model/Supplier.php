<?php
class Supplier extends AppModel {
	public $displayField = 'title';
	
	public $actsAs = array(
//		'Translate'	=> array('title', 'content', 'meta_keywords', 'meta_description')
	);
	
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Title is required',
				'allowEmpty' => false
			)
		)
	);
	
	public $relationships = array();
}
?>