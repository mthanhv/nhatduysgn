<?php
class Rule extends AppModel {
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Name is required',
				'allowEmpty' => false
			)
		),
		'action' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Action is required',
				'allowEmpty' => false
			)
		)
	);
	
	public $relationships = array(
		'belongsTo' => array(
			'Group' => array(
				'className'  => 'Group',
				'foreignKey' => 'group_id'
			)
		)
	);
}
?>