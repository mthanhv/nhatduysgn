<?php
class Product extends AppModel {
	public $displayField = 'title';

	public $actsAs = array(
//		'Translate'	=> array('title', 'content', 'meta_keywords', 'meta_description')
	);
	
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Title is required',
				'allowEmpty' => false
			)
		),
//		'content' => array(
//			'notempty' => array(
//				'rule' => array('notempty'),
//				'message' => 'Content is required',
//				'allowEmpty' => false
//			)
//		),
		'code' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Code is required',
				'allowEmpty' => false
			)
		)
	);
	
	public $relationships = array(
		'belongsTo' => array(
			'ProductCategory' => array(
				'className'  => 'ProductCategory',
				'foreignKey' => 'product_category_id'
			),
			'Supplier' => array(
				'className'  => 'Supplier',
				'foreignKey' => 'supplier_id'
			),
			'Brand' => array(
				'className'  => 'Brand',
				'foreignKey' => 'brand_id'
			)
		),
		'hasAndBelongsToMany' => array(
			'Tag' => array(
				'className' => 'Tag',
				'joinTable' => 'products_tags',
				'foreignKey' => 'product_id',
				'associationForeignKey' => 'tag_id'
			)
		)
	);
}
?>