<?php
class Contact extends AppModel {
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter name',
				'allowEmpty' => false
			)
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'Email address is not valid',
				'allowEmpty' => false
			)
		),
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter title',
				'allowEmpty' => false
			)
		),
		'content' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter your question or comment',
				'allowEmpty' => false
			)
		)
	);
	
	public $relationships = array();
}
?>