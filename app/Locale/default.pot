# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2013-12-14 02:49+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

msgid "Publish"
msgstr ""

msgid "UnPublish"
msgstr ""

msgid "English"
msgstr ""

msgid "Vietnamese"
msgstr ""

msgid "<span class=\"first-char\">We</span><span class=\"left-chars\"> provides</span>"
msgstr ""

msgid "<span class=\"first-char\">Best</span><span class=\"left-chars\"> products</span>"
msgstr ""

msgid "Invalid article category"
msgstr ""

msgid "Invalid article"
msgstr ""

msgid "The submission has been saved"
msgstr ""

msgid "The submission could not be saved. Please, try again"
msgstr ""

msgid "<span class=\"first-char\">About</span><span class=\"left-chars\"> us</span>"
msgstr ""

msgid "<span class=\"first-char\">Our</span><span class=\"left-chars\"> products</span>"
msgstr ""

msgid "Invalid product category"
msgstr ""

msgid "Invalid product"
msgstr ""

msgid "Your session expired"
msgstr ""

msgid "You have to log in to access website"
msgstr ""

msgid "Guest"
msgstr ""

msgid "The file name already exists"
msgstr ""

msgid "The thumbnail name already exists"
msgstr ""

msgid "Create thumbnail fail"
msgstr ""

msgid "Move uploaded file fail"
msgstr ""

msgid "File uploaded fail"
msgstr ""

msgid "Select parent menu"
msgstr ""

msgid "The article has been saved"
msgstr ""

msgid "The article could not be saved. Please, try again"
msgstr ""

msgid "Article could not be saved . Please try again"
msgstr ""

msgid "Article was deleted"
msgstr ""

msgid "Article could not be deleted. Please try again"
msgstr ""

msgid "Articles were deleted"
msgstr ""

msgid "The block has been saved"
msgstr ""

msgid "The block could not be saved. Please, try again"
msgstr ""

msgid "Invalid block"
msgstr ""

msgid "Block could not be saved . Please try again"
msgstr ""

msgid "Block was deleted"
msgstr ""

msgid "Block could not be deleted. Please try again"
msgstr ""

msgid "Blocks were deleted"
msgstr ""

msgid "Invalid contact"
msgstr ""

msgid "Contact was deleted"
msgstr ""

msgid "Contact could not be deleted. Please try again"
msgstr ""

msgid "Contacts were deleted"
msgstr ""

msgid "The menu_item has been saved"
msgstr ""

msgid "The menu_item could not be saved. Please, try again"
msgstr ""

msgid "Select menu"
msgstr ""

msgid "Invalid menu item"
msgstr ""

msgid "The menu item has been saved"
msgstr ""

msgid "The menu item could not be saved. Please, try again"
msgstr ""

msgid "MenuItem was deleted"
msgstr ""

msgid "MenuItem could not be deleted. Please try again"
msgstr ""

msgid "MenuItems were deleted"
msgstr ""

msgid "Menu item was moved up."
msgstr ""

msgid "Menu item was moved up unsuccessfully."
msgstr ""

msgid "Menu item was moved down."
msgstr ""

msgid "Menu item was moved down unsuccessfully."
msgstr ""

msgid "The product category has been saved"
msgstr ""

msgid "The product category could not be saved. Please, try again"
msgstr ""

msgid "Select parent product category"
msgstr ""

msgid "ProductCategory was deleted"
msgstr ""

msgid "ProductCategory could not be deleted. Please try again"
msgstr ""

msgid "ProductCategories were deleted"
msgstr ""

msgid "Product category was moved up."
msgstr ""

msgid "Product category was moved up unsuccessfully."
msgstr ""

msgid "Product category was moved down."
msgstr ""

msgid "Product category was moved down unsuccessfully."
msgstr ""

msgid "The product has been saved"
msgstr ""

msgid "Product could not be saved . Please try again"
msgstr ""

msgid "Product was deleted"
msgstr ""

msgid "Product could not be deleted. Please try again"
msgstr ""

msgid "Products were deleted"
msgstr ""

msgid "Copy of %s"
msgstr ""

msgid "Products were cloned"
msgstr ""

msgid "Products were not cloned successfully"
msgstr ""

msgid "The settings has been saved"
msgstr ""

msgid "The setting could not be saved. Please, try again"
msgstr ""

msgid "The slide has been saved"
msgstr ""

msgid "The slide could not be saved. Please, try again"
msgstr ""

msgid "Invalid slide"
msgstr ""

msgid "Slide could not be saved . Please try again"
msgstr ""

msgid "Slide was deleted"
msgstr ""

msgid "Slide could not be deleted. Please try again"
msgstr ""

msgid "Slides were deleted"
msgstr ""

msgid "Article"
msgstr ""

msgid "MenuItem"
msgstr ""

msgid "Product Category"
msgstr ""

msgid "Product"
msgstr ""

msgid "Block"
msgstr ""

msgid "Setting"
msgstr ""

msgid "Invalid data"
msgstr ""

msgid "You are already logged in"
msgstr ""

msgid "Invalid user name or password"
msgstr ""

msgid "Your account is disabled"
msgstr ""

msgid "Request changing password"
msgstr ""

msgid "Your request was accepted. Please check your email for more instruction"
msgstr ""

msgid "Can't send email. Please try again"
msgstr ""

msgid "Can't save your request. Please try again"
msgstr ""

msgid "Account with your email doesn't exist. Please try again"
msgstr ""

msgid "Email is invalid. Please try again"
msgstr ""

msgid "Your code is invalid"
msgstr ""

msgid "The passwords do not match"
msgstr ""

msgid "Change password successfully. You may log in now"
msgstr ""

msgid "Can not change your password. Please try again"
msgstr ""

msgid "The user has been saved"
msgstr ""

msgid "The user could not be saved. Please, try again"
msgstr ""

msgid "Invalid user"
msgstr ""

msgid "Only the admin can change its profile!"
msgstr ""

msgid "You cannot delete a user in administrators group"
msgstr ""

msgid "User was deleted"
msgstr ""

msgid "User was not deleted"
msgstr ""

msgid "Users was deleted"
msgstr ""

msgid "Top 10 lastest contacts"
msgstr ""

msgid "#"
msgstr ""

msgid "Name"
msgstr ""

msgid "Title"
msgstr ""

msgid "Email"
msgstr ""

msgid "Created"
msgstr ""

msgid "View"
msgstr ""

msgid "Top 10 lastest products"
msgstr ""

msgid "Thumb"
msgstr ""

msgid "Category"
msgstr ""

msgid "Articles"
msgstr ""

msgid "Back"
msgstr ""

msgid "Choose category"
msgstr ""

msgid "Create"
msgstr ""

msgid "Create & New"
msgstr ""

msgid "Update"
msgstr ""

msgid "Please select article"
msgstr ""

msgid "Are you sure you want to delete the selected articles ?"
msgstr ""

msgid "Add new article"
msgstr ""

msgid "Delete selected articles"
msgstr ""

msgid "E.g. Article title"
msgstr ""

msgid "Search"
msgstr ""

msgid "Actions"
msgstr ""

msgid "Edit"
msgstr ""

msgid "Delete"
msgstr ""

msgid "Are you sure you want to delete article '%s' ?"
msgstr ""

msgid "Total: <b>{:count}</b> %s"
msgstr ""

msgid "Created: %s"
msgstr ""

msgid "Blocks"
msgstr ""

msgid "Please select block"
msgstr ""

msgid "Are you sure you want to delete the selected blocks ?"
msgstr ""

msgid "Add new block"
msgstr ""

msgid "Delete selected blocks"
msgstr ""

msgid "Are you sure you want to delete block '%s' ?"
msgstr ""

msgid "Please select contact"
msgstr ""

msgid "Are you sure you want to delete the selected contacts ?"
msgstr ""

msgid "Contacts"
msgstr ""

msgid "Delete selected contacts"
msgstr ""

msgid "E.g. Contact name, Contact title"
msgstr ""

msgid "Are you sure you want to delete contact '%s' ?"
msgstr ""

msgid "Contact"
msgstr ""

msgid "Name: %s"
msgstr ""

msgid "Email: %s"
msgstr ""

msgid "Phone: %s"
msgstr ""

msgid "Ip: %s"
msgstr ""

msgid "Title: %s"
msgstr ""

msgid "Main Menu"
msgstr ""

msgid "Dashboard"
msgstr ""

msgid "Product Categories"
msgstr ""

msgid "Products"
msgstr ""

msgid "Users"
msgstr ""

msgid "Slides"
msgstr ""

msgid "Menu Items"
msgstr ""

msgid "Settings"
msgstr ""

msgid "Translation"
msgstr ""

msgid "Click here to change password"
msgstr ""

msgid "Change password code: %s"
msgstr ""

msgid "Welcome: %s, %s"
msgstr ""

msgid "logout"
msgstr ""

msgid "Logout"
msgstr ""

msgid "Are you sure you want to logout ?"
msgstr ""

msgid "MenuItems"
msgstr ""

msgid "Choose menu"
msgstr ""

msgid "Choose parent menu item"
msgstr ""

msgid "Please select menu_item"
msgstr ""

msgid "Are you sure you want to delete the selected menu_items ?"
msgstr ""

msgid "Add new menu item"
msgstr ""

msgid "Delete selected menu items"
msgstr ""

msgid "Menu"
msgstr ""

msgid "Move up"
msgstr ""

msgid "Move down"
msgstr ""

msgid "Are you sure you want to delete menu item '%s' ?"
msgstr ""

msgid "Product categories"
msgstr ""

msgid "Choose parent"
msgstr ""

msgid "Choose parent product category"
msgstr ""

msgid "Please select product category"
msgstr ""

msgid "Are you sure you want to delete the selected product categories ?"
msgstr ""

msgid "Add new product category"
msgstr ""

msgid "Delete selected product categories"
msgstr ""

msgid "Are you sure you want to delete product category '%s' ?"
msgstr ""

msgid "ProductCategory"
msgstr ""

msgid "Album"
msgstr ""

msgid "Is best ?"
msgstr ""

msgid "Please select product"
msgstr ""

msgid "Are you sure you want to delete the selected products ?"
msgstr ""

msgid "Add new product"
msgstr ""

msgid "Clone selected products"
msgstr ""

msgid "Delete selected products"
msgstr ""

msgid "E.g. Product title"
msgstr ""

msgid "Are you sure you want to delete product '%s' ?"
msgstr ""

msgid "Yes"
msgstr ""

msgid "No"
msgstr ""

msgid "Save"
msgstr ""

msgid "Please select slide"
msgstr ""

msgid "Are you sure you want to delete the selected slides ?"
msgstr ""

msgid "Add new slide"
msgstr ""

msgid "Delete selected slides"
msgstr ""

msgid "Are you sure you want to delete slide '%s' ?"
msgstr ""

msgid "Slide"
msgstr ""

msgid "Send"
msgstr ""

msgid "Id"
msgstr ""

msgid "Translate"
msgstr ""

msgid "Password"
msgstr ""

msgid "Retype your password"
msgstr ""

msgid "Submit"
msgstr ""

msgid "Login ?"
msgstr ""

msgid "Login"
msgstr ""

msgid "User name"
msgstr ""

msgid "Forgot password ?"
msgstr ""

msgid "Choose group"
msgstr ""

msgid "Password (leave blank to keep it)"
msgstr ""

msgid "Please select user"
msgstr ""

msgid "Are you sure you want to delete the selected users ?"
msgstr ""

msgid "Add new user"
msgstr ""

msgid "Delete selected users"
msgstr ""

msgid "E.g. Full name, User name, Email"
msgstr ""

msgid "Group"
msgstr ""

msgid "Send email to this user"
msgstr ""

msgid "Filter users by this group"
msgstr ""

msgid "Are you sure you want to delete user '%s' ?"
msgstr ""

msgid "User"
msgstr ""

msgid "All articles"
msgstr ""

msgid "Related articles"
msgstr ""

msgid "Phone"
msgstr ""

msgid "Content"
msgstr ""

msgid "Cancel"
msgstr ""

msgid "WE'D LOVE TO HEAR FROM YOU!"
msgstr ""

msgid "Switch to english"
msgstr ""

msgid "Switch to vietnamese"
msgstr ""

msgid "<span class=\"first-char\">Need</span><span class=\"left-chars\"> our help ?</span>"
msgstr ""

msgid "Product name or product code"
msgstr ""

msgid "Type search text here... ?"
msgstr ""

msgid "Read more"
msgstr ""

msgid "All products"
msgstr ""

msgid "<span class=\"first-char\">Search</span><span class=\"left-chars\"> advance</span>"
msgstr ""

msgid "Search by:"
msgstr ""

msgid "Product name"
msgstr ""

msgid "Product code"
msgstr ""

msgid "Product type"
msgstr ""

msgid "Category name"
msgstr ""

msgid "Search Results"
msgstr ""

msgid "Model Number:"
msgstr ""

msgid "Type:"
msgstr ""

msgid "Category:"
msgstr ""

msgid "There is no result"
msgstr ""

msgid "Not available"
msgstr ""

msgid "FOB:"
msgstr ""

msgid "Port:"
msgstr ""

msgid "Moq:"
msgstr ""

msgid "Supply ability:"
msgstr ""

msgid "Payment term:"
msgstr ""

msgid "Material:"
msgstr ""

msgid "Size:"
msgstr ""

msgid "Durable:"
msgstr ""

msgid "Color:"
msgstr ""

msgid "Place of origin:"
msgstr ""

msgid "Contact us:"
msgstr ""

msgid "Related products"
msgstr ""

msgid "Title is required"
msgstr ""

msgid "Content is required"
msgstr ""

msgid "Name is required"
msgstr ""

msgid "Email is invalid"
msgstr ""

msgid "Menu is required"
msgstr ""

msgid "Link is required"
msgstr ""

msgid "Image is required"
msgstr ""

msgid "Action is required"
msgstr ""

msgid "Site name is required"
msgstr ""

msgid "Site slogan is required"
msgstr ""

msgid "Only alphabets and numbers allowed"
msgstr ""

msgid "User name was taken"
msgstr ""

msgid "Password is required"
msgstr ""

msgid "Full name is required"
msgstr ""