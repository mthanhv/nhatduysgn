<div class="search_box pull-right">
    <?php $key = isset($this->request->params['named']['key']) ? $this->Utility->safe_b64decode($this->request->params['named']['key']) : null;?>
    <?php echo $this->Form->create('Product', ['url'=>['plugin'=>false, 'controller'=>'products', 'action'=>'search']]);?>
    <?php echo $this->Form->input('key', ['label'=>false, 'placeholder'=>__('Keyword'), 'value'=>$key]);?>
    <?php echo $this->Form->end();?>
</div>