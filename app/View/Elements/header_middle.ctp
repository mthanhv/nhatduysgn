<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <div class="logo pull-left">
                <?php echo $this->Html->link($this->Html->image('/images/home/logo.png'), ['plugin' => false, 'controller' => 'home', 'action' => 'index'], ['escape' => false]);?>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="shop-menu pull-right">
                <h2 class="title company-info text-center"><?php echo __('Công Ty TNHH Kỹ Thuật Nhật Duy');?></h2>
                <address>Chuyên cung cấp giải pháp <strong>MRO</strong> (<strong>M</strong>aintenance, <strong>R</strong>epair, and <strong>O</strong>perations) cho các nghành công nghiệp như: Dầu khí, Nhà máy Điện, Nhà máy Đạm, Nhà máy Hóa Chất, Nhà máy Thực Phẩm và các nghành Công nghiệp khác</address>
            </div>
        </div>
    </div>
</div>