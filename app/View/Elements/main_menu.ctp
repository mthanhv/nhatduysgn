<ul class="nav navbar-nav collapse navbar-collapse">
    <li><?php echo $this->Html->link(__('HOME'), ['plugin' => false, 'controller' => 'home', 'action' => 'index']);?></li>
    <li><?php echo $this->Html->link(__('ABOUT US'), ['plugin' => false, 'controller' => 'articles', 'action' => 'view', 1]);?></li>
	<!-- <li><?php echo $this->Html->link(__('SERVICE'), ['plugin' => false, 'controller' => 'articles', 'action' => 'view', 64]);?></li> -->
    <!-- <li><?php echo $this->Html->link(__('PROJECTS'), ['plugin' => false, 'controller' => 'articles', 'action' => 'projects']);?></li> -->
    <li><?php echo $this->Html->link(__('PRODUCTS'), ['plugin' => false, 'controller' => 'products', 'action' => 'index']);?></li>
    <!-- <li><?php echo $this->Html->link(__('EVENTS'), ['plugin' => false, 'controller' => 'articles', 'action' => 'events']);?></li> -->
    <!-- <li><?php echo $this->Html->link(__('CERTIFICATIONS'), ['plugin' => false, 'controller' => 'articles', 'action' => 'view', 100]);?></li> -->
    <li><?php echo $this->Html->link(__('NEWS'), ['plugin' => false, 'controller' => 'articles', 'action' => 'index']);?></li>
    <li><?php echo $this->Html->link(__('CONTACT US'), ['plugin' => false, 'controller' => 'contacts', 'action' => 'contactus']);?></li>
</ul>