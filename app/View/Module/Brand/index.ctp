<div class="brands-name">
    <ul class="nav nav-pills nav-stacked">
        <?php foreach ($brands as $brand) { ?>
            <li>
                <?php
                    echo $this->Html->link(
                        sprintf('<span class="pull-right">(%s)</span>%s</a>', $brand[0]['count'], $brand['Brand']['title']),
                        [
                            'plugin' => false,
                            'controller' => 'products',
                            'action' => 'index',
                            'brand' => $brand['Brand']['id'],
                            'title' => $this->Utility->seo_string($brand['Brand']['title'])
                        ],
                        [
                            'escape' => false,
                            'title' => $brand['Brand']['title']
                        ]
                    );
                ?>
            </li>
        <?php } ?>
    </ul>
</div>