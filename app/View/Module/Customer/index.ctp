<div class="image-gallery">
    <div class="image-track">
        <?php
            foreach ($customers as $customer) {
                echo '<div class="img">' . $this->Html->image('/files/customers/thumbs/'.$customer['Customer']['image'], ['title' => $customer['Customer']['title']]) . '</div>';
            }  
        ?>
    </div>
</div>

<style>
    .image-gallery {
        overflow: hidden;
        white-space: nowrap;
        position: relative;
        margin: 0px 15px 30px 15px;
    }

    .image-track {
        display: inline-block;
        white-space: nowrap;
        -webkit-animation: scroll 30s linear infinite;
        -moz-animation: scroll 30s linear infinite;
        -o-animation: scroll 30s linear infinite;
        animation: scroll 30s linear infinite;
    }

    .image-track .img {
        display: inline-block;
    }

    .image-track img {
        width: auto; /* Adjust the width as needed */
        height: 57px;
        display: inline-block;
        border: 1px solid #CCCCC6;
        margin: 0 5px;
    }

    @-webkit-keyframes scroll {
        0% {
            -webkit-transform: translateX(0);
        }
        100% {
            -webkit-transform: translateX(-100%);
        }
    }

    @-moz-keyframes scroll {
        0% {
            -moz-transform: translateX(0);
        }
        100% {
            -moz-transform: translateX(-100%);
        }
    }

    @-o-keyframes scroll {
        0% {
            -o-transform: translateX(0);
        }
        100% {
            -o-transform: translateX(-100%);
        }
    }

    @keyframes scroll {
        0% {
            transform: translateX(0);
        }
        100% {
            transform: translateX(-100%);
        }
    }
</style>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function() {
        const imageTrack = document.querySelector(".image-track");
        const imageDivs = imageTrack.querySelectorAll(".img");

        // Calculate the total width of the image track
        let totalWidth = 0;
        imageDivs.forEach(img => {
            console.log(img.clientWidth);
            totalWidth += img.clientWidth;
        });

        // Set the total width as the width of the image track
        imageTrack.style.width = `${totalWidth}px`;

        // Adjust the animation duration based on the total width
        const animationDuration = totalWidth / 50; // Adjust the divisor to control speed
        imageTrack.style.animationDuration = `${animationDuration}s`;

        imageTrack.addEventListener('mouseover', () => {
            imageTrack.style.animationPlayState = 'paused';
        });

        imageTrack.addEventListener('mouseout', () => {
            imageTrack.style.animationPlayState = 'running';
        });
    });
</script>