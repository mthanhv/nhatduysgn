<div id="languages">
    <div id="languagescontent">
        <?php $current_url = $this->Utility->safe_b64encode(Router::url($this->here, true)); ?>
        <?php foreach ($languages as $code => $name) { ?>
            <span>
                <a href="/home/language/<?php echo $code;?>/<?php echo $current_url;?>"><img src="/media/flags/<?php echo $code;?>.png" alt="<?php echo $name;?>" title="<?php echo $name;?>"></a>
            </span>
        <?php } ?>
    </div>
</div>