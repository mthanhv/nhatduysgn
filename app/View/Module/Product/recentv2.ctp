<!--recent_items-->
<?php if (!empty($products)) : ?>
<div id="recent-item" class="carousel slide">
    <div id="recent-item-bounder">
        <div id="recent-item-container" class="carousel-inner">
            <?php foreach($products as $index => $item) { ?>
                <div class="recent-product">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <?php echo $this->Html->link($this->Html->image("/files/products/thumbs/{$item['Product']['image']}"), array('controller'=>'products', 'action'=>'view', 'id'=>$item['Product']['id'], 'product'=>$this->Utility->seo_string($item['Product']['title']), 'ext'=>'html'), array('escape'=>false, 'class'=>'thumb', 'title'=>h($item['Product']['title'])));?>
                                <p class="product-title"><?php echo h($item['Product']['title']);?></p>
                                <?php echo $this->Html->link('<i class="fa fa-plus-square"></i>' . __('View detail'), array('controller'=>'products', 'action'=>'view', 'id'=>$item['Product']['id'], 'product'=>$this->Utility->seo_string($item['Product']['title']), 'ext'=>'html'), array('escape'=>false, 'class'=>'btn btn-default add-to-cart', 'title'=>h($item['Product']['title'])));?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <a id="recent-item-prev" class="left recent-item-control" href="javascript:void(0)">
        <i class="fa fa-angle-left"></i>
    </a>
    <a id="recent-item-next" class="right recent-item-control" href="javascript:void(0)">
        <i class="fa fa-angle-right"></i>
    </a>
</div>
<?php endif; ?>
<script type="text/javascript">
    var NUMBER_OF_RECENT_ITEMS = <?php echo count($products);?>;
</script>
<!--/recent_items-->