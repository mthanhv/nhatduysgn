<?php if (!empty($products)) : ?>
    <div id="marqueecontainer" onMouseover="copyspeed=pausespeed" onMouseout="copyspeed=marqueespeed" style="position: relative; overflow: hidden;">
        <ul id="vmarquee" class="products" style="position: absolute;">
        <?php foreach($products as $product) { ?>
            <li class="product-item">
                <?php echo $this->Html->link($this->Html->image("/files/products/thumbs/{$product['Product']['image']}"), array('controller'=>'products', 'action'=>'view', 'id'=>$product['Product']['id'], 'product'=>$this->Utility->seo_string($product['Product']['title']), 'ext'=>'html'), array('escape'=>false, 'title'=>h($product['Product']['title'])));?>
            </li>
        <?php } ?>
        </ul>
    </div>
    <script type="text/javascript">
        var delayb4scroll =	1000;
        var marqueespeed = 1;
        var pauseit = 1;
        var copyspeed = marqueespeed;
        var pausespeed = (pauseit == 0) ? copyspeed : 0;
        var actualheight = '';

        function scrollmarquee() {
            if (parseInt(cross_marquee.style.top) > (actualheight * (-1) + 8))
                cross_marquee.style.top = parseInt(cross_marquee.style.top) - copyspeed + "px";
            else
                cross_marquee.style.top = parseInt(marqueeheight) + 8 + "px";
        }

        function initializemarquee() {
            cross_marquee = document.getElementById("vmarquee");
            cross_marquee.style.top = 0;
            marqueeheight = document.getElementById("marqueecontainer").offsetHeight;
            actualheight = cross_marquee.offsetHeight;
            //if Opera or Netscape 7x, add scrollbars to scroll and exit
            if (window.opera || navigator.userAgent.indexOf("Netscape/7") != -1) {
                cross_marquee.style.height = marqueeheight + "px";
                cross_marquee.style.overflow = "scroll";
                return;
            }
            setTimeout('lefttime = setInterval("scrollmarquee()", 30)', delayb4scroll);
        }

        if (window.addEventListener) window.addEventListener("load", initializemarquee, false);
        else if (window.attachEvent) window.attachEvent("onload", initializemarquee);
        else if (document.getElementById) window.onload = initializemarquee;
    </script>
<?php else : ?>
    <p class="no-recent-item"><?php echo __('There is no item');?></p>
<?php endif ; ?>