<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <?php foreach(array_chunk($products, 4) as $index => $chunk) { ?>
            <div class="item<?php echo ($index == 0) ? ' active' : '';?>">
            <?php foreach($chunk as $index => $product) { ?>
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <?php echo $this->Html->link($this->Html->image("/files/products/thumbs/{$product['Product']['image']}"), array('controller'=>'products', 'action'=>'view', 'id'=>$product['Product']['id'], 'product'=>$this->Utility->seo_string($product['Product']['title']), 'ext'=>'html'), array('escape'=>false, 'class'=>'thumb', 'title'=>h($product['Product']['title'])));?>
                                <p class="product-title"><?php echo h($product['Product']['title']);?></p>
                                <?php echo $this->Html->link('<i class="fa fa-plus-square"></i>' . __('View detail'), array('controller'=>'products', 'action'=>'view', 'id'=>$product['Product']['id'], 'product'=>$this->Utility->seo_string($product['Product']['title']), 'ext'=>'html'), array('escape'=>false, 'class'=>'btn btn-default add-to-cart', 'title'=>h($product['Product']['title'])));?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            </div>
        <?php } ?>
    </div>
    <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
        <i class="fa fa-angle-left"></i>
    </a>
    <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
        <i class="fa fa-angle-right"></i>
    </a>
</div>