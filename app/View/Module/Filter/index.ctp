<div id="facets">
    <?php
        echo $this->Form->create('Product', ['id'=>'filter', 'novalidate']);
        foreach ($facets as $attr => $facet) {
            if (isset($facet['options'])) {
                $options = [];
                foreach ($facet['options'] as $option => $count) {
                    $options[$option] = sprintf('%s (%d)', $option, $count);
                }
                echo $this->Form->input($attr, ['label'=>$facet['name'], 'type'=>$facet['type'], 'options'=>$options, 'multiple' => true]);
            } else {
                echo $this->Form->input($attr, ['label'=>$facet['name'], 'type'=>$facet['type']]);
            }
        }
        echo $this->Form->submit(__('Apply'), ['id' => 'btn-apply', 'class'=>'submit-btn']);
        echo $this->Form->end();
    ?>
</div>