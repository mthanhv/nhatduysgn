<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<div id="slider-carousel" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<?php foreach ($slides as $index => $slide) : ?>
					<li data-target="#slider-carousel" data-slide-to="<?php echo $index;?>" class="<?php echo ($index == 0) ? 'active' : '';?>"></li>
					<?php endforeach; ?>
				</ol>

				<div class="carousel-inner">
					<?php foreach ($slides as $index => $slide) : ?>
					<div class="item <?php echo ($index == 0) ? 'active' : '';?>">
						<?php echo $this->Html->link($this->Html->image('/files/slides/'.$slide['Slide']['image']), $slide['Slide']['link'], array('escape'=>false));?>
					</div>
					<?php endforeach; ?>
				</div>

				<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
					<i class="fa fa-angle-left"></i>
				</a>
				<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
					<i class="fa fa-angle-right"></i>
				</a>
			</div>

		</div>
	</div>
</div>