<?php
if (!function_exists('render_categories')) {
    function render_categories($categories, $html_helper, $utility_helper, $selected_category_id = 0) {
        $count = empty($categories) ? 0 : count($categories);
        $i = 1;

        $data = '<div class="panel-group category-products" id="accordian">';
        foreach ($categories as $category) {
            $id = $category['ProductCategory']['id'];
            $title = $category['ProductCategory']['name'];
            $has_children = empty($category['children']) ? '' : ' parent';
            if ($has_children) {
                $title = '<span class="badge pull-right"><i class="fa fa-plus"></i></span>' . $title;

                $a_tag = $html_helper->link(
                    $title,
                    'javascript:void(0)',
                    array(
                        'data-toggle' => 'collapse',
                        'data-parent' => '#accordian',
                        'data-target' => '#c'.$id,
                        'title' => $category['ProductCategory']['name'],
                        'escape' => false
                    )
                );
            } else {
                $a_tag = $html_helper->link(
                    $title,
                    array(
                        'plugin' => false,
                        'controller' => 'products',
                        'action' => 'index',
                        'category' => $id,
                        'title' => $utility_helper->seo_string($title)
                    ),
                    array(
                        'data-toggle' => 'collapse',
                        'data-parent' => '#accordian',
                        'data-target' => '#c'.$id,
                        'title' => $category['ProductCategory']['name'],
                        'escape' => false
                    )
                );
            }

            $first_child = ($i === 1) ? ' first-child' : '';
            $last_child = ($i === $count) ? ' last-child' : '';
            $active = ($id == $selected_category_id) ? ' active' : '';

            if (!empty($a_tag)) {
                $data .= '<div class="panel panel-default"><div class="panel-heading"><h4 class="panel-title">';
                $data .= $a_tag;
                $data .= '</h4></div>';

                if (!empty($category['children'])) {
                    $data .= '<div id="' . 'c'.$id . '" class="panel-collapse collapse"><div class="panel-body"><ul>';
                    foreach ($category['children'] as $child) {
                        $data .= '<li>' . $html_helper->link(
                                $child['ProductCategory']['name'],
                                array(
                                    'plugin' => false,
                                    'controller' => 'products',
                                    'action' => 'index',
                                    'category' => $child['ProductCategory']['id'],
                                    'title' => $utility_helper->seo_string($child['ProductCategory']['name'])
                                ),
                                array(
                                    'title' => $child['ProductCategory']['name'],
                                )
                            ) . '</li>';
                    }
                    $data .= '</ul></div></div>';
                }
                $data .= '</div>';
            }

            $i ++;
        }
        $data .= '</div>';
        return $data;
    }
}
?>
<?php
    echo render_categories($categories, $this->Html, $this->Utility, $this->Session->read('Category.selected'));
?>