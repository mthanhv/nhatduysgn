<div id="featured-categories">
    <ul>
        <?php foreach ($categories as $index => $category) { ?>
            <?php $class = ($index == 0) ? ' first-item' : (($index == (count($categories) - 1)) ? ' last-item' : ''); ?>
            <li class="category<?php echo (($index % 2) ? ' even' : ' odd') . $class;?>">
                <?php echo $this->Html->link(
                    $category['ProductCategory']['name'],
                    array('plugin'=>false, 'controller'=>'products', 'action'=>'index', 'cat'=>$category['ProductCategory']['id']),
                    array('class'=>'title', 'title'=>h($category['ProductCategory']['name']))
                );?>
                <?php echo $this->Html->link(
                    $this->Html->image("/files/categories/thumbs/{$category['ProductCategory']['image']}"),
                    array('controller'=>'products', 'action'=>'index', 'cat'=>$category['ProductCategory']['id']),
                    array('escape'=>false, 'class'=>'thumb', 'title'=>h($category['ProductCategory']['name']))
                );?>
                <?php echo $this->Html->link(
                    '&#10097;&#10097;',
                    array('plugin'=>false, 'controller'=>'products', 'action'=>'index', 'cat'=>$category['ProductCategory']['id']),
                    array('escape'=>false, 'class'=>'read-more', 'title'=>__('Read more'))
                );?>
                <div class="clear"></div>
            </li>
        <?php } ?>
    </ul>
    <div class="clear"></div>
</div>