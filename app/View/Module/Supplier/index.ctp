<div class="col-sm-10">
    <div id="supplier-slide" class="carousel">
        <div id="slide-bounder">
            <div id="slide-container">
            <?php foreach ($suppliers as $supplier) { ?>
                <div class="video-gallery text-center">
                    <div class="video-gallery-content">
                        <?php
                            echo $this->Html->link(
                                sprintf(
                                    '<div class="iframe-img">%s</div>',
                                    $this->Html->image('/files/suppliers/thumbs/'.$supplier['Supplier']['image'], ['title' => $supplier['Supplier']['title']])
                                ),
                                [
                                    'plugin' => false,
                                    'controller' => 'products',
                                    'action' => 'index',
                                    'supplier' => $supplier['Supplier']['id'],
                                    'title' => $this->Utility->seo_string($supplier['Supplier']['title']),
                                ],
                                [
                                    'escape' => false,
                                    'title' => $supplier['Supplier']['title']
                                ]
                            );
                        ?>
                        <p><?php echo $supplier['Supplier']['title'];?></p>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div>
        <a id="prevctl" class="left recommended-item-control" href="javascript:void(0)">
            <i class="fa fa-angle-left"></i>
        </a>
        <a id="nextctl" class="right recommended-item-control" href="javascript:void(0)">
            <i class="fa fa-angle-right"></i>
        </a>
    </div>
</div>
<script type="text/javascript">
    var NUMBER_OF_SUPPLIER = <?php echo count($suppliers);?>;
</script>