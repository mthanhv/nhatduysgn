<?php
	if (!function_exists('render_tree_menu')) {
		function render_tree_menu($menu_items, $html_helper, $selected_menu_id = 0) {
			$count = empty($menu_items) ? 0 : count($menu_items);
			$i = 1;
			
			$data = '<ul class="menu sf-menu">';
			foreach ($menu_items as $menu_item) {
				$id = $menu_item['MenuItem']['id'];
				$title = $menu_item['MenuItem']['title'];
				$link = $menu_item['MenuItem']['link'];
				$has_children = empty($menu_item['children']) ? '' : ' parent';
				$a_tag = $html_helper->link('<span>'.$title.'</span>', $link, array('title' => $title, 'escape' => false));
                $first_child = ($i === 1) ? ' first-child' : '';
				$last_child = ($i === $count) ? ' last-child' : '';
				$active = ($id == $selected_menu_id) ? ' active' : '';
				
				if (!empty($a_tag)) {
					$data .= "<li id='menu-{$id}' class='menu-item item{$id}{$has_children}{$first_child}{$last_child}{$active}'>";
					$data .= $a_tag;
					if (!empty($menu_item['children'])) {
						$data .= render_tree_menu($menu_item['children'], $html_helper, $selected_menu_id);
					}
					$data .= '</li>';
				}
				
				$i ++;
			}
			$data .= '</ul>';
			return $data;
		}
	}
?>
<?php
	echo render_tree_menu($menu_items, $this->Html, $this->Session->read('Menu.selected'));
?>