<div id="recent-items">
    <ul>
        <?php if (!empty($articles)) : ?>
            <?php foreach($articles as $index => $article) { ?>
                <?php $class = ($index == 0) ? ' first-item' : (($index == (count($articles) - 1)) ? ' last-item' : ''); ?>
                <li class="recent-item<?php echo (($index % 2) ? ' even' : ' odd') . $class;?>">
                    <div class="col-left">
                        <div class="creation">
                            <?php
                                $timeStamp = strtotime($article['Article']['created']);
                                echo sprintf('<span class="date">%s</span><span class="month">%s</span>', date('d', $timeStamp), date('m', $timeStamp));
                            ?>
                        </div>
                    </div>
                    <div class="col-right">
                        <div class="title">
                            <?php echo $this->Html->link($article['Article']['title'], array('controller'=>'articles', 'action'=>'view', 'id'=>$article['Article']['id'], 'article'=>$this->Utility->seo_string($article['Article']['title']), 'ext'=>'html'), array('title'=>h($article['Article']['title'])));?>
                        </div>
                        <div class="short">
                            <?php echo $this->Utility->short_description($article['Article']['content'], 200);?>
                        </div>
                        <div class="read-more">
                            <?php echo $this->Html->link(__('READ MORE >'), array('controller'=>'articles', 'action'=>'view', 'id'=>$article['Article']['id'], 'article'=>$this->Utility->seo_string($article['Article']['title']), 'ext'=>'html'), array('title'=>h($article['Article']['title'])));?>
                        </div>
                    </div>
                </li>
            <?php } ?>
        <?php else : ?>
            <li class="no-recent-item"><a href="#"><?php echo __('There is no item');?></a></li>
        <?php endif ; ?>
        <li class="clear"></li>
    </ul>
</div>