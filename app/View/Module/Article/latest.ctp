<div id="recent-items">
    <ul class="nav nav-pills nav-stacked">
        <?php if (!empty($articles)) : ?>
            <?php foreach($articles as $article) { ?>
                <li class="recent-item">
                    <?php echo $this->Html->link($article['Article']['title'], array('controller'=>'articles', 'action'=>'view', 'id'=>$article['Article']['id'], 'article'=>$this->Utility->seo_string($article['Article']['title']), 'ext'=>'html'), array('title'=>h($article['Article']['title'])));?>
                </li>
            <?php } ?>
        <?php else : ?>
            <li class="no-recent-item"><a href="#"><?php echo __('There is no item');?></a></li>
        <?php endif ; ?>
        <li class="clear"></li>
    </ul>
</div>