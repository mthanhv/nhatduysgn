<div id="tags">
<?php
    if (!empty($tags)) {
        foreach ($tags as $tag) {
            ?>
            <?php echo $this->Html->link(
                $tag['Tag']['title'],
                array('plugin'=>false, 'controller'=>'products', 'action'=>'index', 'tag'=>$tag['Tag']['id'], 'title'=>$this->Utility->seo_string($tag['Tag']['title'])),
                array('class'=>'tag', 'title'=>$tag['Tag']['title'], 'style'=>sprintf('font-size:%dpx', $tag['Tag']['font_size']))
            );
            ?>
    <?php
        }
    }
?>
</div>