<?php $this->layout = 'errors';?>

<div class="container text-center">
	<div class="logo-404">
		<a href="index.html"><img src="/images/home/logo.png" alt="" /></a>
	</div>
	<div class="content-404">
		<h1><b>Lỗi</b> không tìm thấy</h1>
        <p>Xin vui lòng liên hệ với ban quản trị website để được hỗ trợ</p>
	</div>
</div>