<!--/blog-post-area-->
<div class="blog-post-area">
    <!-- <h2 class="title text-center"><?php echo __('Article detail');?></h2> -->
    <div class="single-blog-post">
        <h3><?php echo h($article['Article']['title']);?></h3>
        <?php if ($article['Article']['article_category_id'] != ARTICLE_CATEGORY_ABOUTUS) :?>
        <div class="post-meta">
            <ul>
                <li><i class="fa fa-clock-o"></i> <?php echo date('H:s', strtotime($article['Article']['created']));?></li>
                <li><i class="fa fa-calendar"></i> <?php echo date('d/m/Y', strtotime($article['Article']['created']));?></li>
            </ul>
        </div>
        <?php endif; ?>
        <?php echo $this->Html->image("/files/articles/thumbs/{$article['Article']['image']}");?>
        <div class="article-content"><?php echo $article['Article']['content'];?></div>
        <?php if ($article['Article']['article_category_id'] != ARTICLE_CATEGORY_ABOUTUS) :?>
        <div class="pager-area">
            <ul class="pager pull-right">
                <?php if (isset($related_articles['prev'])) : ?>
                    <li><?php echo $this->Html->link(__('Prev'), array('controller'=>'articles', 'action'=>'view', 'id'=>$related_articles['prev']['Article']['id'], 'article'=>$this->Utility->seo_string($related_articles['prev']['Article']['title']), 'ext'=>'html'), array('title'=>h($related_articles['prev']['Article']['title'])));?></li>
                <?php endif;?>
                <?php if (isset($related_articles['next'])) : ?>
                    <li><?php echo $this->Html->link(__('Next'), array('controller'=>'articles', 'action'=>'view', 'id'=>$related_articles['next']['Article']['id'], 'article'=>$this->Utility->seo_string($related_articles['next']['Article']['title']), 'ext'=>'html'), array('title'=>h($related_articles['next']['Article']['title'])));?></li>
                <?php endif;?>
            </ul>
        </div>
        <?php endif; ?>
    </div>
</div>
<!--/blog-post-area-->