<div class="blog-post-area projects">
	<h2 class="title text-center"><?php echo __('Projects');?></h2>
	<?php foreach ($articles as $article) : ?>
	<div class="single-blog-post">
		<?php echo $this->Html->link(
        	$this->Html->image("/files/articles/thumbs/{$article['Article']['image']}"),
        	array('controller'=>'articles', 'action'=>'view', 'id'=>$article['Article']['id'], 'article'=>$this->Utility->seo_string($article['Article']['title']), 'ext'=>'html'),
        	array('escape'=>false, 'class'=>'thumb', 'title'=>h($article['Article']['title']))
        );?>
		<h3 class="project-title">
			<?php echo $this->Html->link(
				$article['Article']['title'],
				array('controller'=>'articles', 'action'=>'view', 'id'=>$article['Article']['id'], 'article'=>$this->Utility->seo_string($article['Article']['title']), 'ext'=>'html'),
				array('class'=>'title', 'title'=>h($article['Article']['title']))
			);?>
		</h3>
   		<p><?php echo $this->Utility->short_description($article['Article']['content'], 100);?></p>
	</div>
	<?php endforeach; ?>
    <div class="pagination-area">
        <ul class="pagination">
            <?php
            echo $this->Paginator->first('«', array('tag' => 'li'));
            echo $this->Paginator->prev('‹', array('tag' => 'li'), null, array('tag' => 'li', 'disabledTag' => 'a'));
            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
            echo $this->Paginator->next('›', array('tag' => 'li'), null, array('tag' => 'li', 'disabledTag' => 'a'));
            echo $this->Paginator->last('»', array('tag' => 'li'));
            ?>
        </ul>
    </div>
</div>