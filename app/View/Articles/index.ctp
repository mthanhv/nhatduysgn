<div class="blog-post-area">
	<h2 class="title text-center"><?php echo __('Latest from our blog');?></h2>
	<?php foreach ($articles as $article) : ?>
	<div class="single-blog-post">
		<h3><?php echo h($article['Article']['title']);?></h3>
		<div class="post-meta">
			<ul>
				<li><i class="fa fa-clock-o"></i> <?php echo date('H:s', strtotime($article['Article']['created']));?></li>
				<li><i class="fa fa-calendar"></i> <?php echo date('d/m/Y', strtotime($article['Article']['created']));?></li>
			</ul>
		</div>
        <?php echo $this->Html->link($this->Html->image("/files/articles/thumbs/{$article['Article']['image']}"), array('controller'=>'articles', 'action'=>'view', 'id'=>$article['Article']['id'], 'article'=>$this->Utility->seo_string($article['Article']['title']), 'ext'=>'html'), array('escape'=>false, 'class'=>'thumb', 'title'=>h($article['Article']['title'])));?>
		<p><?php echo $this->Utility->short_description($article['Article']['content'], 500);?></p>
        <?php echo $this->Html->link(__('View detail'), array('controller'=>'articles', 'action'=>'view', 'id'=>$article['Article']['id'], 'article'=>$this->Utility->seo_string($article['Article']['title']), 'ext'=>'html'), array('class'=>'btn btn-primary', 'title'=>h($article['Article']['title'])));?>
	</div>
	<?php endforeach; ?>
    <div class="pagination-area">
        <ul class="pagination">
            <?php
            echo $this->Paginator->first('«', array('tag' => 'li'));
            echo $this->Paginator->prev('‹', array('tag' => 'li'), null, array('tag' => 'li', 'disabledTag' => 'a'));
            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
            echo $this->Paginator->next('›', array('tag' => 'li'), null, array('tag' => 'li', 'disabledTag' => 'a'));
            echo $this->Paginator->last('»', array('tag' => 'li'));
            ?>
        </ul>
    </div>
</div>