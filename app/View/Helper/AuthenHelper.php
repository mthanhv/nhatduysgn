<?php
	class AuthenHelper extends AppHelper{
		var $helpers = array('Session');
		var $authenProfile = null;

		function __construct(View $View, $settings = array()) {
			if(isset($settings['authenProfile'])) $this->authenProfile = $settings['authenProfile'];
			
			parent::__construct($View, $settings);
		}
		
		function setAuthenProfile($authenProfile = null) {
			$this->authenProfile = $authenProfile;
		}
		
		// Function to check the access for the controller / action
		function isAllowed($url = "", $group_ids = array()) {
			if (is_array($url)) { $url = $this->cleanUrl($url) ;}
			
			// If group_ids is empty then use group_ids of logged user
			if(empty($group_ids)) $group_ids = $this->getGroupIds();
			
			$allow_global = false;
			$groups_rules = $this->getRules($group_ids);
			foreach($groups_rules as $group_id => $rules) {
				$allow_local = false;	
				foreach ($rules as $data) {
					if (preg_match("/^({$data['Rule']['action']})$/i", $url, $matches)) {
						$allow_local = ($data['Rule']['permission'] == 'Allow') ? true : false;
					}
				}
				$allow_global = $allow_global | $allow_local;
			}
			return $allow_global;
		}
		
		function getRules($group_ids = array()) {
            static $cacheRules = null;

            if (null === $cacheRules) {
                $cacheRules = Cache::read('cacheRules');
            }

			$rules = array();
			$rule = null;
			foreach ($group_ids as $group_id) {
				if (isset($cacheRules[$group_id])) {
					$rules[$group_id] = $cacheRules[$group_id];
				} else {
					if($rule == null) {
						App::import('Model', 'Rule');
						$rule = new Rule();
					}
					
					$conditions = "Rule.group_id = {$group_id}";
					$order = 'Rule.order ASC';
					
					$rule->unbindModelAll();
					$data = $rule->find('all', array('fields'=>array('Rule.action', 'Rule.permission'), 'conditions'=>$conditions, 'order'=>$order));
					$nb = count($data);
					for($i = 0; $i < $nb; $i++) {
						$data[$i]['Rule']['action'] = str_replace(array('/', '*', ' or '), array('\/', '.*', '|'), $data[$i]['Rule']['action']);
					}
					
					$rules[$group_id] = $cacheRules[$group_id] = $data;
					Cache::write('cacheRules', $cacheRules);
				}
			}
			return $rules;
		}

		private function cleanUrl($url) {
			$clurl = array_intersect_key($url, array('controller' => '', 'action' => '', 'prefix' => '', 'admin' => ''));
			return Router::url($clurl + array("base" => false), false);
		}
		
		function isLogged() {
			return ($this->getUserId() !== null);
		}
		
		function getLogin() {
			return $this->Session->read("Authen{$this->authenProfile}.user_name");
		}

		function getUserId() {
			return $this->Session->read("Authen{$this->authenProfile}.id");
		}

		function getUserEmail() {
			return $this->Session->read("Authen{$this->authenProfile}.email");
		}

		function getFullName() {
			return $this->Session->read("Authen{$this->authenProfile}.full_name");
		}
		
		function getGroupIds() {
			$gid = $this->Session->read("Authen{$this->authenProfile}.group_ids");
			return (empty($gid) ? array(0) : $gid);
		}

		function getGroupNames() {
			$gn = $this->Session->read("Authen{$this->authenProfile}.group_names");
			return (empty($gn) ? array(__('Guest')) : $gn);
		}
		
		function getUserData() {
			return $this->Session->read("Authen{$this->authenProfile}");
		}

		function setTimestamp() {
			$ts = $this->Session->write("Authen{$this->authenProfile}.timestamp", time());
		}
		
		function login($user) {
			$this->Session->write("Authen{$this->authenProfile}", $user);
			$this->setTimestamp();
		}
		
		function logout() {
			$this->Session->delete("Authen{$this->authenProfile}");
		}
		
	    function setPreviousUrl($url) {
			$this->Session->write("URL{$this->authenProfile}.previousUrl", $url);
		}

		function getPreviousUrl() {
			return $this->Session->read("URL{$this->authenProfile}.previousUrl");
		}
	}
?>