<?php
class UtilityHelper extends AppHelper {
	function encrypt($string, $key = 'aedb406534340e189ce692631464a0f8') {
		if(empty($string)) return '';
		
		$result = '';
		for($i = 0; $i < strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)+ord($keychar));
			$result .= $char;
		}
		return $this->safe_b64encode($result);
	}
	
	function decrypt($string, $key = 'aedb406534340e189ce692631464a0f8') {
		if(empty($string)) return '';
		
		$result = '';
		$string = $this->safe_b64decode($string);
		for($i = 0; $i < strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$result .= $char;
		}
		return $result;
	}
	
	function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
        return $data;
    }
	
	function safe_b64decode($string) {
        $data = str_replace(array('-', '_'), array('+', '/'), $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
	
	function get_timespan($timevalue = 0) {
		$remainder = $timevalue;
		$days = floor($remainder / (60 * 60 * 24)); $remainder = $remainder % (60 * 60 * 24);
		$hours = floor($remainder / (60 * 60));	$remainder = $remainder % (60 * 60);
		$minutes = floor($remainder / 60);
		$seconds = $remainder % 60;
		return array('days'=>$days, 'hours'=>$hours, 'minutes'=>$minutes, 'seconds'=>$seconds);
	}
	
	//Support: JPG, JPEG, PNG, GIF file. Destination file may be: JPG, JPEG, PNG, GIF
	function img_convert($src_name, $dst_name, $new_w = 0, $new_h = 0) {
		//get image extension.
		$src_ext = strtolower(pathinfo($src_name, PATHINFO_EXTENSION));
		$dst_ext = strtolower(pathinfo($dst_name, PATHINFO_EXTENSION));
		
		//creates the new image using the appropriate function from gd library
		$src_img = $dst_img = null;
		if($src_ext == 'jpg' || $src_ext == 'jpeg') $src_img = imagecreatefromjpeg($src_name);
		else if($src_ext == 'png') $src_img = imagecreatefrompng($src_name);
		else if ($src_ext == 'gif') $src_img = imagecreatefromgif($src_name);

		if($src_img) {
			$src_offset_x = $src_offset_y = $dst_offset_x = $dst_offset_y = $dst_rect_w = $dst_rect_h = $src_rect_w = $src_rect_h = 0;
			
			//gets the dimmensions of the image
			$old_w = imageSX($src_img);
			$old_h = imageSY($src_img);
			
			if($new_w != 0 && $new_h != 0) {
				$ratio1 = $old_w/$new_w;
				$ratio2 = $old_h/$new_h;
				if($ratio1 > $ratio2) {
					$src_rect_h = $old_h;
					$src_rect_w = $new_w*$ratio2;
				} else {
					$src_rect_w = $old_w;
					$src_rect_h = $new_h*$ratio1;
				}
				$src_offset_x = ($old_w - $src_rect_w)/2;
				$src_offset_y = ($old_h - $src_rect_h)/2;
				$dst_rect_w = $new_w;
				$dst_rect_h = $new_h;
			} else if($new_w != 0) {
				$ratio1 = $new_w/$old_w;
				$src_rect_w = $old_w;
				$src_rect_h = $old_h;
				$dst_rect_w = $new_w;
				$dst_rect_h = $old_h*$ratio1;
			} else if($new_h != 0) {
				$ratio2 = $new_h/$old_h;
				$src_rect_w = $old_w;
				$src_rect_h = $old_h;
				$dst_rect_h = $new_h;
				$dst_rect_w = $old_w*$ratio2;
			} else {
				$src_rect_w = $dst_rect_w = $new_w = $old_w;
				$src_rect_h = $dst_rect_h = $new_h = $old_h;
			}	

			// we create a new image with the new dimmensions
			$dst_img = ImageCreateTrueColor($dst_rect_w, $dst_rect_h);

			// resize the big image to the new created one
			//var_dump(array($dst_offset_x, $dst_offset_y, $src_offset_x, $src_offset_y, $dst_rect_w, $dst_rect_h, $src_rect_w, $src_rect_h));
			imagecopyresampled($dst_img, $src_img, $dst_offset_x, $dst_offset_y, $src_offset_x, $src_offset_y, $dst_rect_w, $dst_rect_h, $src_rect_w, $src_rect_h);

			// Output the created image to the file. Now we will have the thumbnail into the file named by $dst_name
			if($dst_ext == 'jpg' || $dst_ext == 'jpeg') imagejpeg($dst_img, $dst_name);
			else if($dst_ext == 'png') imagepng($dst_img, $dst_name);
			else if ($dst_ext == 'gif') imagegif($dst_img, $dst_name);

			//destroys source and destination images.
			imagedestroy($dst_img);
			imagedestroy($src_img);
			return true;
		}
		return false;
	}

	public function upload_file($file_data = null, $options = array()) {
		if(is_array($file_data) && isset($file_data['error']) && $file_data['error'] == UPLOAD_ERR_OK) {
			$_defaults = array(
				'location' => 'files/',
				'check_exists' => false,
				'generate_unique_name' => false,
				'clean_name' => true,
				'name' => null,
				'ext' => null,
				'thumbs' => null
			);
			
			$_thumb_defaults = array(
				'location' => 'files/',
				'check_exists' => false,
				'generate_unique_name' => false,
				'use_master_file_name' => true,
				'clean_name' => true,
				'name' => null,
				'ext' => null,
				'width' => 0,
				'height' => 0,
			);
			
			$options = array_merge($_defaults, $options);
			if (is_array($options['thumbs'])) {
				$thumbs = array();
				foreach($options['thumbs'] as $key => $thumb) {
					$thumbs[$key] = array_merge($_thumb_defaults, $thumb);
				}
				$options['thumbs'] = $thumbs;
			}
			
			$name = null;
			if (!empty($options['name'])) $name = $options['name'];
			else if (!empty($options['generate_unique_name'])) $name = uniqid();
			else $name = pathinfo($file_data['name'], PATHINFO_FILENAME);
			
			if (!empty($options['clean_name'])) $name = $name;
			
			if (!empty($options['ext'])) $ext = $options['ext'];
			else $ext = pathinfo($file_data['name'], PATHINFO_EXTENSION);
			
			$file_path = "{$options['location']}{$name}.{$ext}";
			
			if (!empty($options['check_exists']) && file_exists($file_path)) {
				return array('error' => true, 'data' => __('The file name already exists'));
			}
			
			if (move_uploaded_file($file_data['tmp_name'], $file_path)) {
				$data = array();
				$data['name'] = $name;
				$data['ext'] = $ext;
				$data['location'] = $options['location'];
				$data['thumbs'] = array();
				foreach($options['thumbs'] as $key => $thumb) {
					$thumb_name = null;
					if (!empty($thumb['name'])) $thumb_name = $thumb['name'];
					if (!empty($thumb['use_master_file_name'])) $thumb_name = $name;
					else if (!empty($thumb['generate_unique_name'])) $thumb_name = uniqid();
					else $thumb_name = pathinfo($file_data['name'], PATHINFO_FILENAME);

					if (!empty($thumb['clean_name'])) $thumb_name = $thumb_name;

					if (!empty($thumb['ext'])) $ext = $thumb['ext'];
					else $ext = pathinfo($file_data['name'], PATHINFO_EXTENSION);

					$thumb_path = "{$thumb['location']}{$thumb_name}.{$ext}";
					
					if (!empty($thumb['check_exists']) && file_exists($thumb_path)) {
						return array('error' => true, 'data' => __('The thumbnail name already exists'));
					}
					
					if ($this->img_convert($file_path, $thumb_path, $thumb['width'], $thumb['height'])) {
						$data['thumbs'][$key]['name'] = $thumb_name;
						$data['thumbs'][$key]['ext'] = $ext;
						$data['thumbs'][$key]['location'] = $thumb['location'];
					} else {
						return array('error' => true, 'data' => __('Create thumbnail fail'));
					}
				}
				return array('error' => false, 'data' => $data);
			} else {
				return array('error' => true, 'data' => __('Move uploaded file fail'));
			}
			
		} else {
			return array('error' => true, 'data' => __('File uploaded fail'));
		}
	}

	public function unzip($zip_file = null, $location = null) {
		$zip = new ZipArchive();
		if ($zip->open($zip_file) === true) {
			if ($zip->extractTo($location)) {
				$zip->close();
				return true;
			} else {
				$zip->close();
				return false;
			}
		}
		return false;
	}

    public function short_description($str = null, $length = 200) {
        $str = strip_tags($str);
        if (mb_strlen($str) <= $length) {
            return $str;
        } else {
            $str = mb_substr($str, 0, $length);
            $index = mb_strrpos($str, ' ');
            $str = mb_substr($str, 0, $index);
            return $str . '...';
        }
    }

	public function remove_sign($str) {
		$uniChar = array (
			'a' => array('à','ả','ã','á','ạ','ă','ằ','ẳ','ẵ','ắ','ặ','â','ầ','ẩ','ẫ','ấ','ậ','à','ả','ã','á','ạ'),
			'A' => array('À','Ả','Ã','Á','Ạ','Ă','Ằ','Ẳ','Ẵ','Ắ','Ặ','Â','Ầ','Ẩ','Ẫ','Ấ','Ậ','À','Ả','Ã','Á','Ạ'),
			'd' => array('đ'),
			'D' => array('Đ'),
			'e' => array('è','ẻ','ẽ','é','ẹ','ê','ề','ể','ễ','ế','ệ','è','ẻ','ẽ','é','ẹ'),
			'E' => array('È','Ẻ','Ẽ','É','Ẹ','Ê','Ề','Ể','Ễ','Ế','Ệ','È','Ẻ','Ẽ','É','Ẹ'),
			'i' => array('ì','ỉ','ĩ','í','ị','ì','ỉ','ĩ','í','ị'),
			'I' => array('Ì','Ỉ','Ĩ','Í','Ị','Ì','Ỉ','Ĩ','Í','Ị'),
			'o' => array('ò','ỏ','õ','ó','ọ','ô','ồ','ổ','ỗ','ố','ộ','ơ','ờ','ở','ỡ','ớ','ợ','ò','ỏ','õ','ó','ọ'),
			'O' => array('Ò','Ỏ','Õ','Ó','Ọ','Ô','Ồ','Ổ','Ỗ','Ố','Ộ','Ơ','Ờ','Ở','Ỡ','Ớ','Ợ','Ò','Ỏ','Õ','Ó','Ọ'),
			'u' => array('ù','ủ','ũ','ú','ụ','ư','ừ','ử','ữ','ứ','ự','ù','ủ','ũ','ú','ụ'),
			'U' => array('Ù','Ủ','Ũ','Ú','Ụ','Ư','Ừ','Ử','Ữ','Ứ','Ự','Ù','Ủ','Ũ','Ú','Ụ'),
			'y' => array('ỳ','ỷ','ỹ','ý','ỵ','ỳ','ỷ','ỹ','ý','ỵ'),
			'Y' => array('Ỳ','Ỷ','Ỹ','Ý','Ỵ','Ỳ','Ỷ','Ỹ','Ý','Ỵ')
		);
		
		foreach ($uniChar as $k => $v) {
			$str = str_replace($v, $k, $str); 	
		}
		return $str;
	}
	
	public function seo_string($str) {
		$str = $this->remove_sign($str);
		App::import('Utility', 'Sanitize');
		$str = Sanitize::paranoid($str, array(' ', '-'));
		$str = str_replace(' ', '-', $str);
		$str = strtolower($str);
		
		return $str;
	}
}
?>