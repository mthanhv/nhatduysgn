<?php
	class SiteHelper extends AppHelper{
        const SITE_CACHE_KEY = 'cacheSettings';

        public function get($key = null) {
            $language = Configure::read('Config.language');
            $cacheKey = static::SITE_CACHE_KEY.'.'.$language;

            $settings = Cache::read($cacheKey);
            if (isset($settings[$key])) {
                return $settings[$key];
            } else {
                $setting = ClassRegistry::init('Setting');
                $data = $setting->find('first');
                if (!empty($data)) {
                    $settings = $data['Setting'];
                    Cache::write($cacheKey, $settings);
                    return isset($settings[$key]) ? $settings[$key] : null;
                }
            }
            return null;
        }

        public function delete() {
            $languages = Configure::read('AVAILABLE_LANGUAGE_CODES');
            foreach ($languages as $language) {
                Cache::delete(static::SITE_CACHE_KEY.'.'.$language);
            }
        }
	}
?>