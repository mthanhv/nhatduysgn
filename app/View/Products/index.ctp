<!--product_items-->
<div class="product-items">
    <h2 class="title text-center"><?php echo __('Products');?></h2>
    <?php if (!empty($products)) { ?>
    <?php foreach($products as $index => $product) { ?>
        <div class="item-product">
            <div class="product-image-wrapper">
                <div class="single-products">
                    <div class="productinfo text-center">
                        <?php echo $this->Html->link($this->Html->image("/files/products/thumbs/{$product['Product']['image']}"), array('controller'=>'products', 'action'=>'view', 'id'=>$product['Product']['id'], 'product'=>$this->Utility->seo_string($product['Product']['title']), 'ext'=>'html'), array('escape'=>false, 'class'=>'thumb', 'title'=>h($product['Product']['title'])));?>
                        <p class="product-title"><?php echo h($product['Product']['title']);?></p>
                    </div>
                </div>
                <div class="choose">
                    <ul class="nav nav-pills nav-justified">
                        <li>
                            <?php echo $this->Html->link('<i class="fa fa-plus-square"></i>' . __('View detail'), array('controller'=>'products', 'action'=>'view', 'id'=>$product['Product']['id'], 'product'=>$this->Utility->seo_string($product['Product']['title']), 'ext'=>'html'), array('escape'=>false, 'class'=>'product-title', 'title'=>h($product['Product']['title'])));?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="product-pagination">
        <ul class="pagination">
            <?php
                echo $this->Paginator->first('«', array('tag' => 'li'));
                echo $this->Paginator->prev('‹', array('tag' => 'li'), null, array('tag' => 'li', 'disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
                echo $this->Paginator->next('›', array('tag' => 'li'), null, array('tag' => 'li', 'disabledTag' => 'a'));
                echo $this->Paginator->last('»', array('tag' => 'li'));
            ?>
        </ul>
    </div>
    <?php } else { ?>
        <div class="no-product-found"><?php echo __('No product found');?></div>
    <?php } ?>
</div>
<!--product_items-->