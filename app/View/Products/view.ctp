<!--product-details-->
<div class="product-details">
    <div class="col-sm-5">
        <div class="view-product">
            <?php echo $this->Html->image("/files/products/{$product['Product']['image']}", [
                'data-zoom-image' => $this->Html->url("/files/products/{$product['Product']['image']}"),
                'id' => 'image-zoom'
            ]);?>
<!--            <h3>ZOOM</h3>-->
        </div>
    </div>
    <div class="col-sm-7">
        <!--product-information-->
        <div class="product-information">
            <h2><?php echo $product['Product']['title'];?></h2>
            <div class="product-attribute">
                <table>
                    <tbody>
                        <?php if (!empty($product['Product']['code'])) : ?>
                            <tr>
                                <td><?php echo __('Code');?></td>
                                <td><?php echo $product['Product']['code'];?></td>
                            </tr>
                        <?php endif; ?>
                        <?php if (!empty($product['ProductCategory']['name'])) : ?>
                            <tr>
                                <td><?php echo __('Category');?></td>
                                <td>
                                    <?php echo $this->Html->link(
                                        $product['ProductCategory']['name'],
                                        array('plugin'=>false, 'controller'=>'products', 'action'=>'index', 'category'=>$product['ProductCategory']['id']),
                                        array('class'=>'supplier', 'title'=>h($product['ProductCategory']['name']))
                                    );
                                    ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <?php if (!empty($product['Brand']['title'])) : ?>
                            <tr>
                                <td><?php echo __('Brand');?></td>
                                <td>
                                    <?php echo $this->Html->link(
                                        $product['Brand']['title'],
                                        array('plugin'=>false, 'controller'=>'products', 'action'=>'index', 'brand'=>$product['Brand']['id']),
                                        array('class'=>'supplier', 'title'=>h($product['Brand']['title']))
                                    );
                                    ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <?php if (!empty($product['Supplier']['title'])) : ?>
                            <tr>
                                <td><?php echo __('Supplier');?></td>
                                <td>
                                    <?php echo $this->Html->link(
                                        $product['Supplier']['title'],
                                        array('plugin'=>false, 'controller'=>'products', 'action'=>'index', 'supplier'=>$product['Supplier']['id']),
                                        array('class'=>'supplier', 'title'=>h($product['Supplier']['title']))
                                    );
                                    ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <?php if (!empty($product['Product']['origin'])) : ?>
                            <tr>
                                <td><?php echo __('Origin');?></td>
                                <td><?php echo $product['Product']['origin'];?></td>
                            </tr>
                        <?php endif; ?>
                        <?php if (!empty($product['Tag'])) : ?>
                            <tr>
                                <td><?php echo __('Tags');?></td>
                                <td>
                                    <?php foreach ($product['Tag'] as $tag) : ?>
                                        <?php echo $this->Html->link(
                                            $tag['title'],
                                            array('plugin'=>false, 'controller'=>'products', 'action'=>'index', 'tag'=>$tag['id']),
                                            array('class'=>'tag', 'title'=>h($tag['title']))
                                        ) . ' / ';
                                        ?>
                                    <?php endforeach;?>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <?php if (!empty($product['Product']['attachment'])) : ?>
                            <tr>
                                <td><?php echo __('Attachment');?></td>
                                <td><?php echo $this->Html->link($product['Product']['attachment'], "/files/products/attachments/{$product['Product']['attachment']}");?></td>
                            </tr>
                        <?php endif; ?>
                        <tr>
                            <td><?php echo __('Share this');?></td>
                            <td><div class="addthis_sharing_toolbox"></div></td>
                        </tr>
                        <tr>
                            <td class="quick-contact"><?php echo __('Quick contact');?></td>
                            <td>
                                028-7300 7797
                                <?php $emails = explode(',', $this->Site->get('site_emails'));?>
                                <?php if (!empty($emails)) : ?>
                                    <?php foreach ($emails as $email) : ?>
                                        <br/><a href="mailto:<?php echo $email;?>"><?php echo $email;?></a>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!--/product-information-->

        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56db96219aef1808"></script>
    </div>
</div>
<!--/product-details-->

<!--category-tab-->
<div class="category-tab shop-details-tab">
    <div class="col-sm-12">
        <ul class="nav nav-tabs">
            <?php if (!empty($product['Product']['content'])) : ?>
            <li class="<?php echo $active_tab == PRODUCT_DETAIL_ACTIVE_CONTENT ? 'active' : '';?>"><a href="#details" data-toggle="tab"><?php echo __('Product detail');?></a></li>
            <?php endif; ?>
            <?php if (!empty($product['Product']['specification'])) : ?>
            <li class="<?php echo $active_tab == PRODUCT_DETAIL_ACTIVE_SPECIFICATION ? 'active' : '';?>"><a href="#specification" data-toggle="tab"><?php echo __('Specification');?></a></li>
            <?php endif; ?>
        </ul>
    </div>
    <div class="tab-content">
        <?php if (!empty($product['Product']['content'])) : ?>
        <div class="tab-pane fade<?php echo $active_tab == PRODUCT_DETAIL_ACTIVE_CONTENT ? ' active in' : ''?>" id="details" >
            <div class="product-content"><?php echo $product['Product']['content'];?></div>
        </div>
        <?php endif; ?>

        <?php if (!empty($product['Product']['specification'])) : ?>
        <div class="tab-pane fade<?php echo $active_tab == PRODUCT_DETAIL_ACTIVE_SPECIFICATION ? ' active in' : ''?>" id="specification" >
            <div class="product-specification"><?php echo $product['Product']['specification'];?></div>
        </div>
        <?php endif; ?>
    </div>
</div>
<!--/category-tab-->

<!--recommended_items-->
<?php if (!empty($related_products)) :?>
<div id="recommended-item" class="carousel slide">
    <h2 class="title text-center"><?php echo __('Recommended items');?></h2>

    <div id="recommended-item-bounder">
        <div id="recommended-item-container" class="carousel-inner">
            <?php foreach($related_products as $index => $item) { ?>
                <div class="recommended-product">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <?php echo $this->Html->link($this->Html->image("/files/products/thumbs/{$item['Product']['image']}"), array('controller'=>'products', 'action'=>'view', 'id'=>$item['Product']['id'], 'product'=>$this->Utility->seo_string($item['Product']['title']), 'ext'=>'html'), array('escape'=>false, 'class'=>'thumb', 'title'=>h($item['Product']['title'])));?>
                                <p class="product-title"><?php echo h($item['Product']['title']);?></p>
                                <?php echo $this->Html->link('<i class="fa fa-plus-square"></i>' . __('View detail'), array('controller'=>'products', 'action'=>'view', 'id'=>$item['Product']['id'], 'product'=>$this->Utility->seo_string($item['Product']['title']), 'ext'=>'html'), array('escape'=>false, 'class'=>'btn btn-default add-to-cart', 'title'=>h($item['Product']['title'])));?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <a id="recommended-item-prev" class="left recommended-item-control" href="javascript:void(0)">
        <i class="fa fa-angle-left"></i>
    </a>
    <a id="recommended-item-next" class="right recommended-item-control" href="javascript:void(0)">
        <i class="fa fa-angle-right"></i>
    </a>
</div>
<?php endif; ?>
<script type="text/javascript">
    var NUMBER_OF_RECOMMENDED_ITEMS = <?php echo count($related_products);?>;
</script>
<!--/recommended_items-->

<?php echo $this->modules('recent_items');?>