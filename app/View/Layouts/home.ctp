<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo $this->Site->get('site_description');?>">
    <meta name="author" content="Thanh Vo">
    <title><?php echo $this->Site->get('site_name'); ?> - <?php echo $title_for_layout; ?></title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/prettyPhoto.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
    <link href="/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <script src="/js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
<header id="header"><!--header-->
    <div class="header_top"><!--header_top-->
        <?php echo $this->element('header_top');?>
    </div><!--/header_top-->

    <div class="header-middle"><!--header-middle-->
        <?php echo $this->element('header_middle');?>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <?php echo $this->element('main_menu');?>
                    </div>
                </div>
                <div class="col-sm-3">
                    <?php echo $this->element('search_box');?>
                </div>
            </div>
        </div>
    </div><!--/header-bottom-->
</header><!--/header-->

<section id="slider"><!--slider-->
    <?php echo $this->modules('slider');?>
</section><!--/slider-->

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <?php echo $this->element('hot_line');?>
                    <?php echo $this->modules('left');?>
                </div>
            </div>
            <div class="col-sm-9" style="padding: 0">
                <?php echo $this->modules('right'); ?>
                <?php echo $this->fetch('content'); ?>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <h2 class="title text-center"><?php echo __('Customers');?></h2>
            <?php echo $this->modules('customers');?>
        </div>
    </div>
</section>

<footer id="footer"><!--Footer-->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="companyinfo">
                        <h2><span><?php echo __('Partners');?></span></h2>
                        <p><?php echo __('Our products are provided by popular and famous partners around the world');?></p>
                    </div>
                </div>
                <?php echo $this->modules('suppliers');?>
            </div>
        </div>
    </div>

    <div class="footer-widget">
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <div class="single-widget">
                        <h2><?php echo __('Contact Info');?></h2>
                        <div class="contact">
                            <p class="company-name">CÔNG TY TNHH KỸ THUẬT NHẬT DUY</p>
                            <p>68/4 Đường số 3, P. Bình Hưng Hoà A, Q. Bình Tân, TP. HCM</p>
                            <p>Phone: 028 7300 7797 - Fax: 028 7304 7744</p>
                            <p>Email: <a href="mailto:sales@nhatduysgn.com">sales@nhatduysgn.com</a></p>
                            <p>MST: 0310880481</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-7 small-device">
                    <?php echo $this->modules('footer_top');?>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">Copyright © 2009 Nhat Duy.</p>
            </div>
        </div>
    </div>
</footer><!--/Footer-->

<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.scrollUp.min.js"></script>
<script src="/js/jquery.scroller.js"></script>
<script src="/js/jquery.prettyPhoto.js"></script>
<script src="/js/main.js"></script>
</body>
</html>