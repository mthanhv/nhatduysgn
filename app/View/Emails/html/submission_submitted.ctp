<div style="font-family:Arial;font-size:13px;">
	<p><?php echo __('Hi %s', $data['name']); ?>, </p>
	<p><?php echo __('Thank you for submitting! We appreciate that you’ve taken the time to write us. We’ll get back to you very soon.');?></p>
	<p><?php echo __('Your submission detail as below');?></p>
	<p>
        <table width="80%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
            <tbody>
            <tr>
                <td style="padding: 5px; border: 1px solid #cccccc;" align="right" valign="top"><?php echo __('Name');?></td>
                <td style="padding: 5px; border: 1px solid #cccccc;" valign="top"><?php echo h($data['name']);?></td>
            </tr>
            <tr>
                <td style="padding: 5px; border: 1px solid #cccccc;" align="right" valign="top"><?php echo __('Company name');?></td>
                <td style="padding: 5px; border: 1px solid #cccccc;" valign="top"><?php echo h($data['company']);?></td>
            </tr>
            <tr>
                <td style="padding: 5px; border: 1px solid #cccccc;" align="right" valign="top"><?php echo __('Address');?></td>
                <td style="padding: 5px; border: 1px solid #cccccc;" valign="top"><?php echo h($data['address']);?></td>
            </tr>
            <tr>
                <td style="padding: 5px; border: 1px solid #cccccc;" align="right" valign="top"><?php echo __('Telephone');?></td>
                <td style="padding: 5px; border: 1px solid #cccccc;" valign="top"><?php echo h($data['phone']);?></td>
            </tr>
            <tr>
                <td style="padding: 5px; border: 1px solid #cccccc;" align="right" valign="top"><?php echo __('Email address');?></td>
                <td style="padding: 5px; border: 1px solid #cccccc;" valign="top"><?php echo h($data['email']);?></td>
            </tr>
            <tr>
                <td style="padding: 5px; border: 1px solid #cccccc;" align="right" valign="top"><?php echo __('Title');?></td>
                <td style="padding: 5px; border: 1px solid #cccccc;" valign="top"><?php echo h($data['title']);?></td>
            </tr>
            <tr>
                <td style="padding: 5px; border: 1px solid #cccccc;" align="right" valign="top"><?php echo __('Demand');?></td>
                <td style="padding: 5px; border: 1px solid #cccccc;" valign="top"><?php echo h($data['content']);?></td>
            </tr>
            </tbody>
        </table>
	</p>
	<p><?php echo __('Best regards,');?><br/></p>
	<span style="font-size:11px"><?php echo __('Please do not respond to this message as it is automatically.');?></span>
	<hr style="border:none;border-top:1px solid #DDD;"/>
	<span style="font-size:11px"><?php echo __('Should you have any enquiries with regards to our services, please do not hesitate to contact us');?></span>
</div>