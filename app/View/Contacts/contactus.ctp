<div class="bg">
    <div class="row">
        <div class="col-sm-12">
            <h2 class="title text-center"><?php echo __('Contact <strong>Us</strong>');?></h2>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.0193799884287!2d106.5988125754658!3d10.809827358575923!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752b939441fe0f%3A0x78e9f1705ccb707d!2zQ8OUTkcgVFkgVE5ISCBL4bu4IFRIVeG6rFQgTkjhuqxUIERVWQ!5e0!3m2!1sen!2s!4v1722224752145!5m2!1sen!2s" width="100%" height="450" style="border:0; padding-bottom: 30px;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8">
            <div class="contact-form">
                <h2 class="title text-center"><?php echo __('Get In Touch');?></h2>
                <div id="flash-message" class="status alert alert-success" style="display: none"><?php echo $this->Session->flash();?></div>
                <?php echo $this->Form->create('Contact', ['id'=>'main-contact-form', 'class'=>'contact-form row', 'name'=>'contact-form', 'method'=>'post']);?>
                    <div class="form-group col-md-6">
                        <?php echo $this->Form->input('name', ['label'=>false, 'div'=>false, 'class'=>'form-control', 'placeholder'=>__('Name')]);?>
                    </div>
                    <div class="form-group col-md-6">
                        <?php echo $this->Form->input('email', ['label'=>false, 'div'=>false, 'class'=>'form-control', 'placeholder'=>__('Email')]);?>
                    </div>
                    <div class="form-group col-md-6">
                        <?php echo $this->Form->input('phone', ['label'=>false, 'div'=>false, 'class'=>'form-control', 'placeholder'=>__('Phone')]);?>
                    </div>
                    <div class="form-group col-md-6">
                        <?php echo $this->Form->input('address', ['label'=>false, 'div'=>false, 'class'=>'form-control', 'placeholder'=>__('Address')]);?>
                    </div>
                    <div class="form-group col-md-12">
                        <?php echo $this->Form->input('title', ['label'=>false, 'div'=>false, 'class'=>'form-control', 'placeholder'=>__('Subject')]);?>
                    </div>
                    <div class="form-group col-md-12">
                        <?php echo $this->Form->input('content', ['label'=>false, 'div'=>false, 'id'=>'message', 'rows'=>8, 'class'=>'form-control', 'placeholder'=>__('Your Message Here')]);?>
                    </div>
                    <div class="form-group col-md-12">
                        <input type="submit" name="submit" class="btn btn-primary pull-right" value="<?php echo __('Send');?>">
                    </div>
                <?php echo $this->Form->end();?>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="contact-info">
                <h2 class="title text-center"><?php echo __('Contact Info');?></h2>
                <address>
                    <p>Công Ty TNHH Kỹ Thuật Nhật Duy</p>
                    <p>Địa Chỉ: 68/4 Đường số 3, P. Bình Hưng Hoà A, Q. Bình Tân, TP. HCM</p>
                    <p>Điện Thoại: 028 7300 7797</p>
                    <?php $emails = explode(',', $this->Site->get('site_emails'));?>
                    <?php if (!empty($emails)) : ?>
                    <p>Email:
                        <?php foreach ($emails as $email) : ?>
                            <a href="mailto:<?php echo $email;?>"><?php echo $email;?></a>&nbsp;
                        <?php endforeach; ?>
                    </p>
                    <?php endif; ?>
                </address>
                <div class="social-networks">
                    <h2 class="title text-center"><?php echo __('Social Networking');?></h2>
                    <?php echo $this->element('social_network');?>
                </div>
            </div>
        </div>
    </div>
</div>