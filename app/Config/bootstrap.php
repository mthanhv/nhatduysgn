<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

require APP . 'Lib' . DS . 'Error' . DS . 'exceptions.php';

// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File'));

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *     'Model'                     => array('/path/to/models', '/next/path/to/models'),
 *     'Model/Behavior'            => array('/path/to/behaviors', '/next/path/to/behaviors'),
 *     'Model/Datasource'          => array('/path/to/datasources', '/next/path/to/datasources'),
 *     'Model/Datasource/Database' => array('/path/to/databases', '/next/path/to/database'),
 *     'Model/Datasource/Session'  => array('/path/to/sessions', '/next/path/to/sessions'),
 *     'Controller'                => array('/path/to/controllers', '/next/path/to/controllers'),
 *     'Controller/Component'      => array('/path/to/components', '/next/path/to/components'),
 *     'Controller/Component/Auth' => array('/path/to/auths', '/next/path/to/auths'),
 *     'Controller/Component/Acl'  => array('/path/to/acls', '/next/path/to/acls'),
 *     'View'                      => array('/path/to/views', '/next/path/to/views'),
 *     'View/Helper'               => array('/path/to/helpers', '/next/path/to/helpers'),
 *     'Console'                   => array('/path/to/consoles', '/next/path/to/consoles'),
 *     'Console/Command'           => array('/path/to/commands', '/next/path/to/commands'),
 *     'Console/Command/Task'      => array('/path/to/tasks', '/next/path/to/tasks'),
 *     'Lib'                       => array('/path/to/libs', '/next/path/to/libs'),
 *     'Locale'                    => array('/path/to/locales', '/next/path/to/locales'),
 *     'Vendor'                    => array('/path/to/vendors', '/next/path/to/vendors'),
 *     'Plugin'                    => array('/path/to/plugins', '/next/path/to/plugins'),
 * ));
 *
 */
App::build(array('Module' => array('%s' . 'Module' . DS)), App::REGISTER);

/**
 * Custom Inflector rules, can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */

/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */
CakePlugin::load('Admin');

/**
 * You can attach event listeners to the request lifecycle as Dispatcher Filter . By Default CakePHP bundles two filters:
 *
 * - AssetDispatcher filter will serve your asset files (css, images, js, etc) from your themes and plugins
 * - CacheDispatcher filter will read the Cache.check configure variable and try to serve cached content generated from controllers
 *
 * Feel free to remove or add filters as you see fit for your application. A few examples:
 *
 * Configure::write('Dispatcher.filters', array(
 *        'MyCacheFilter', //  will use MyCacheFilter class from the Routing/Filter package in your app.
 *        'MyPlugin.MyFilter', // will use MyFilter class from the Routing/Filter package in MyPlugin plugin.
 *        array('callable' => $aFunction, 'on' => 'before', 'priority' => 9), // A valid PHP callback type to be called on beforeDispatch
 *        array('callable' => $anotherMethod, 'on' => 'after'), // A valid PHP callback type to be called on afterDispatch
 *
 * ));
 */
Configure::write('Dispatcher.filters', array(
    'AssetDispatcher',
    'CacheDispatcher'
));

/**
 * Configures default file logging options
 */
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
    'engine' => 'FileLog',
    'types' => array('notice', 'info', 'debug'),
    'file' => 'debug',
));
CakeLog::config('error', array(
    'engine' => 'FileLog',
    'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
    'file' => 'error',
));

//Default language for site
Configure::write('Config.language', 'vie');

//User management configurations
Configure::write('Authen.baseUrl', Router::url('/', true));
Configure::write('Authen.loginAction', array('plugin' => 'admin', 'controller' => 'user', 'action' => 'login'));
Configure::write('Authen.loggedAction', array('plugin' => 'admin', 'controller' => 'admin', 'action' => 'index'));
Configure::write('Authen.defaultDeniedAction', array('plugin' => 'admin', 'controller' => 'user', 'action' => 'denied'));
Configure::write('Authen.sessionTimeout', 3600 * 24 * 7); //One week

defined('MAIN_MENU') || define('MAIN_MENU', 1);

defined('GROUP_ADMIN') || define('GROUP_ADMIN', 1);
defined('GROUP_REGISTERED') || define('GROUP_REGISTERED', 2);

defined('ARTICLE_ABOUTUS') || define('ARTICLE_ABOUTUS', 1);
defined('ARTICLE_CERTIFICATES') || define('ARTICLE_CERTIFICATES', 2);

defined('ARTICLE_CATEGORY_ABOUTUS') || define('ARTICLE_CATEGORY_ABOUTUS', 1);
defined('ARTICLE_CATEGORY_NEWS') || define('ARTICLE_CATEGORY_NEWS', 2);
defined('ARTICLE_CATEGORY_PROJECT') || define('ARTICLE_CATEGORY_PROJECT', 4);
defined('ARTICLE_CATEGORY_EVENT') || define('ARTICLE_CATEGORY_EVENT', 5);

defined('PUBLISHED') || define('PUBLISHED', 1);
defined('UNPUBLISHED') || define('UNPUBLISHED', 0);
Configure::write('PUBLISH_STATUS', array(
    PUBLISHED => __('Publish'),
    UNPUBLISHED => __('UnPublish')
));

defined('FEATURED') || define('FEATURED', 1);
defined('UNFEATURED') || define('UNFEATURED', 0);
Configure::write('FEATURED_STATUS', array(
    PUBLISHED => __('Featured'),
    UNPUBLISHED => __('UnFeatured')
));

Configure::write('AVAILABLE_LANGUAGE_CODES', array('eng', 'vie'));
Configure::write('AVAILABLE_LANGUAGES', array(
    'eng' => __('English'),
    'vie' => __('Tiếng Việt'),
));
Configure::write('AVAILABLE_LANGUAGES_FROM', array(
    'eng' => __('English'),
    'vie' => __('Tiếng Việt'),
));
Configure::write('AVAILABLE_LANGUAGES_TO', array(
    'vie' => __('Tiếng Việt'),
    'eng' => __('English'),
));

defined('BLOCK_ABOUT_US_INFO') || define('BLOCK_ABOUT_US_INFO', 1);
defined('BLOCK_CONTACT_US_INFO') || define('BLOCK_CONTACT_US_INFO', 2);
defined('BLOCK_HEADER_TOP') || define('BLOCK_HEADER_TOP', 3);

defined('LINK_TO_HOME') || define('LINK_TO_HOME', 1);
defined('LINK_TO_PRODUCT_LIST') || define('LINK_TO_PRODUCT_LIST', 2);
defined('LINK_TO_PRODUCT') || define('LINK_TO_PRODUCT', 3);
defined('LINK_TO_ARTICLE_LIST') || define('LINK_TO_ARTICLE_LIST', 4);
defined('LINK_TO_ARTICLE') || define('LINK_TO_ARTICLE', 5);
defined('LINK_TO_CUSTOM') || define('LINK_TO_CUSTOM', 6);
Configure::write('LINK_TO_CATEGORIES', array(
    LINK_TO_HOME => __('Home'),
    LINK_TO_PRODUCT_LIST => __('Product List'),
    LINK_TO_PRODUCT => __('Product'),
    LINK_TO_ARTICLE_LIST => __('Article List'),
    LINK_TO_ARTICLE => __('Article'),
    LINK_TO_CUSTOM => __('Custom'),
));

defined('RECENT_VIEW_ITEMS_KEY') || define('RECENT_VIEW_ITEMS_KEY', 'RecentView.Items');
defined('RECENT_VIEW_ITEMS_MAX') || define('RECENT_VIEW_ITEMS_MAX', 5);
defined('RECENT_VIEW_ITEMS_TIMEOUT_DELTA') || define('RECENT_VIEW_ITEMS_TIMEOUT_DELTA', 2592000);

defined('SUPPLIER_THUMB1_W') || define('SUPPLIER_THUMB1_W', 129);
defined('SUPPLIER_THUMB1_H') || define('SUPPLIER_THUMB1_H', 57);
defined('CUSTOMER_THUMB1_W') || define('CUSTOMER_THUMB1_W', 129);
defined('CUSTOMER_THUMB1_H') || define('CUSTOMER_THUMB1_H', 57);
defined('SLIDE_THUMB1_W') || define('SLIDE_THUMB1_W', 1140);
defined('SLIDE_THUMB1_H') || define('SLIDE_THUMB1_H', 500);
defined('SLIDE_THUMB2_W') || define('SLIDE_THUMB2_W', 220);
defined('SLIDE_THUMB2_H') || define('SLIDE_THUMB2_H', 60);
defined('ARTICLE_THUMB1_W') || define('ARTICLE_THUMB1_W', 830);
defined('ARTICLE_THUMB1_H') || define('ARTICLE_THUMB1_H', 400);

defined('PRODUCT_DETAIL_ACTIVE_CONTENT') || define('PRODUCT_DETAIL_ACTIVE_CONTENT', 'PRODUCT_CONTENT');
defined('PRODUCT_DETAIL_ACTIVE_SPECIFICATION') || define('PRODUCT_DETAIL_ACTIVE_SPECIFICATION', 'PRODUCT_SPECIFICATION');