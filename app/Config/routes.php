<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::connect('/', array('plugin' => false, 'controller' => 'home', 'action' => 'index'));
Router::connect('/gioi-thieu.html', array('plugin' => false, 'controller' => 'articles', 'action' => 'view', 1));
Router::connect('/dich-vu.html', array('plugin' => false, 'controller' => 'articles', 'action' => 'view', 64));
Router::connect('/san-pham', array('plugin' => false, 'controller' => 'products', 'action' => 'index'));
Router::connect('/chung-chi.html', array('plugin' => false, 'controller' => 'articles', 'action' => 'view', 100));
Router::connect('/tin-tuc', array('plugin' => false, 'controller' => 'articles', 'action' => 'index'));
Router::connect('/du-an', array('plugin' => false, 'controller' => 'articles', 'action' => 'projects'));
Router::connect('/su-kien', array('plugin' => false, 'controller' => 'articles', 'action' => 'events'));
Router::connect('/lien-he.html', array('plugin' => false, 'controller' => 'contacts', 'action' => 'contactus'));
Router::connect('/p:id-:product', ['plugin' => false, 'controller' => 'products', 'action' => 'view'], ['pass' => ['id'], 'id' => '[0-9]+']);
Router::connect('/a:id-:article', ['plugin' => false, 'controller' => 'articles', 'action' => 'view'], ['pass' => ['id'], 'id' => '[0-9]+']);
Router::connect('/products/search', ['plugin' => false, 'controller' => 'products', 'action' => 'search']);
Router::connect('/products/*', ['plugin' => false, 'controller' => 'products', 'action' => 'index']);
//Router::connect('/products/b:brand-:title', ['plugin' => false, 'controller' => 'products', 'action' => 'index'], ['pass'=>['brand'], 'brand'=>'[0-9]+']);

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
